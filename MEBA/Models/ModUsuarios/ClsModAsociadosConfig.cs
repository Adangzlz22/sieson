﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModUsuarios
{
    public class ClsModAsociadosConfig
    {
        public int IDAsociadoConfig { get; set; }
        public Nullable<int> IDAsociado { get; set; }
        public string SitioWEB { get; set; }
        public string ServerSeguridad { get; set; }
        public string UsuarioSQLSeguridad { get; set; }
        public string PasswordSQLSeguridad { get; set; }
        public string BaseProduccion { get; set; }
        public string urlApi { get; set; }
        public string usuarioApi { get; set; }
        public string passwordApi { get; set; }
        public int? IdRol { get; set; }
        public int? primerZona { get; set; }
    }
}