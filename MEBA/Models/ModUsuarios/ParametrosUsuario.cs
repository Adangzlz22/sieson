﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModUsuarios
{
    public class ParametrosUsuario
    {
        public string Accion { get; set; }
        public int IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string ApeidoP { get; set; }
        public string ApeidoM { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public string Usuario { get; set; }
        public string Contrasena { get; set; }
        public string Grupo { get; set; }
        public int Rol { get; set; }
        public bool Estatus { get; set; }
        public List<int> lstIdZonas { get; set; }
    }
}