﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModUsuarios
{
    public class ClsModAsociadosContactos
    {
        public int IDContacto { get; set; }
        public int IDAsociado { get; set; }
        public Nullable<int> IDTipoContacto { get; set; }
        public string Nombre { get; set; }
        public string Telefono { get; set; }
        public string Extension { get; set; }
        public string Puesto { get; set; }
        public string Email { get; set; }
        public string Cometario { get; set; }
        public Nullable<bool> Activo { get; set; }
        public Nullable<System.DateTime> FechaAlta { get; set; }
        public Nullable<int> IdRol { get; set; }
        public string Rol { get; set; }
    }
}