﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModUsuarios
{
    public class parametrosMenu
    {
        public int contador { get; set; }
        public int OIDUsuario { get; set; }
        public int OIDMenuPadre { get; set; }
        public int OIDMenuHijo { get; set; }
        public string MenuDescripcionPadre { get; set; }
        public string MenuDescripcionHijo { get; set; }
        public bool check { get; set; }
    }
}