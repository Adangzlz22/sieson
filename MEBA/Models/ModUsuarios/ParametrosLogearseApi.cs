﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModUsuarios
{
    public class ParametrosLogearseApi
    {
        public string Usuario { get; set; }
        public string Password { get; set; }
        public string Url { get; set; }
        public string EndPoint { get; set; }

    }
}