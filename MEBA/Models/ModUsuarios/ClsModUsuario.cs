﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModUsuarios
{
    public class ClsModUsuario
    {
        public int IdUsuario { get; set; }
        public string GuidUsuario { get; set; }
        public string GuidEmpresa { get; set; }
        public string GuidSucursal { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public Nullable<System.DateTime> FechaNacimiento { get; set; }
        public string RFC { get; set; }
        public string CURP { get; set; }
        public string CodigoPostal { get; set; }
        public string Nacionalidad { get; set; }
        public Nullable<bool> Estatus { get; set; }
        public Nullable<int> idRol { get; set; }
        public Nullable<System.DateTime> FechaCreacion { get; set; }
        public Nullable<System.DateTime> FechaUltimaModificacion { get; set; }
        public byte[] Foto { get; set; }
        public string Titulo { get; set; }
        public MEBARolUsuario objRol { get; set; }
        public MEBAProEmpresas objEmpresa { get; set; }
        public List<int?> lstIdZonas { get; set; }
    }
}