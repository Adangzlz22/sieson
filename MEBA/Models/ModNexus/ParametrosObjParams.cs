﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModNexus
{
    public class ParametrosObjParams
    {
        public string token { get; set; }
        public string stationId { get; set; }
        public string transactionCode { get; set; }
    }
}