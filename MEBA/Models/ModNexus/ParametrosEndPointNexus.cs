﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModNexus
{
    public class ParametrosEndPointNexus
    {
        public string Url { get; set; }
        public string EndPoint { get; set; }
        public string objParams { get; set; }

        public string Identifier { get; set; }
        public string stationId { get; set; }
        public string estacion { get; set; }
    }
}