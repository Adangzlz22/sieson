﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModZonas
{
    public class resultadosZonas
    {
        public int IdZona { get; set; }
        public string Zona { get; set; }
        public MEBAProEmpresasUsuarios Usuario { get; set; }
        public List<int?> lstEstacionesAsignadas { get; set; }
    }
}