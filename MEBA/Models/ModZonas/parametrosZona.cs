﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModZonas
{
    public class parametrosZona
    {
        public string Accion { get; set; }
        public int IdZona { get; set; }
        public string Zona { get; set; }
        public List<int?> lstEstacionesAsignadas { get; set; }
    }
}