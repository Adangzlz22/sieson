﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModReportes
{
    public class ModConse
    {
        public string consecutivoReporte { get; set; }
        public string fechaInicio { get; set; }
        public string fechaFin { get; set; }
        public string Manga { get; set; }
        public string Premium { get; set; }
        public string Diesel { get; set; }
    }
}