﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModReportes
{
    public class resultadoResponse
    {
        public bool Error { get; set; }
        public List<resultadosRptProductos> Data { get; set; }
        public string Comment { get; set; }
    }
}