﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModReportes
{
    public class resultadosRptProductos
    {
        
        public string StationId { get; set; }
        public string ProductId { get; set; }
        public string ProductDescription { get; set; }
        public string ProductFiscalCode { get; set; }
        public decimal InitialInventory { get; set; }
        public decimal FinalInventory { get; set; }
        public decimal Purchase { get; set; }
        public decimal Sale { get; set; }
        public decimal Final { get; set; }
        public decimal Difference { get; set; }
        public string StandardDeviation { get; set; }
        public string Fecha { get; set; }
        public decimal WithCommission { get; set; }
        
    }
}