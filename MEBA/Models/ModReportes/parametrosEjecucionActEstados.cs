﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModReportes
{
    public class parametrosEjecucionActEstados
    {
        public DateTime fechaini { get; set; }
        public DateTime fechafin { get; set; }
        public int StationId { get; set; }
    }
}