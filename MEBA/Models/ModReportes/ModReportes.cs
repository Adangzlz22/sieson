﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModReportes
{
    public class ModReportes
    {
        public string token { get; set; }
        public string stationId { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
    }
}