﻿using MEBA.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModReportes
{
    public class resultadoReportes
    {
        public bool Error { get; set; }
        public List<rptInventarioVsVentas> Data { get; set; }
        public string Comment { get; set; }
    }
}