﻿using MEBA.Models.ModReportes.ReporteResumen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModReportes
{
    public class ParametrosReportes
    {
        public string Url { get; set; }
        public string objParams { get; set; }
        public string objParams2 { get; set; }
        public string Token { get; set; }
        public int stationId { get; set; }
        public string commercialName { get; set; }
        public string fechaInicio { get; set; }
        public string fechaFin { get; set; }
        public int ejecucion { get; set; }
        public string stationNumber { get; set; }
        public string commercial { get; set; }
        public string Descripcion { get; set; }
        public List<lstEstaciones> lstEstaciones { get; set; }
        public List<int?> lstEstacionesClass { get; set; }
        public decimal Magna { get; set; }
        public decimal Premium { get; set; }
        public decimal Diesel { get; set; }
        public int IdZonas { get; set; }

    }
}