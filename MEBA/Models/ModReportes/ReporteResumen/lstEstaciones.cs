﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModReportes.ReporteResumen
{
    public class lstEstaciones
    {
        public int StationId { get; set; }
        public string Nombre { get; set; }
    }
}