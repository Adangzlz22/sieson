﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModReportes.ReporteResumen
{
    public class ResultadoReportesResumen
    {
        public bool Error { get; set; }
        public List<ResumenReporte> Data { get; set; }
        public string Comment { get; set; }
    }
}