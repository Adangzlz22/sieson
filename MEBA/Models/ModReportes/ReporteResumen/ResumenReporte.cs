﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModReportes.ReporteResumen
{
    public class ResumenReporte
    {
        public int StationId { get; set; }
        public string FormattedDate { get; set; }
        public decimal SaleQty1 { get; set; }
        public decimal SaleQty2 { get; set; }
        public decimal SaleQty3 { get; set; }
        public string Qty1 { get; set; }
        public string Qty2 { get; set; }
        public string Qty3 { get; set; }
        public string DiffAmount1 { get; set; }
        public string DiffAmount2 { get; set; }
        public string DiffAmount3 { get; set; }
        public string DiffAmountTotal { get; set; }
        public decimal DiffQty1 { get; set; }
        public decimal DiffQty2 { get; set; }
        public decimal DiffQty3 { get; set; }
        public decimal DiffQtyTotal { get; set; }
        public string PercentageQty1 { get; set; }
        public string PercentageQty2 { get; set; }
        public string PercentageQty3 { get; set; }
        public string WithCommission { get; set; }
        
        public List<resultadosRptProductos> DataProductos { get; set; }
    }
}