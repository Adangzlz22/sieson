﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModReportes.ReporteResumen
{
    public class MEBAReporteViewResumen
    {
        public int oidReporte { get; set; }
        public Nullable<int> consecutivo { get; set; }
        public Nullable<int> IdEstacion { get; set; }
        public string Estacion { get; set; }
        public string Fecha { get; set; }
        public string Producto { get; set; }
        public decimal InvInicial { get; set; }
        public decimal Compras { get; set; }
        public decimal Ventas { get; set; }
        public decimal DiffAmount { get; set; }
        
        public decimal InvFinalTeorico { get; set; }
        public decimal LecturaFinal { get; set; }
        public decimal DifTeoricoVsLectura { get; set; }
        public decimal CalculadoDifTeoricoVsLectura { get; set; }
        public decimal DifEstacion { get; set; }
        public decimal Pendiente { get; set; }
        public decimal CalculadoDifEstacion { get; set; }


        public decimal Captura { get; set; }
        public decimal AjustePerfectoI{ get; set; }
        public decimal Faltante { get; set; }
        public decimal Sobrante { get; set; }
        public decimal Cumplimiento { get; set; }
        public decimal DiffQty { get; set; }
        public decimal WithCommission { get; set; }

    }
}