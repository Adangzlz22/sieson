﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModGenerales
{
    public class ClsModResponse
    {
        public string MENSAJE { get; set; }
        public bool SUCCESS { get; set; }
        public object ITEMS { get; set; }
    }
}