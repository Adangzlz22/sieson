﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModCotizacion
{
    public class DTOCotizacion
    {
        public int id { get; set; }
        public Nullable<int> IdCotizacion { get; set; }
        public Nullable<System.DateTime> FechaCreacion { get; set; }
        public string FechaTexto { get; set; }
        public Nullable<int> ProductoId { get; set; }
        public Nullable<decimal> PrecioVenta { get; set; }
        public Nullable<decimal> PrecioUnitario { get; set; }
        public Nullable<decimal> Cantidad { get; set; }
        public string Folio { get; set; }
        public string Tramo { get; set; }
        public string Atencion { get; set; }
        public string NombreCliente { get; set; }
        public string RFCCliente { get; set; }
        public string Direccion { get; set; }
        public string NumINT { get; set; }
        public string CP { get; set; }
        public string Ciudad { get; set; }
        public string Estado { get; set; }
        public string Pais { get; set; }
        public string Unidad { get; set; }
        public string Medida { get; set; }
        public string Observaciones { get; set; }
        public string Solicito { get; set; }
        public string Autorizo { get; set; }


        public string NombreProducto { get; set; }
        public string Reflejante { get; set; }
        public string codigo { get; set; }
        
    }
}