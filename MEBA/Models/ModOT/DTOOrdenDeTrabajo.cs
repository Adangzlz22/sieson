﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModOT
{
    public class DTOOrdenDeTrabajo
    {
        public byte[] imagen1 { get; set; }
        public byte[] imagen2 { get; set; }
        public byte[] imagen3 { get; set; }
        public byte[] imagen4 { get; set; }
        public string codigo1{ get; set; }
        public string codigo2{ get; set; }
        public string codigo3{ get; set; }
        public string codigo4 { get; set; }
        public string cantidad1{ get; set; }
        public string cantidad2{ get; set; }
        public string cantidad3{ get; set; }
        public string cantidad4 { get; set; }
        public string medida1 { get; set; }
        public string medida2 { get; set; }
        public string medida3 { get; set; }
        public string medida4 { get; set; }
        public string reflejante1 { get; set; }
        public string reflejante2 { get; set; }
        public string reflejante3 { get; set; }
        public string reflejante4 { get; set; }

    }
}