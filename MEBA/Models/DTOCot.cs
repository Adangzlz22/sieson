﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models
{
    public class DTOCot
    {
        public Nullable<int> IdCotizacion { get; set; }
        public string NombreCliente { get; set; }
        public string RFCCliente { get; set; }
        public Nullable<System.DateTime> FechaCreacion { get; set; }
        public string Folio { get; set; }
        public bool Activo { get; set; }
    }
}