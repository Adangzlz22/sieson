﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModXZonas
{
    public class resultadoZonas
    {
        public int StationId { get; set; }
        public int Estacion { get; set; }
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
        public decimal Despacho { get; set; }
        public decimal ConTicket { get; set; }
        public decimal Flotilla { get; set; }
        public decimal R4 { get; set; }
        public decimal R10 { get; set; }
        public decimal R20 { get; set; }
        public decimal Jarreos { get; set; }
        public decimal R4R { get; set; }
        public decimal R10R { get; set; }
        public decimal R20R { get; set; }
        public decimal B { get; set; }
        public decimal A { get; set; }
    }
}