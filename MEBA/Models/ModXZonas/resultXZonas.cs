﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModXZonas
{
    public class resultXZonas
    {
        public bool Error { get; set; }
        public List<resultadoZonas> Data { get; set; }
        public string Comment { get; set; }
    }
}