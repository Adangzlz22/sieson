//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MEBA.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CatOrdenDeCompra
    {
        public int id { get; set; }
        public Nullable<int> IdCotizacion { get; set; }
        public Nullable<System.DateTime> FechaCreacion { get; set; }
        public string FechaTexto { get; set; }
        public Nullable<int> ProductoId { get; set; }
        public Nullable<decimal> PrecioVenta { get; set; }
        public Nullable<decimal> Cantidad { get; set; }
        public string Folio { get; set; }
        public string Tramo { get; set; }
        public string Atencion { get; set; }
        public string NombreCliente { get; set; }
        public string RFCCliente { get; set; }
        public string Direccion { get; set; }
        public string NumINT { get; set; }
        public string CP { get; set; }
        public string Ciudad { get; set; }
        public string Estado { get; set; }
        public string Pais { get; set; }
        public Nullable<bool> Activo { get; set; }
        public Nullable<decimal> PrecioUnitario { get; set; }
        public string Medida { get; set; }
        public string Oberservaciones { get; set; }
        public string Autorizo { get; set; }
        public string Solicito { get; set; }
        public string Reflejante { get; set; }
    }
}
