﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModProductos
{
    public class parametrosProductos
    {
        public string Opcion { get; set; }
        public int id { get; set; }
        public string descripcion { get; set; }
        public string img64 { get; set; }
        public string tipoFormato { get; set; }
        public string img { get; set; }
        public string unidad { get; set; }
        public string codigo { get; set; }
        public string codigoP { get; set; }
        public Nullable<decimal> precioVenta { get; set; }
        public HttpPostedFile Archivo { get; set; }
        public int Pag { get; set; }
        public int numPag { get; set; }


    }
}