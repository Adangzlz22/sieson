﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModProductos
{
    public class clsModInventario
    {
        public int station_id { get; set; }
        public string tank_number { get; set; }
        public string product_id { get; set; }
        public int inventory { get; set; }
        public string LastUpdate { get; set; }
    }
}