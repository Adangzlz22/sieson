﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModEmpresas
{
    public class ParametrosEmpresas
    {
        public string Accion { get; set; }
        public string TipoConexion { get; set; }
        public int IDEmpresa { get; set; }
        public string SitioWEB { get; set; }
        public string ServerSeguridad { get; set; }
        public string UsuarioSQLSeguridad { get; set; }
        public string PasswordSQLSeguridad { get; set; }
        public string BaseProduccion { get; set; }
        public string urlApi { get; set; }
        public string usuarioApi { get; set; }
        public string passwordApi { get; set; }
        public string NombreComercial { get; set; }
        public string Telefono { get; set; }
        public string Calle { get; set; }
        public int CP { get; set; }
        public string Pais { get; set; }
        public string Estado { get; set; }
        public string Municipio { get; set; }
        public bool Estatus { get; set; }
        public bool ReprocesarEstados { get; set; }

        public bool MostrarTiempo { get; set; }
        public bool MostrarXml { get; set; }
        public bool MostrarTurno { get; set; }
        public bool MostrarInventarios { get; set; }


    }
}