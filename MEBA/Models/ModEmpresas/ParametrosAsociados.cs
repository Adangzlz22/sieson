﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModEmpresas
{
    public class ParametrosAsociados
    {
        public string Accion { get; set; }
        public int IDAsociado { get; set; }
        public int IDContacto { get; set; }
        public string Nombre { get; set; }
        public string Telefono { get; set; }
        public int IdRol { get; set; }
        


    }
}