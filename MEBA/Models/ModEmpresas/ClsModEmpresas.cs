﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModEmpresas
{
    public class ClsModEmpresas
    {
        public MEBAProEmpresas objEmpresa { get; set; }
        public MEBAAsociados objAsociados { get; set; }
        public MEBAAsociadosConfig objAsociadosConfig { get; set; }
        public MEBAGenEstados objEstados { get; set; }
        public MEBAGenMunicipios objMunicipios { get; set; }
        
    }
}