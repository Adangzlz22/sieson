﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModEstaciones
{
    public class clsModTurno
    {
        public DateTime UltimaFecha { get; set; }
        public int UltimoTurno { get; set; }
        public int CantidadTurnos { get; set; }
        public double minutos { get; set; }
    }
}