﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModEstaciones
{
    public class clsModEstaciones
    {
        public string order_number { get; set; }
        public int station_id { get; set; }
        public string number { get; set; }
        public string short_name { get; set; }
        public string current_shift { get; set; }
        public string commercial_name { get; set; }
    }
}