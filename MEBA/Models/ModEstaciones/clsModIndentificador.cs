﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModEstaciones
{
    public class clsModIndentificador
    {
        public int OID { get; set; }
        public int? IdentificadorSP { get; set; }
        public string Imagen { get; set; }
        public string IdentificadorDestino { get;set; }
    }
}