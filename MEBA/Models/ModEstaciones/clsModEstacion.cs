﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModEstaciones
{
    public class clsModEstacion
    {
        public int StationId { get; set; }
        public string StationNumber { get; set; }
        public string CommercialName { get; set; }
        public int Identifier { get; set; }
    }
}