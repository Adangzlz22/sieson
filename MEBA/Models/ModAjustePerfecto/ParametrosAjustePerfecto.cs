﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Models.ModAjustePerfecto
{
    public class ParametrosAjustePerfecto
    {
        public string Accion { get; set; }
        public int IdAjuste { get; set; }
        public int IdAsociado { get; set; }
        public int IdEstacion { get; set; }
        public string Estacion { get; set; }
        
        public decimal magna { get; set; }
        public decimal premium { get; set; }
        public decimal diesel { get; set; }

    }
}