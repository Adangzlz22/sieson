﻿using MEBA.Clases;
using MEBA.Models;
using MEBA.Models.ModGenerales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MEBA.Controllers
{
    public class LoyoutController : Controller
    {
        MEBAEntities db = new MEBAEntities();
        // GET: Loyout
        public ActionResult Index()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult menu()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult about()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult item()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult MenuHijo(int idPadre)
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                ViewBag.idPadre = idPadre;
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult CerrarSession()
        {
            vSessiones.sesionUsuarioDTO = null;
            vSessiones.sessionUsuarioCONFIGDTO = null;
            return Redirect("/Login/Login");
        }
        public ActionResult _Layout()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public JsonResult obtenerMenu()
        {
            string html = "";
            ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
            ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;

            var lstMenu = db.spdObtenerMenu(vSessiones.sesionUsuarioDTO.IdUsuario).ToList();

            //string dropdown = @"
            //        <li class='nav-item' id='{0}'>
            //            <div class='dropdown dropend'>
            //                <a class='nav-link' href='#' data-bs-toggle='dropdown' style='border:none!important'>
            //                    {1} 
            //                    <span> {2}</span>
            //                </a>
            //                <ul class='dropdown-menu submenudrop'>
            //                    <li><h5 class='dropdown-header'>Submenú</h5></li>
            //                        {3}
            //                </ul>
            //            </div>
            //        </li>
            //    ";
            //string submenu = "";
            //int contador = 0;
            string descripcion = "";
            foreach (var item in lstMenu)
            {
                //var contList = lstMenu.Where(r => r.RedirectionPadre == "/#" && r.MenuDescripcionPadre == item.MenuDescripcionPadre).ToList().Count();

                //if (item.RedirectionPadre != "/#")
                //{

                if (descripcion != item.MenuDescripcionPadre)
                {

                    descripcion = item.MenuDescripcionPadre;
                    string contenido = item.RedirectionPadre == "/#" ? "/Loyout/MenuHijo?idPadre=" + item.OIDMenuPadre.ToString() : item.RedirectionPadre;
                    //contador = 0;
                    //submenu = "";
                    html += @"
                            <li class='nav-item' id='dash'>
                                <a class='nav-link' style='border:none!important' href='" + contenido + @"'>
                                    " + item.MenuIconPadre + @"
                                    <span>" + item.MenuDescripcionPadre + @"</span>
                                </a>
                            </li>
                        ";
                }


                //}
                //else
                //{
                //    contador++;
                //    submenu += @"
                //        <li><a class='dropdown-item' href='" + item.RedirectionHijo + @"'> " + item.MenuIconHijo +" "+ item.MenuDescripcionHijo + @"  </a></li>
                //    ";
                //}
                //if (item.RedirectionPadre == "/#" && contador == contList)
                //{
                // html +=  String.Format(dropdown, item.MenuDescripcionPadre, item.MenuIconPadre, item.MenuDescripcionPadre, submenu);
                //}
            }

            return Json(html, JsonRequestBehavior.AllowGet);

        }

        public JsonResult obtenerMenuHijo(int IdPadre)
        {
            string html = "";
            ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
            ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;

            var lstMenu = db.spdObtenerMenu(vSessiones.sesionUsuarioDTO.IdUsuario).ToList();

            string dropdown = @"
                 <div class='col-6 col-sm-6 col-md-4 col-lg-3 mb-3'>
                    <a href='{0}'>
                        <div class='card' style='height:100%' id='{1}'>
                            <div class='card-body card-imp'>
                                <div class='text-center'>
                                    {2}
                                    <br />
                                    <p>{3}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                ";

            foreach (var item in lstMenu)
            {
                if (item.OIDMenuPadre == IdPadre)
                {
                    html += String.Format(dropdown, item.RedirectionHijo, item.MenuDescripcionHijo, item.MenuIconHijo, item.MenuDescripcionHijo);
                }
            }

            return Json(html, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        [AllowAnonymous]
        [ActionName("postGenerarAlertas")]
        public JsonResult postGenerarAlertas()
        {
            List<GenAlertas> lstDatos = new List<GenAlertas>();
            int con = 0;
            string response = "";
            try
            {
                using (MEBAEntities oDB2 = new MEBAEntities())
                {
                    int IdUsuario = vSessiones.sesionUsuarioDTO.IdUsuario;
                    lstDatos = oDB2.GenAlertas.Where(r => r.IdUsuario == IdUsuario && r.Activo == true).ToList();
                }
            }
            catch (Exception e)
            {
                var a = con;
                Console.WriteLine(e.ToString());
            }

            return Json(lstDatos, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [AllowAnonymous]
        [ActionName("btnEjecutarDesactivarAlerta")]
        public JsonResult btnEjecutarDesactivarAlerta(GenAlertas parametros)
        {
            List<GenAlertas> lstDatos = new List<GenAlertas>();
            int con = 0;
            string response = "";
            try
            {
                using (MEBAEntities oDB2 = new MEBAEntities())
                {
                    var objAlertas = oDB2.GenAlertas.Where(r => r.IdAlerta == parametros.IdAlerta).FirstOrDefault();
                    if (objAlertas != null)
                    {
                        objAlertas.Activo = false;
                        oDB2.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                var a = con;
                Console.WriteLine(e.ToString());
            }

            return Json(lstDatos, JsonRequestBehavior.AllowGet);
        }

    }
}
