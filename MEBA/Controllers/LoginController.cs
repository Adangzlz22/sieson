﻿using MEBA.Clases;
using MEBA.Models;
using MEBA.Models.ModGenerales;
using MEBA.Models.ModUsuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TCITools;

namespace MEBA.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult Login()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                return Redirect("/Home/Index");
            }
            else
            {
                return View();
            }
        }
        public ActionResult Login2()
        {
            return View();
        }
        public ActionResult Login3()
        {
            return View();
        }
        public ActionResult RecuperarPassword()
        {
            return View();
        }
        public ActionResult ConfirmPassword()
        {
            return View();
        }
        // GET: Login/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }


        public string _codigoUnico = GlobalUtils.CodigoUnico;

        [HttpPost]
        [AllowAnonymous]
        [ActionName("postLogearseUsuario")]
        public JsonResult postLogearseUsuario(ParametrosLogin parametros)
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {
        
                using (MEBAEntities oDB2 = new MEBAEntities())
                {
                 
                        Herramientas oHerramientas = new Herramientas();
                        string password = Funciones.EncriptaClave(parametros.Password);
                        MEBAProEmpresasUsuarios usuLogeado = oDB2.MEBAProEmpresasUsuarios.Where(r => r.UserName == parametros.Usuario && r.Password == password && r.Estatus == true).FirstOrDefault();
                        if (usuLogeado != null)
                        {
                            var usuEmpresa = oDB2.MEBAProEmpresas.Where(r => r.GuidEmpresa == usuLogeado.GuidEmpresa && r.Estatus == true).FirstOrDefault();
                            if (usuEmpresa != null)
                            {
                                MEBAAsociados objAsociado = oDB2.MEBAAsociados.Where(r => r.IDAsociado == usuEmpresa.IDAsociado).FirstOrDefault();

                                if (objAsociado != null)
                                {
                                    MEBAAsociadosConfig obConfig = oDB2.MEBAAsociadosConfig.Where(r => r.IDAsociado == objAsociado.IDAsociado).FirstOrDefault();
                                    if (obConfig != null)
                                    {



                                        MEBAProEmpresas Empresa = oDB2.MEBAProEmpresas.Where(r => r.GuidEmpresa == usuLogeado.GuidEmpresa).FirstOrDefault();
                                        vSessiones.sesionUsuarioDTO = usuLogeado;
                                        vSessiones.sessionUsuarioCONFIGDTO = obConfig;
                                        vSessiones.sessionUsuarioEmpresa = Empresa;

                                        MEBABitacoraAcc objBitacora = new MEBABitacoraAcc();

                                        objBitacora.Fecha = DateTime.Now;
                                        objBitacora.Tipo = "Accesos";
                                        objBitacora.Origen = obConfig.SitioWEB;
                                        objBitacora.Usuario = usuLogeado.UserName;
                                        objBitacora.Grupo = Empresa.NombreComercial;
                                        objBitacora.Zona = "";
                                        objBitacora.Estacion = "(N/A)";
                                        objBitacora.Descripcion = "Ingreso a Plataforma";
                                        objBitacora.IdAsociado = vSessiones.sessionUsuarioCONFIGDTO.IDAsociado;
                                        var FechaAnterior = oDB2.spdObtenerFechaAnterior("0", vSessiones.sessionUsuarioEmpresa.NombreComercial).FirstOrDefault();
                                        if (FechaAnterior != null)
                                        {
                                            objBitacora.FechaAnterior = FechaAnterior;
                                        }
                                        oDB2.MEBABitacoraAcc.Add(objBitacora);
                                        oDB2.SaveChanges();

                                        objResultado.ITEMS = null;
                                        objResultado.SUCCESS = true;
                                        objResultado.MENSAJE = "";

                                    }
                                    else
                                    {
                                        objResultado.ITEMS = null;
                                        objResultado.SUCCESS = false;
                                        objResultado.MENSAJE = "Ocurrio algun error al conectar con el servidor asociado";
                                    }
                                }
                                else
                                {
                                    objResultado.ITEMS = null;
                                    objResultado.SUCCESS = false;
                                    objResultado.MENSAJE = "Ocurrio algun error al conectar con el servidor asociado";
                                }
                            }
                            else
                            {
                                objResultado.ITEMS = null;
                                objResultado.SUCCESS = false;
                                objResultado.MENSAJE = "No se encontro ningun usuario con estas credenciales o la empresa se ha desactivado.";
                            }
                        }
                    }


            }
            catch (Exception ex)
            {

                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        [ActionName("postRecuperarContraseña")]
        public JsonResult postRecuperarContraseña(ParametrosRecuperarContrasena parametros)
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {
                using (MEBAEntities oDB = new MEBAEntities())
                {
                    MEBAAsociados objAsociado = oDB.MEBAAsociados.Where(r => r.GUIDAsocuado == _codigoUnico).FirstOrDefault();
                    if (objAsociado != null)
                    {
                        MEBAAsociadosConfig obConfig = oDB.MEBAAsociadosConfig.Where(r => r.IDAsociado == objAsociado.IDAsociado).FirstOrDefault();
                        if (obConfig != null)
                        {

                            string _Servidor = "";
                            string _Usuario = "";
                            string _Password = "";
                            PLDComm comm = new PLDComm();
                            if (comm.ObtenerCredencialesAsociado(_codigoUnico))
                            {
                                _Servidor = comm.Servidor;
                                _Usuario = comm.Usuario;
                                _Password = comm.Password;
                                using (MEBAEntities oDB2 = new MEBAEntities())
                                {

                                    MEBAProEmpresasUsuarios usuLogeado = oDB2.MEBAProEmpresasUsuarios.Where(r => r.UserName == parametros.Usuario).FirstOrDefault();
                                    if (usuLogeado != null)
                                    {
                                        if (usuLogeado.Correo == parametros.Correo)
                                        {

                                            OPEWatchEnvios objEnvio = new OPEWatchEnvios();

                                            Herramientas oHerra = new Herramientas();

                                            objEnvio.idTipoEnvio = 1;
                                            objEnvio.Fecha = DateTime.Now;
                                            objEnvio.Tipo = "Correo de recuperacion";
                                            objEnvio.Plataforma = "MEBASoftware.com";
                                            objEnvio.Asunto = "Recuperacion de contraseña.";
                                            objEnvio.De = "contacto@sicmacontroles.mx";
                                            objEnvio.Para = usuLogeado.Correo;
                                            objEnvio.CC = usuLogeado.Correo;
                                            objEnvio.CCO = usuLogeado.Correo;
                                            objEnvio.Prioridad = "ALTA";
                                            objEnvio.Mensaje = "Este es un mensaje auto generado para la recuperacion de su clave.";
                                            objEnvio.Html = false;
                                            objEnvio.AccionSQLAntes = "";
                                            objEnvio.AccionSQLDespues = "";
                                            objEnvio.Enviado = false;
                                            objEnvio.UsuarioQueGenera = usuLogeado.UserName;
                                            objEnvio.FechaHoraEnvio = null;
                                            objEnvio.MensajeError = "";
                                            objEnvio.FechaProgramaEnvio = null;

                                            oDB.OPEWatchEnvios.Add(objEnvio);
                                            oDB.SaveChanges();

                                            objResultado.ITEMS = usuLogeado;
                                            objResultado.SUCCESS = true;
                                            objResultado.MENSAJE = "";
                                        }
                                        else
                                        {
                                            objResultado.ITEMS = null;
                                            objResultado.SUCCESS = false;
                                            objResultado.MENSAJE = "El correo no concuerda con el usuario.";
                                        }
                                    }
                                    else
                                    {
                                        objResultado.ITEMS = null;
                                        objResultado.SUCCESS = false;
                                        objResultado.MENSAJE = "No se encontro ningun usuario con estas credenciales";
                                    }
                                }
                            }

                        }
                        else
                        {
                            objResultado.ITEMS = null;
                            objResultado.SUCCESS = false;
                            objResultado.MENSAJE = "Ocurrio algun error al conectar con el servidor asociado";
                        }
                    }
                    else
                    {
                        objResultado.ITEMS = null;
                        objResultado.SUCCESS = false;
                        objResultado.MENSAJE = "Ocurrio algun error al conectar con el servidor asociado";
                    }
                }
            }
            catch (Exception ex)
            {

                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        [ActionName("postCerrarSession")]
        public JsonResult postCerrarSession()
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {

                using (MEBAEntities oDB2 = new MEBAEntities())
                {
                    Herramientas oHerramientas = new Herramientas();
                    string password = vSessiones.sesionUsuarioDTO.Password;
                    MEBAProEmpresasUsuarios usuLogeado = oDB2.MEBAProEmpresasUsuarios.Where(r => r.UserName == vSessiones.sesionUsuarioDTO.UserName && r.Password == password).FirstOrDefault();
                    if (usuLogeado != null)
                    {
                        var usuEmpresa = oDB2.MEBAProEmpresas.Where(r => r.GuidEmpresa == usuLogeado.GuidEmpresa).FirstOrDefault();
                        if (usuEmpresa != null)
                        {
                            MEBAAsociados objAsociado = oDB2.MEBAAsociados.Where(r => r.IDAsociado == usuEmpresa.IDAsociado).FirstOrDefault();

                            if (objAsociado != null)
                            {
                                MEBAAsociadosConfig obConfig = oDB2.MEBAAsociadosConfig.Where(r => r.IDAsociado == objAsociado.IDAsociado).FirstOrDefault();
                                if (obConfig != null)
                                {
                        


                                    MEBAProEmpresas Empresa = oDB2.MEBAProEmpresas.Where(r => r.GuidEmpresa == usuLogeado.GuidEmpresa).FirstOrDefault();

                                    MEBABitacoraAcc objBitacora = new MEBABitacoraAcc();

                                    objBitacora.Fecha = DateTime.Now;
                                    objBitacora.Tipo = "Accesos";
                                    objBitacora.Origen = obConfig.SitioWEB;
                                    objBitacora.Usuario = usuLogeado.UserName;
                                    objBitacora.Grupo = Empresa.NombreComercial;
                                    objBitacora.Zona = "";
                                    objBitacora.Estacion = "(N/A)";
                                    objBitacora.Descripcion = "Cerrar sesion de plataforma";
                                    objBitacora.IdAsociado = vSessiones.sessionUsuarioCONFIGDTO.IDAsociado;
                                    var FechaAnterior = oDB2.spdObtenerFechaAnterior("0", vSessiones.sessionUsuarioEmpresa.NombreComercial).FirstOrDefault();
                                    if (FechaAnterior != null)
                                    {
                                        objBitacora.FechaAnterior = FechaAnterior;
                                    }
                                    oDB2.MEBABitacoraAcc.Add(objBitacora);
                                    oDB2.SaveChanges();
                                    vSessiones.sesionUsuarioDTO = null;
                                    vSessiones.sessionUsuarioCONFIGDTO = null;
                                    objResultado.ITEMS = null;
                                    objResultado.SUCCESS = true;
                                    objResultado.MENSAJE = "";

                                }
                                else
                                {
                                    objResultado.ITEMS = null;
                                    objResultado.SUCCESS = false;
                                    objResultado.MENSAJE = "Ocurrio algun error al conectar con el servidor asociado";
                                }
                            }
                            else
                            {
                                objResultado.ITEMS = null;
                                objResultado.SUCCESS = false;
                                objResultado.MENSAJE = "Ocurrio algun error al conectar con el servidor asociado";
                            }
                        }
                        else
                        {
                            objResultado.ITEMS = null;
                            objResultado.SUCCESS = false;
                            objResultado.MENSAJE = "No se encontro ningun usuario con estas credenciales";
                        }
                    }
                }


            }
            catch (Exception ex)
            {

                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

    }
}
