﻿using CrystalDecisions.CrystalReports.Engine;
using MEBA.Clases;
using MEBA.Models;
using MEBA.Models.ds;
using MEBA.Models.ModCotizacion;
using MEBA.Models.ModGenerales;
using MEBA.Models.ModReportes;
using MEBA.Models.ModUsuarios;
using MEBA.Reportes;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Windows.Documents;
using LicenseContext = OfficeOpenXml.LicenseContext;

namespace MEBA.Controllers
{
    public class ReportesController : Controller
    {
        ClsModResponse objResultado = new ClsModResponse();
        MEBAEntities db = new MEBAEntities();
        // GET: Reportes
        public ActionResult Index()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }

        }
        public ActionResult Cotizacion()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                addBitacora("Operativo", "Acceso a la cotizacion");
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }

        }
        public ActionResult ODCompras()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                addBitacora("Operativo", "Acceso ala orden de compra");
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }

        }

        [HttpPost]
        [AllowAnonymous]
        [ActionName("postExtraerUltimoFolio")]
        public JsonResult postExtraerUltimoFolio()
        {
            objResultado = new ClsModResponse();
            try
            {
                using (db = new MEBAEntities())
                {
                    var listado = db.CatCotizacion.OrderByDescending(r => r.IdCotizacion).FirstOrDefault() == null ? 0 : db.CatCotizacion.OrderByDescending(r => r.IdCotizacion).FirstOrDefault().IdCotizacion;
                    listado++;
                    objResultado.ITEMS = AgregarCeros((int)listado, 8);
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        [ActionName("postSpdObtenerListadoCotizacion")]
        public JsonResult postSpdObtenerListadoCotizacion()
        {
            objResultado = new ClsModResponse();
            try
            {
                using (db = new MEBAEntities())
                {
                    var listado = db.spdObtenerListadoCotizacion().ToList().Select(y=>new DTOCot { 
                        IdCotizacion = y.IdCotizacion,
                        NombreCliente = y.NombreCliente,
                        RFCCliente = y.RFCCliente,
                        FechaCreacion = y.FechaCreacion,
                        Folio = y.Folio,
                        Activo = (bool)db.CatCotizacion.Where(r=>r.IdCotizacion == y.IdCotizacion).FirstOrDefault().Activo,
                    }).ToList();
                    objResultado.ITEMS = listado;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [AllowAnonymous]
        [ActionName("postSpdObtenerListadoCotizacionxIdCotizacion")]
        public JsonResult postSpdObtenerListadoCotizacionxIdCotizacion(parametrosCotizacion parametros)
        {
            objResultado = new ClsModResponse();
            try
            {
                using (db = new MEBAEntities())
                {
                    var listado = db.CatCotizacion.Where(r => r.IdCotizacion == parametros.IdCotizacion).ToList().Select(y => new DTOCotizacion
                    {
                        id = y.id,
                        IdCotizacion = y.IdCotizacion,
                        FechaCreacion = y.FechaCreacion,
                        FechaTexto = y.FechaTexto,
                        ProductoId = y.ProductoId,
                        PrecioVenta = y.PrecioVenta,
                        PrecioUnitario = y.PrecioUnitario,
                        Cantidad = y.Cantidad,
                        Folio = y.Folio,
                        Tramo = y.Tramo,
                        Atencion = y.Atencion,
                        NombreCliente = y.NombreCliente,
                        RFCCliente = y.RFCCliente,
                        Direccion = y.Direccion,
                        NumINT = y.NumINT,
                        CP = y.CP,
                        Ciudad = y.Ciudad,
                        Estado = y.Estado,
                        Pais = y.Pais,
                        Medida = y.Medida,
                        Unidad = db.CatProductos.Where(r => r.id == y.ProductoId).FirstOrDefault() == null ? "" : db.CatProductos.Where(r => r.id == y.ProductoId).FirstOrDefault().unidad,
                        NombreProducto = ObtenerDescripcionProducto(db.CatProductos.Where(r => r.id == y.ProductoId).FirstOrDefault(), y),
                        Reflejante = y.Reflejante,
                        codigo = db.CatProductos.Where(r => r.id == y.ProductoId).FirstOrDefault() == null ? "" : db.CatProductos.Where(r => r.id == y.ProductoId).FirstOrDefault().codigo,
                    }).ToList();
                    objResultado.ITEMS = listado;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        public string ObtenerDescripcionProductoCompras(CatProductos objProductos, CatOrdenDeCompra r)
        {
            string descripcion = "";
            if (objProductos != null)
            {
                if (objProductos.codigo != null && objProductos.codigo != "")
                {
                    descripcion += objProductos.codigo + ", ";
                }
                else
                {
                    descripcion = descripcion.Replace(',', ' ');
                }
                if (r.Medida != null && r.Medida != "")
                {
                    descripcion += r.Medida + ", ";
                }
                else
                {
                    descripcion = descripcion.Replace(',', ' ');
                }
                if (r.Reflejante != null && r.Reflejante != "")
                {
                    descripcion += r.Reflejante + ", ";
                }
                else
                {
                    descripcion = descripcion.Replace(',', ' ');
                }
                if (objProductos.descripcion != null && objProductos.descripcion != "")
                {
                    descripcion += objProductos.descripcion;
                }

            }
            return descripcion;
        }
        public string ObtenerDescripcionProducto(CatProductos objProductos, CatCotizacion r)
        {
            string descripcion = "";
            if (objProductos != null)
            {
                if (objProductos.codigo != null && objProductos.codigo != "")
                {
                    descripcion += objProductos.codigo + ", ";
                }
                else
                {
                    descripcion = descripcion.Replace(',', ' ');
                }
                if (r.Medida != null && r.Medida != "")
                {
                    descripcion += r.Medida + ", ";
                }
                else
                {
                    descripcion = descripcion.Replace(',', ' ');
                }
                if (r.Reflejante != null && r.Reflejante != "")
                {
                    descripcion += r.Reflejante + ", ";
                }
                else
                {
                    descripcion = descripcion.Replace(',', ' ');
                }
                if (objProductos.descripcion != null && objProductos.descripcion != "")
                {
                    descripcion += objProductos.descripcion;
                }

            }
            return descripcion;
        }


        public string addBitacora(string tipo, string descripcion)
        {
            string guardado = "";
            try
            {
                MEBABitacoraAcc objBitacora = new MEBABitacoraAcc();
                objBitacora.Fecha = DateTime.Now;
                objBitacora.Tipo = tipo;
                objBitacora.Origen = "";
                objBitacora.Usuario = vSessiones.sesionUsuarioDTO.UserName;
                objBitacora.Grupo = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                objBitacora.Zona = "";
                objBitacora.Estacion = "(N/A)";
                objBitacora.Descripcion = descripcion;
                objBitacora.IdAsociado = vSessiones.sessionUsuarioCONFIGDTO.IDAsociado;
                db.MEBABitacoraAcc.Add(objBitacora);
                db.SaveChanges();
                guardado = "exitoso";
            }
            catch (Exception ex)
            {
                guardado = "";
            }
            return guardado;
        }


        [HttpPost]
        [AllowAnonymous]
        [ActionName("postSpdAgregarModificarEliminar")]
        public JsonResult postSpdAgregarModificarEliminar(parametrosCotizacion parametros)
        {
            objResultado = new ClsModResponse();
            try
            {
                using (db = new MEBAEntities())
                {
                    CatCotizacion objCotizacion = new CatCotizacion();
                    CatCotizacion objRegistro = new CatCotizacion();
                    List<CatCotizacion> lstCot = new List<CatCotizacion>();
                    MEBABitacoraAcc objBitacora = new MEBABitacoraAcc();


                    switch (parametros.Opcion)
                    {
                        case "A":
                            if (parametros.ProductoId == 12)
                            {
                                objCotizacion = new CatCotizacion();
                                objCotizacion.IdCotizacion = parametros.IdCotizacion;
                                objCotizacion.FechaCreacion = parametros.FechaCreacion;
                                objCotizacion.FechaTexto = ConvertirFechaALetras((DateTime)parametros.FechaCreacion);
                                objCotizacion.ProductoId = parametros.ProductoId;
                                objCotizacion.PrecioUnitario = 0;
                                objCotizacion.PrecioVenta = 0;
                                objCotizacion.Cantidad = 0;
                                objCotizacion.Folio = DateTime.Now.Year.ToString() + "/" + parametros.Folio;
                                objCotizacion.Tramo = parametros.Tramo == null ? "" : parametros.Tramo;
                                objCotizacion.Atencion = parametros.Atencion == null ? "" : parametros.Atencion;
                                objCotizacion.NombreCliente = parametros.NombreCliente == null ? "" : parametros.NombreCliente;
                                objCotizacion.RFCCliente = parametros.RFCCliente == null ? "" : parametros.RFCCliente;
                                objCotizacion.Direccion = parametros.Direccion == null ? "" : parametros.Direccion;
                                objCotizacion.NumINT = parametros.NumINT == null ? "" : parametros.NumINT;
                                objCotizacion.CP = parametros.CP == null ? "" : parametros.CP;
                                objCotizacion.Ciudad = parametros.Ciudad == null ? "" : parametros.Ciudad;
                                objCotizacion.Estado = parametros.Estado == null ? "" : parametros.Estado;
                                objCotizacion.Pais = parametros.Pais == null ? "" : parametros.Pais;
                                objCotizacion.Medida = "";
                                objCotizacion.Reflejante = parametros.Reflejante;
                                objCotizacion.Activo = true;
                                db.CatCotizacion.Add(objCotizacion);




                                db.SaveChanges();
                                addBitacora("Guardado", "Se guardo producto en cotizacion");
                            }
                            else
                            {
                                objRegistro = db.CatCotizacion.Where(r => r.ProductoId == parametros.ProductoId && r.IdCotizacion == parametros.IdCotizacion && r.Medida == parametros.Medida).FirstOrDefault();
                                if (objRegistro == null)
                                {
                                    objCotizacion = new CatCotizacion();
                                    objCotizacion.IdCotizacion = parametros.IdCotizacion;
                                    objCotizacion.FechaCreacion = parametros.FechaCreacion;
                                    objCotizacion.FechaTexto = ConvertirFechaALetras((DateTime)parametros.FechaCreacion);
                                    objCotizacion.ProductoId = parametros.ProductoId;
                                    objCotizacion.PrecioUnitario = parametros.PrecioUnitario;
                                    objCotizacion.PrecioVenta = parametros.Cantidad * parametros.PrecioUnitario;
                                    objCotizacion.Cantidad = parametros.Cantidad;
                                    objCotizacion.Folio = DateTime.Now.Year.ToString() + "/" + parametros.Folio;
                                    objCotizacion.Tramo = parametros.Tramo == null ? "" : parametros.Tramo;
                                    objCotizacion.Atencion = parametros.Atencion == null ? "" : parametros.Atencion;
                                    objCotizacion.NombreCliente = parametros.NombreCliente == null ? "" : parametros.NombreCliente;
                                    objCotizacion.RFCCliente = parametros.RFCCliente == null ? "" : parametros.RFCCliente;
                                    objCotizacion.Direccion = parametros.Direccion == null ? "" : parametros.Direccion;
                                    objCotizacion.NumINT = parametros.NumINT == null ? "" : parametros.NumINT;
                                    objCotizacion.CP = parametros.CP == null ? "" : parametros.CP;
                                    objCotizacion.Ciudad = parametros.Ciudad == null ? "" : parametros.Ciudad;
                                    objCotizacion.Estado = parametros.Estado == null ? "" : parametros.Estado;
                                    objCotizacion.Pais = parametros.Pais == null ? "" : parametros.Pais;
                                    objCotizacion.Medida = parametros.Medida;
                                    objCotizacion.Activo = true;
                                    objCotizacion.Reflejante = parametros.Reflejante;
                                    db.CatCotizacion.Add(objCotizacion);
                                    db.SaveChanges();
                                    addBitacora("Guardado", "Se guardo producto en cotizacion");
                                }
                            }
                            lstCot = db.CatCotizacion.Where(r => r.IdCotizacion == parametros.IdCotizacion).ToList();
                            foreach (var item in lstCot)
                            {
                                CatCotizacion objCot1 = db.CatCotizacion.Where(r => r.id == item.id).FirstOrDefault();
                                objCot1.FechaCreacion = parametros.FechaCreacion;
                                objCot1.FechaTexto = ConvertirFechaALetras((DateTime)parametros.FechaCreacion);
                                objCot1.Tramo = parametros.Tramo == null ? "" : parametros.Tramo;
                                objCot1.Atencion = parametros.Atencion == null ? "" : parametros.Atencion;
                                objCot1.NombreCliente = parametros.NombreCliente == null ? "" : parametros.NombreCliente;
                                objCot1.RFCCliente = parametros.RFCCliente == null ? "" : parametros.RFCCliente;
                                objCot1.Direccion = parametros.Direccion == null ? "" : parametros.Direccion;
                                objCot1.NumINT = parametros.NumINT == null ? "" : parametros.NumINT;
                                objCot1.CP = parametros.CP == null ? "" : parametros.CP;
                                objCot1.Ciudad = parametros.Ciudad == null ? "" : parametros.Ciudad;
                                objCot1.Estado = parametros.Estado == null ? "" : parametros.Estado;
                                objCot1.Pais = parametros.Pais == null ? "" : parametros.Pais;
                                db.SaveChanges();
                                addBitacora("Edicion", "Se editaron Campos en cotizacion");
                            }
                            break;
                        case "B":
                            objCotizacion = db.CatCotizacion.Where(r => r.id == parametros.id).FirstOrDefault();
                            if (parametros.ProductoId != 12)
                            {

                                if (objCotizacion != null)
                                {
                                    objCotizacion.IdCotizacion = parametros.IdCotizacion;
                                    objCotizacion.FechaCreacion = parametros.FechaCreacion;
                                    objCotizacion.FechaTexto = ConvertirFechaALetras((DateTime)parametros.FechaCreacion);
                                    objCotizacion.ProductoId = parametros.ProductoId;
                                    objCotizacion.PrecioUnitario = parametros.PrecioUnitario;
                                    objCotizacion.PrecioVenta = parametros.Cantidad * parametros.PrecioUnitario;
                                    objCotizacion.Cantidad = parametros.Cantidad;
                                    objCotizacion.Folio = DateTime.Now.Year.ToString() + "/" + parametros.Folio;
                                    objCotizacion.Tramo = parametros.Tramo == null ? "" : parametros.Tramo;
                                    objCotizacion.Atencion = parametros.Atencion == null ? "" : parametros.Atencion;
                                    objCotizacion.NombreCliente = parametros.NombreCliente == null ? "" : parametros.NombreCliente;
                                    objCotizacion.RFCCliente = parametros.RFCCliente == null ? "" : parametros.RFCCliente;
                                    objCotizacion.Direccion = parametros.Direccion == null ? "" : parametros.Direccion;
                                    objCotizacion.NumINT = parametros.NumINT == null ? "" : parametros.NumINT;
                                    objCotizacion.CP = parametros.CP == null ? "" : parametros.CP;
                                    objCotizacion.Ciudad = parametros.Ciudad == null ? "" : parametros.Ciudad;
                                    objCotizacion.Estado = parametros.Estado == null ? "" : parametros.Estado;
                                    objCotizacion.Pais = parametros.Pais == null ? "" : parametros.Pais;
                                    objCotizacion.Medida = parametros.Medida;
                                    objCotizacion.Reflejante = parametros.Reflejante;
                                    objCotizacion.Activo = true;
                                    db.SaveChanges();
                                    addBitacora("Edicion", "Se editaron Campos en cotizacion");
                                }
                                lstCot = db.CatCotizacion.Where(r => r.IdCotizacion == parametros.IdCotizacion).ToList();
                                foreach (var item in lstCot)
                                {
                                    CatCotizacion objCot1 = db.CatCotizacion.Where(r => r.id == item.id).FirstOrDefault();
                                    objCot1.FechaCreacion = parametros.FechaCreacion;
                                    objCot1.FechaTexto = ConvertirFechaALetras((DateTime)parametros.FechaCreacion);
                                    objCot1.Tramo = parametros.Tramo == null ? "" : parametros.Tramo;
                                    objCot1.Atencion = parametros.Atencion == null ? "" : parametros.Atencion;
                                    objCot1.NombreCliente = parametros.NombreCliente == null ? "" : parametros.NombreCliente;
                                    objCot1.RFCCliente = parametros.RFCCliente == null ? "" : parametros.RFCCliente;
                                    objCot1.Direccion = parametros.Direccion == null ? "" : parametros.Direccion;
                                    objCot1.NumINT = parametros.NumINT == null ? "" : parametros.NumINT;
                                    objCot1.CP = parametros.CP == null ? "" : parametros.CP;
                                    objCot1.Ciudad = parametros.Ciudad == null ? "" : parametros.Ciudad;
                                    objCot1.Estado = parametros.Estado == null ? "" : parametros.Estado;
                                    objCot1.Pais = parametros.Pais == null ? "" : parametros.Pais;
                                    db.SaveChanges();
                                    addBitacora("Edicion", "Se editaron Campos en cotizacion");
                                }
                            }
                            break;
                        case "C":
                            lstCot = db.CatCotizacion.Where(r => r.IdCotizacion == parametros.IdCotizacion).ToList();
                            foreach (var item in lstCot)
                            {
                                CatCotizacion objCot1 = db.CatCotizacion.Where(r => r.id == item.id).FirstOrDefault();
                                objCot1.FechaCreacion = parametros.FechaCreacion;
                                objCot1.FechaTexto = ConvertirFechaALetras((DateTime)parametros.FechaCreacion);
                                objCot1.Tramo = parametros.Tramo == null ? "" : parametros.Tramo;
                                objCot1.Atencion = parametros.Atencion == null ? "" : parametros.Atencion;
                                objCot1.NombreCliente = parametros.NombreCliente == null ? "" : parametros.NombreCliente;
                                objCot1.RFCCliente = parametros.RFCCliente == null ? "" : parametros.RFCCliente;
                                objCot1.Direccion = parametros.Direccion == null ? "" : parametros.Direccion;
                                objCot1.NumINT = parametros.NumINT == null ? "" : parametros.NumINT;
                                objCot1.CP = parametros.CP == null ? "" : parametros.CP;
                                objCot1.Ciudad = parametros.Ciudad == null ? "" : parametros.Ciudad;
                                objCot1.Estado = parametros.Estado == null ? "" : parametros.Estado;
                                objCot1.Pais = parametros.Pais == null ? "" : parametros.Pais;
                                db.SaveChanges();
                                addBitacora("Edicion", "se guardo toda la cotizacion");
                            }
                            break;

                    }


                    objResultado.ITEMS = objCotizacion;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "Guardado con exito.";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        [ActionName("postSpdEliminarTodaLaCotizacion")]
        public JsonResult postSpdEliminarTodaLaCotizacion(int IdCotizacion)
        {
            objResultado = new ClsModResponse();
            try
            {
                using (db = new MEBAEntities())
                {
                    var lstCot = db.CatCotizacion.Where(r => r.IdCotizacion == IdCotizacion).ToList();
                    foreach (var item in lstCot)
                    {
                        var objCot = db.CatCotizacion.Where(r => r.id == item.id).FirstOrDefault();
                        objCot.Activo = false;
                        db.SaveChanges();
                    }
                    db.SaveChanges();
                    addBitacora("Borrado", "Se borro toda la cotizacion numero : " + AgregarCeros((int)IdCotizacion, 8));

                    objResultado.ITEMS = lstCot;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "Eliminado con exito.";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        [ActionName("postSpdEliminarSeleccionCotizacion")]
        public JsonResult postSpdEliminarSeleccionCotizacion(int id)
        {
            objResultado = new ClsModResponse();
            try
            {
                using (db = new MEBAEntities())
                {
                    var objCot = db.CatCotizacion.Where(r => r.id == id).FirstOrDefault();
                    db.CatCotizacion.Remove(objCot);
                    db.SaveChanges();
                    addBitacora("Borrado", "Se borro un producto de la cotizacion " + objCot.ProductoId);

                    objResultado.ITEMS = objCot;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "Eliminado con exito.";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        [ActionName("postCmbObtenerProductos")]
        public JsonResult postCmbObtenerProductos()
        {
            objResultado = new ClsModResponse();
            try
            {
                using (db = new MEBAEntities())
                {
                    var lstProd = db.CatProductos.ToList().Select(y => new ClsModCombos
                    {
                        value = y.id.ToString(),
                        text = y.codigo + " " + y.descripcion
                    }).ToList();

                    objResultado.ITEMS = lstProd;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }


        public static string AgregarCeros(int numero, int longitudTotal)
        {
            string numeroStr = numero.ToString();

            // Verificar si es necesario añadir ceros
            if (numeroStr.Length >= longitudTotal)
            {
                return numeroStr;
            }
            else
            {
                // Calcular cuántos ceros se deben añadir
                int cantidadCeros = longitudTotal - numeroStr.Length;
                string ceros = new string('0', cantidadCeros);

                // Concatenar los ceros al inicio del número
                string numeroFormateado = ceros + numeroStr;
                return numeroFormateado;
            }
        }


        private static readonly string[] NOMBRES_MESES = {
        "", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO",
        "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"
    };

        public static string ConvertirFechaALetras(DateTime fecha)
        {
            int dia = fecha.Day;
            int mes = fecha.Month;
            int anio = fecha.Year;

            string fechaEnLetras = $"{dia} DE {NOMBRES_MESES[mes]} DE {anio}";

            return fechaEnLetras;
        }



        [HttpPost]
        [AllowAnonymous]
        [ActionName("postExtraerUltimoFolioOC")]
        public JsonResult postExtraerUltimoFolioOC()
        {
            objResultado = new ClsModResponse();
            try
            {
                using (db = new MEBAEntities())
                {
                    var listado = db.CatOrdenDeCompra.OrderByDescending(r => r.IdCotizacion).FirstOrDefault() == null ? 0 : db.CatOrdenDeCompra.OrderByDescending(r => r.IdCotizacion).FirstOrDefault().IdCotizacion;
                    listado++;
                    objResultado.ITEMS = AgregarCeros((int)listado, 8);
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        [ActionName("postSpdObtenerListadoOrdenDeCompras")]
        public JsonResult postSpdObtenerListadoOrdenDeCompras()
        {
            objResultado = new ClsModResponse();
            try
            {
                using (db = new MEBAEntities())
                {
                    var listado = db.spdObtenerListadoOrdenDeCompras().ToList().Select(y => new DTOCot
                    {
                        IdCotizacion = y.IdCotizacion,
                        NombreCliente = y.NombreCliente,
                        RFCCliente = y.RFCCliente,
                        FechaCreacion = y.FechaCreacion,
                        Folio = y.Folio,
                        Activo = (bool)db.CatOrdenDeCompra.Where(r => r.IdCotizacion == y.IdCotizacion).FirstOrDefault().Activo,
                    }).ToList();
                    objResultado.ITEMS = listado;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [AllowAnonymous]
        [ActionName("postSpdObtenerListadoOrdenDeComprasxIdOrdenDeCompras")]
        public JsonResult postSpdObtenerListadoOrdenDeComprasxIdOrdenDeCompras(parametrosCotizacion parametros)
        {
            objResultado = new ClsModResponse();
            try
            {
                using (db = new MEBAEntities())
                {
                    var listado = db.CatOrdenDeCompra.Where(r => r.IdCotizacion == parametros.IdCotizacion).ToList().Select(y => new DTOCotizacion
                    {
                        id = y.id,
                        IdCotizacion = y.IdCotizacion,
                        FechaCreacion = y.FechaCreacion,
                        FechaTexto = y.FechaTexto,
                        ProductoId = y.ProductoId,
                        PrecioVenta = y.PrecioVenta,
                        PrecioUnitario = y.PrecioUnitario,
                        Cantidad = y.Cantidad,
                        Folio = y.Folio,
                        Tramo = y.Tramo,
                        Atencion = y.Atencion,
                        NombreCliente = y.NombreCliente,
                        RFCCliente = y.RFCCliente,
                        Direccion = y.Direccion,
                        NumINT = y.NumINT,
                        CP = y.CP,
                        Ciudad = y.Ciudad,
                        Estado = y.Estado,
                        Pais = y.Pais,
                        Medida = y.Medida,
                        Unidad = db.CatProductos.Where(r => r.id == y.ProductoId).FirstOrDefault() == null ? "" : db.CatProductos.Where(r => r.id == y.ProductoId).FirstOrDefault().unidad,
                        NombreProducto = ObtenerDescripcionProductoCompras(db.CatProductos.Where(r => r.id == y.ProductoId).FirstOrDefault(), y),
                        Observaciones = y.Oberservaciones,
                        Autorizo = y.Autorizo,
                        Solicito = y.Solicito,
                        Reflejante = y.Reflejante,
                        codigo = db.CatProductos.Where(r => r.id == y.ProductoId).FirstOrDefault() == null ? "" : db.CatProductos.Where(r => r.id == y.ProductoId).FirstOrDefault().codigo,

                    }).ToList();
                    objResultado.ITEMS = listado;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        [ActionName("postSpdAgregarModificarEliminarOrden")]
        public JsonResult postSpdAgregarModificarEliminarOrden(parametrosCotizacion parametros)
        {
            objResultado = new ClsModResponse();
            try
            {
                using (db = new MEBAEntities())
                {
                    CatOrdenDeCompra objOrdenDeCompras = new CatOrdenDeCompra();
                    CatOrdenDeCompra objRegistro = new CatOrdenDeCompra();
                    List<CatOrdenDeCompra> lstCot = new List<CatOrdenDeCompra>();
                    switch (parametros.Opcion)
                    {
                        case "A":
                            if (parametros.ProductoId == 12)
                            {
                                objOrdenDeCompras = new CatOrdenDeCompra();
                                objOrdenDeCompras.IdCotizacion = parametros.IdCotizacion;
                                objOrdenDeCompras.FechaCreacion = parametros.FechaCreacion;
                                objOrdenDeCompras.FechaTexto = ConvertirFechaALetras((DateTime)parametros.FechaCreacion);
                                objOrdenDeCompras.ProductoId = parametros.ProductoId;
                                objOrdenDeCompras.PrecioUnitario = 0;
                                objOrdenDeCompras.PrecioVenta = 0;
                                objOrdenDeCompras.Cantidad = 0;
                                objOrdenDeCompras.Folio = DateTime.Now.Year.ToString() + "/" + parametros.Folio;
                                objOrdenDeCompras.Tramo = parametros.Tramo == null ? "" : parametros.Tramo;
                                objOrdenDeCompras.Atencion = parametros.Atencion == null ? "" : parametros.Atencion;
                                objOrdenDeCompras.NombreCliente = parametros.NombreCliente == null ? "" : parametros.NombreCliente;
                                objOrdenDeCompras.RFCCliente = parametros.RFCCliente == null ? "" : parametros.RFCCliente;
                                objOrdenDeCompras.Direccion = parametros.Direccion == null ? "" : parametros.Direccion;
                                objOrdenDeCompras.NumINT = parametros.NumINT == null ? "" : parametros.NumINT;
                                objOrdenDeCompras.CP = parametros.CP == null ? "" : parametros.CP;
                                objOrdenDeCompras.Ciudad = parametros.Ciudad == null ? "" : parametros.Ciudad;
                                objOrdenDeCompras.Estado = parametros.Estado == null ? "" : parametros.Estado;
                                objOrdenDeCompras.Pais = parametros.Pais == null ? "" : parametros.Pais;
                                objOrdenDeCompras.Medida = "";
                                objOrdenDeCompras.Activo = true;
                                objOrdenDeCompras.Oberservaciones = parametros.Observaciones == null ? "" : parametros.Observaciones;
                                objOrdenDeCompras.Solicito = parametros.Solicito == null ? "" : parametros.Solicito;
                                objOrdenDeCompras.Autorizo = parametros.Autorizo == null ? "" : parametros.Autorizo;
                                objOrdenDeCompras.Reflejante = parametros.Reflejante == null ? "" : parametros.Reflejante;
                                db.CatOrdenDeCompra.Add(objOrdenDeCompras);
                                db.SaveChanges();
                                addBitacora("Guardado", "Se guardaron Campos en orden de compra");
                            }
                            else
                            {
                                objRegistro = db.CatOrdenDeCompra.Where(r => r.ProductoId == parametros.ProductoId && r.IdCotizacion == parametros.IdCotizacion && r.Medida == parametros.Medida).FirstOrDefault();
                                if (objRegistro == null)
                                {
                                    objOrdenDeCompras = new CatOrdenDeCompra();
                                    objOrdenDeCompras.IdCotizacion = parametros.IdCotizacion;
                                    objOrdenDeCompras.FechaCreacion = parametros.FechaCreacion;
                                    objOrdenDeCompras.FechaTexto = ConvertirFechaALetras((DateTime)parametros.FechaCreacion);
                                    objOrdenDeCompras.ProductoId = parametros.ProductoId;
                                    objOrdenDeCompras.PrecioUnitario = parametros.PrecioUnitario;
                                    objOrdenDeCompras.PrecioVenta = parametros.Cantidad * parametros.PrecioUnitario;
                                    objOrdenDeCompras.Cantidad = parametros.Cantidad;
                                    objOrdenDeCompras.Folio = DateTime.Now.Year.ToString() + "/" + parametros.Folio;
                                    objOrdenDeCompras.Tramo = parametros.Tramo == null ? "" : parametros.Tramo;
                                    objOrdenDeCompras.Atencion = parametros.Atencion == null ? "" : parametros.Atencion;
                                    objOrdenDeCompras.NombreCliente = parametros.NombreCliente == null ? "" : parametros.NombreCliente;
                                    objOrdenDeCompras.RFCCliente = parametros.RFCCliente == null ? "" : parametros.RFCCliente;
                                    objOrdenDeCompras.Direccion = parametros.Direccion == null ? "" : parametros.Direccion;
                                    objOrdenDeCompras.NumINT = parametros.NumINT == null ? "" : parametros.NumINT;
                                    objOrdenDeCompras.CP = parametros.CP == null ? "" : parametros.CP;
                                    objOrdenDeCompras.Ciudad = parametros.Ciudad == null ? "" : parametros.Ciudad;
                                    objOrdenDeCompras.Estado = parametros.Estado == null ? "" : parametros.Estado;
                                    objOrdenDeCompras.Pais = parametros.Pais == null ? "" : parametros.Pais;
                                    objOrdenDeCompras.Medida = "";
                                    objOrdenDeCompras.Activo = true;
                                    objOrdenDeCompras.Oberservaciones = parametros.Observaciones == null ? "" : parametros.Observaciones;
                                    objOrdenDeCompras.Solicito = parametros.Solicito == null ? "" : parametros.Solicito;
                                    objOrdenDeCompras.Autorizo = parametros.Autorizo == null ? "" : parametros.Autorizo;
                                    objOrdenDeCompras.Reflejante = parametros.Reflejante == null ? "" : parametros.Reflejante;
                                    db.CatOrdenDeCompra.Add(objOrdenDeCompras);
                                    db.SaveChanges();
                                    addBitacora("Guardado", "Se guardaron Campos en orden de compra");
                                }
                            }
                            lstCot = db.CatOrdenDeCompra.Where(r => r.IdCotizacion == parametros.IdCotizacion).ToList();
                            foreach (var item in lstCot)
                            {
                                CatOrdenDeCompra objCot1 = db.CatOrdenDeCompra.Where(r => r.id == item.id).FirstOrDefault();
                                objCot1.FechaCreacion = parametros.FechaCreacion;
                                objCot1.FechaTexto = ConvertirFechaALetras((DateTime)parametros.FechaCreacion);
                                objCot1.Tramo = parametros.Tramo == null ? "" : parametros.Tramo;
                                objCot1.Atencion = parametros.Atencion == null ? "" : parametros.Atencion;
                                objCot1.NombreCliente = parametros.NombreCliente == null ? "" : parametros.NombreCliente;
                                objCot1.RFCCliente = parametros.RFCCliente == null ? "" : parametros.RFCCliente;
                                objCot1.Direccion = parametros.Direccion == null ? "" : parametros.Direccion;
                                objCot1.NumINT = parametros.NumINT == null ? "" : parametros.NumINT;
                                objCot1.CP = parametros.CP == null ? "" : parametros.CP;
                                objCot1.Ciudad = parametros.Ciudad == null ? "" : parametros.Ciudad;
                                objCot1.Estado = parametros.Estado == null ? "" : parametros.Estado;
                                objCot1.Pais = parametros.Pais == null ? "" : parametros.Pais;
                                objCot1.Oberservaciones = parametros.Observaciones == null ? "" : parametros.Observaciones;
                                objCot1.Solicito = parametros.Solicito == null ? "" : parametros.Solicito;
                                objCot1.Autorizo = parametros.Autorizo == null ? "" : parametros.Autorizo;
                                objCot1.Reflejante = parametros.Reflejante == null ? "" : parametros.Reflejante;
                                db.SaveChanges();
                                addBitacora("Edicion", "Se editaron Campos en orden de compra");
                            }
                            break;
                        case "B":
                            objOrdenDeCompras = db.CatOrdenDeCompra.Where(r => r.id == parametros.id).FirstOrDefault();
                            if (parametros.ProductoId != 12)
                            {

                                if (objOrdenDeCompras != null)
                                {
                                    objOrdenDeCompras.IdCotizacion = parametros.IdCotizacion;
                                    objOrdenDeCompras.FechaCreacion = parametros.FechaCreacion;
                                    objOrdenDeCompras.FechaTexto = ConvertirFechaALetras((DateTime)parametros.FechaCreacion);
                                    objOrdenDeCompras.ProductoId = parametros.ProductoId;
                                    objOrdenDeCompras.PrecioUnitario = parametros.PrecioUnitario;
                                    objOrdenDeCompras.PrecioVenta = parametros.Cantidad * parametros.PrecioUnitario;
                                    objOrdenDeCompras.Cantidad = parametros.Cantidad;
                                    objOrdenDeCompras.Folio = DateTime.Now.Year.ToString() + "/" + parametros.Folio;
                                    objOrdenDeCompras.Tramo = parametros.Tramo == null ? "" : parametros.Tramo;
                                    objOrdenDeCompras.Atencion = parametros.Atencion == null ? "" : parametros.Atencion;
                                    objOrdenDeCompras.NombreCliente = parametros.NombreCliente == null ? "" : parametros.NombreCliente;
                                    objOrdenDeCompras.RFCCliente = parametros.RFCCliente == null ? "" : parametros.RFCCliente;
                                    objOrdenDeCompras.Direccion = parametros.Direccion == null ? "" : parametros.Direccion;
                                    objOrdenDeCompras.NumINT = parametros.NumINT == null ? "" : parametros.NumINT;
                                    objOrdenDeCompras.CP = parametros.CP == null ? "" : parametros.CP;
                                    objOrdenDeCompras.Ciudad = parametros.Ciudad == null ? "" : parametros.Ciudad;
                                    objOrdenDeCompras.Estado = parametros.Estado == null ? "" : parametros.Estado;
                                    objOrdenDeCompras.Pais = parametros.Pais == null ? "" : parametros.Pais;
                                    objOrdenDeCompras.Medida = "";
                                    objOrdenDeCompras.Activo = true;
                                    objOrdenDeCompras.Oberservaciones = parametros.Observaciones == null ? "" : parametros.Observaciones;
                                    objOrdenDeCompras.Solicito = parametros.Solicito == null ? "" : parametros.Solicito;
                                    objOrdenDeCompras.Autorizo = parametros.Autorizo == null ? "" : parametros.Autorizo;
                                    objOrdenDeCompras.Reflejante = parametros.Reflejante == null ? "" : parametros.Reflejante;
                                    db.SaveChanges();
                                    addBitacora("Edicion", "Se editaron Campos en orden de compra");
                                }
                                lstCot = db.CatOrdenDeCompra.Where(r => r.IdCotizacion == parametros.IdCotizacion).ToList();
                                foreach (var item in lstCot)
                                {
                                    CatOrdenDeCompra objCot1 = db.CatOrdenDeCompra.Where(r => r.id == item.id).FirstOrDefault();
                                    objCot1.FechaCreacion = parametros.FechaCreacion;
                                    objCot1.FechaTexto = ConvertirFechaALetras((DateTime)parametros.FechaCreacion);
                                    objCot1.Tramo = parametros.Tramo == null ? "" : parametros.Tramo;
                                    objCot1.Atencion = parametros.Atencion == null ? "" : parametros.Atencion;
                                    objCot1.NombreCliente = parametros.NombreCliente == null ? "" : parametros.NombreCliente;
                                    objCot1.RFCCliente = parametros.RFCCliente == null ? "" : parametros.RFCCliente;
                                    objCot1.Direccion = parametros.Direccion == null ? "" : parametros.Direccion;
                                    objCot1.NumINT = parametros.NumINT == null ? "" : parametros.NumINT;
                                    objCot1.CP = parametros.CP == null ? "" : parametros.CP;
                                    objCot1.Ciudad = parametros.Ciudad == null ? "" : parametros.Ciudad;
                                    objCot1.Estado = parametros.Estado == null ? "" : parametros.Estado;
                                    objCot1.Pais = parametros.Pais == null ? "" : parametros.Pais;
                                    objCot1.Oberservaciones = parametros.Observaciones == null ? "" : parametros.Observaciones;
                                    objCot1.Solicito = parametros.Solicito == null ? "" : parametros.Solicito;
                                    objCot1.Autorizo = parametros.Autorizo == null ? "" : parametros.Autorizo;
                                    objCot1.Reflejante = parametros.Reflejante == null ? "" : parametros.Reflejante;
                                    db.SaveChanges();
                                    addBitacora("Edicion", "Se editaron Campos en orden de compra");
                                }
                            }
                            break;
                        case "C":
                            lstCot = db.CatOrdenDeCompra.Where(r => r.IdCotizacion == parametros.IdCotizacion).ToList();
                            foreach (var item in lstCot)
                            {
                                CatOrdenDeCompra objCot1 = db.CatOrdenDeCompra.Where(r => r.id == item.id).FirstOrDefault();
                                objCot1.FechaCreacion = parametros.FechaCreacion;
                                objCot1.FechaTexto = ConvertirFechaALetras((DateTime)parametros.FechaCreacion);
                                objCot1.Tramo = parametros.Tramo == null ? "" : parametros.Tramo;
                                objCot1.Atencion = parametros.Atencion == null ? "" : parametros.Atencion;
                                objCot1.NombreCliente = parametros.NombreCliente == null ? "" : parametros.NombreCliente;
                                objCot1.RFCCliente = parametros.RFCCliente == null ? "" : parametros.RFCCliente;
                                objCot1.Direccion = parametros.Direccion == null ? "" : parametros.Direccion;
                                objCot1.NumINT = parametros.NumINT == null ? "" : parametros.NumINT;
                                objCot1.CP = parametros.CP == null ? "" : parametros.CP;
                                objCot1.Ciudad = parametros.Ciudad == null ? "" : parametros.Ciudad;
                                objCot1.Estado = parametros.Estado == null ? "" : parametros.Estado;
                                objCot1.Pais = parametros.Pais == null ? "" : parametros.Pais;
                                objCot1.Oberservaciones = parametros.Observaciones == null ? "" : parametros.Observaciones;
                                objCot1.Solicito = parametros.Solicito == null ? "" : parametros.Solicito;
                                objCot1.Autorizo = parametros.Autorizo == null ? "" : parametros.Autorizo;
                                objCot1.Reflejante = parametros.Reflejante == null ? "" : parametros.Reflejante;
                                db.SaveChanges();
                                addBitacora("Edicion", "Se editaron Campos en orden de compra");
                            }
                            break;

                    }


                    objResultado.ITEMS = objOrdenDeCompras;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "Guardado con exito.";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        [ActionName("postSpdEliminarTodaLaOrdenDeCompras")]
        public JsonResult postSpdEliminarTodaLaOrdenDeCompras(int IdCotizacion)
        {
            objResultado = new ClsModResponse();
            try
            {
                using (db = new MEBAEntities())
                {
                    var lstCot = db.CatOrdenDeCompra.Where(r => r.IdCotizacion == IdCotizacion).ToList();
                    foreach (var item in lstCot)
                    {
                        var objCot = db.CatOrdenDeCompra.Where(r => r.id == item.id).FirstOrDefault();
                        objCot.Activo = false;
                        db.SaveChanges();
                    }

                    db.SaveChanges();
                    addBitacora("Borrado", "Se borro toda la cotizacion numero : " + AgregarCeros((int)IdCotizacion, 8));

                    objResultado.ITEMS = lstCot;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "Eliminado con exito.";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        [ActionName("postSpdEliminarSeleccionOrdenDeCompras")]
        public JsonResult postSpdEliminarSeleccionOrdenDeCompras(int id)
        {
            objResultado = new ClsModResponse();
            try
            {
                using (db = new MEBAEntities())
                {
                    var objCot = db.CatOrdenDeCompra.Where(r => r.id == id).FirstOrDefault();
                    db.CatOrdenDeCompra.Remove(objCot);
                    db.SaveChanges();
                    addBitacora("Borrado", "Se borro un producto de la orden de compra " + objCot.ProductoId);

                    objResultado.ITEMS = objCot;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "Eliminado con exito.";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [ActionName("postSpdBuscarCliente")]
        public JsonResult postSpdBuscarCliente()
        {
            objResultado = new ClsModResponse();
            try
            {
                using (db = new MEBAEntities())
                {
                    var lstClientes = db.spdBuscarCliente().ToList();

                    objResultado.ITEMS = lstClientes;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "Eliminado con exito.";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }







    }
}