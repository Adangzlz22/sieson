﻿using MEBA.Models.ModGenerales;
using MEBA.Models;
using MEBA.Reportes;
using System;
using System.IO;
using System.Web.Http;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.Mvc;
using DevExpress.Data.ODataLinq.Helpers;
using System.Linq;
using MEBA.Clases;
using MEBA.Models.ds;
using MEBA.Models.ModCotizacion;
using MEBA.Models.ModOT;
using System.Collections.Generic;
using DevExpress.Office.Utils;
using DevExpress.CodeParser;
using Microsoft.Ajax.Utilities;

namespace MEBA.Controllers
{
    public class ReportesCrysController : Controller
    {
        private ReportDocument rd = new ReportDocument();
        MEBAEntities db = new MEBAEntities();
        int consecutivo = 0;

        public ActionResult postObtenerReporte(parametrosReportes parametros)
        {
            ClsModResponse clsModResponse = new ClsModResponse();
            try
            {
                clsModResponse = setReporteInfo(parametros);
            }
            catch (Exception ex)
            {
                clsModResponse.ITEMS = null;
                clsModResponse.MENSAJE = ex.Message.ToString();
                clsModResponse.SUCCESS = false;
            }
            return Json(clsModResponse, JsonRequestBehavior.AllowGet);
        }
        public double obtenerDecimal(double numero)
        {
            double numeroRedondeado = Math.Round(numero, 2);
            return numeroRedondeado;
        }
        public ClsModResponse setReporteInfo(parametrosReportes parametros)
        {
            ClsModResponse clsModResponse = new ClsModResponse();
            int contador = 0;
            MemoryStream ms = new MemoryStream();
            Stream StreamPDF;
            Stream StreamPDFs;
            string ruta = "";
            string Archivo = "";
            byte[] buffer;
            string FECHAACTUALAño = DateTime.Now.Year.ToString().Substring(2, 2);
            string FECHAACTUAL = DateTime.Now.ToString("dd-MM-yyyy");
            int a = 1;
            double total = 0;
            double cantidad = 0;
            string[] nacion = new string[0];
            string n = "";
            try
            {

                switch (parametros.IdReporte)
                {
                    case (int)ReportesEnum.Cotizacion:

                        rd = new rptCotizacionNueva();
                        var lstCotizacion = db.CatCotizacion.Where(r => r.IdCotizacion == parametros.IdCotizacion).ToList().Select(r => new ModCotizacion
                        {
                            id = r.id,
                            IdCotizacion = (int)r.IdCotizacion,
                            FechaCreacion = (DateTime)r.FechaCreacion,
                            FechaTexto = r.FechaTexto,
                            ProductoId = (int)r.ProductoId,
                            PrecioVenta = (decimal)r.PrecioVenta,
                            Cantidad = (decimal)r.Cantidad,
                            Folio = r.Folio,
                            Tramo = r.Tramo,
                            Atencion = r.Atencion,
                            NombreCliente = r.NombreCliente,
                            RFCCliente = r.RFCCliente,
                            Direccion = r.Direccion,
                            NumINT = r.NumINT,
                            CP = r.CP,
                            Ciudad = r.Ciudad,
                            Estado = r.Estado,
                            Pais = r.Pais,
                            Activo = (bool)r.Activo,
                            PrecioProducto = (decimal)r.PrecioUnitario,
                            NombreProducto = ObtenerDescripcionProducto(db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault(), r),
                            Unidad = (db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault() == null ? "" : db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault().unidad),
                            img = ObtenerImagen(db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault() == null ? "" : db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault().img),
                            codigo = (db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault() == null ? "" : db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault().codigoP),

                        }).ToList();
                        a = 1;
                        total = 0;
                        foreach (var item in lstCotizacion)
                        {
                            item.id = a++;
                            total += Convert.ToDouble(item.PrecioVenta);
                        }
                        cantidad = (total * .16) + total;
                        cantidad = obtenerDecimal(cantidad);
                        nacion = cantidad.ToString().Split('.');
                        n = "";
                        if (nacion.Length > 1)
                        {
                            n = nacion[1];
                        }
                        else
                        {
                            n = "00";
                        }
                        if (Convert.ToInt32(n) >= 1 && Convert.ToInt32(n) <= 9)
                        {
                            n = nacion[1] + "0";
                        }
                        //rd.Database.Tables[0].SetDataSource(lstGeneral);
                        rd.Database.Tables[0].SetDataSource(obtenerEncabezado());
                        rd.Database.Tables[1].SetDataSource(lstCotizacion);
                        rd.SetParameterValue("FechaConLetra", "HERMOSILLO, SONORA " + ConvertirFechaALetras(lstCotizacion.FirstOrDefault().FechaCreacion));
                        rd.SetParameterValue("DineroConLetra", "SON - " + ConvertirNumeroALetras(cantidad) + " " + n + "/100 MN");
                        rd.SetParameterValue("subtotal", total);
                        rd.SetParameterValue("total", cantidad);
                        rd.SetParameterValue("iva", total * .16);
                        StreamPDF = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                        buffer = new byte[StreamPDF.Length];
                        StreamPDF.Seek(0, SeekOrigin.Begin);
                        StreamPDF.CopyTo(ms);
                        addBitacora("Reporte", "Generación de la cotización del usuario : " + vSessiones.sesionUsuarioDTO.Nombre);
                        //    ruta = "C:\\ValesCorte";
                        //    Archivo = "C:\\ValesCorte\\" + NombreDocumento + "-" + Primero + "-" + Ultimo + "-" + cont + ".pdf";

                        //    if (!Directory.Exists(ruta))
                        //        Directory.CreateDirectory(ruta);
                        //    StreamPDF.Read(buffer, 0, buffer.Length);
                        //    File.WriteAllBytes(Archivo, buffer);
                        break;
                    case (int)ReportesEnum.OrdenesDeCompra:

                        rd = new rptOrdenDeCompra();
                        var lstCatOrdenDeCompra = db.CatOrdenDeCompra.Where(r => r.IdCotizacion == parametros.IdCotizacion).ToList().Select(r => new ModOrdenDeCompras
                        {
                            id = r.id,
                            IdCotizacion = (int)r.IdCotizacion,
                            FechaCreacion = (DateTime)r.FechaCreacion,
                            FechaTexto = r.FechaTexto,
                            ProductoId = (int)r.ProductoId,
                            PrecioVenta = (decimal)r.PrecioVenta,
                            Cantidad = (decimal)r.Cantidad,
                            Folio = r.Folio,
                            Tramo = r.Tramo,
                            Atencion = r.Atencion,
                            NombreCliente = r.NombreCliente,
                            RFCCliente = r.RFCCliente,
                            Direccion = r.Direccion,
                            NumINT = r.NumINT,
                            CP = r.CP,
                            Ciudad = r.Ciudad,
                            Estado = r.Estado,
                            Pais = r.Pais,
                            Activo = (bool)r.Activo,
                            PrecioProducto = (decimal)r.PrecioUnitario,
                            NombreProducto = ObtenerDescripcionProductoCompras(db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault(), r),
                            Unidad = (db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault() == null ? "" : db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault().unidad),
                            img = ObtenerImagen(db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault() == null ? "" : db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault().img),
                            Observaciones = r.Oberservaciones,
                            Autorizo = r.Autorizo,
                            Solicito = r.Solicito
                        }).ToList();
                        a = 1;
                        total = 0;
                        foreach (var item in lstCatOrdenDeCompra)
                        {
                            item.id = a++;
                            total += Convert.ToDouble(item.PrecioVenta);
                        }
                        cantidad = (total * .16) + total;
                        cantidad = obtenerDecimal(cantidad);

                        nacion = cantidad.ToString().Split('.');
                        n = "";
                        if (nacion.Length > 1)
                        {
                            n = nacion[1];
                        }
                        else
                        {
                            n = "00";
                        }
                        if (Convert.ToInt32(n) >= 1 && Convert.ToInt32(n) <= 9)
                        {
                            n = nacion[1] + "0";
                        }
                        //rd.Database.Tables[0].SetDataSource(lstGeneral);
                        rd.Database.Tables[0].SetDataSource(obtenerEncabezado());
                        rd.Database.Tables[1].SetDataSource(lstCatOrdenDeCompra);
                        rd.SetParameterValue("FechaConLetra", "HERMOSILLO, SONORA " + ConvertirFechaALetras(lstCatOrdenDeCompra.FirstOrDefault().FechaCreacion));
                        rd.SetParameterValue("DineroConLetra", "SON - " + ConvertirNumeroALetras(cantidad) + " " + n + "/100 MN");
                        rd.SetParameterValue("subtotal", total);
                        rd.SetParameterValue("total", cantidad);
                        rd.SetParameterValue("iva", total * .16);
                        rd.SetParameterValue("Oberservaciones", lstCatOrdenDeCompra.FirstOrDefault().Observaciones);
                        rd.SetParameterValue("AUTORIZO", lstCatOrdenDeCompra.FirstOrDefault().Autorizo);
                        rd.SetParameterValue("SOLICITO", lstCatOrdenDeCompra.FirstOrDefault().Solicito);
                        StreamPDF = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                        buffer = new byte[StreamPDF.Length];
                        StreamPDF.Seek(0, SeekOrigin.Begin);
                        StreamPDF.CopyTo(ms);
                        addBitacora("Reporte", "Generación de la orden de compra del usuario : " + vSessiones.sesionUsuarioDTO.Nombre);
                        //    ruta = "C:\\ValesCorte";
                        //    Archivo = "C:\\ValesCorte\\" + NombreDocumento + "-" + Primero + "-" + Ultimo + "-" + cont + ".pdf";

                        //    if (!Directory.Exists(ruta))
                        //        Directory.CreateDirectory(ruta);
                        //    StreamPDF.Read(buffer, 0, buffer.Length);
                        //    File.WriteAllBytes(Archivo, buffer);

                        break;
                    case (int)ReportesEnum.OrdenDeTrabajo:

                        rd = new rptOrdenDeTrabajo();
                        var lstOrdenDeTrabajo = db.CatCotizacion.Where(r => r.IdCotizacion == parametros.IdCotizacion).ToList().Select(r => new ModOt
                        {
                            id = r.id,
                            IdCotizacion = (int)r.IdCotizacion,
                            FechaCreacion = (DateTime)r.FechaCreacion,
                            FechaTexto = r.FechaTexto,
                            ProductoId = (int)r.ProductoId,
                            PrecioVenta = (decimal)r.PrecioVenta,
                            Cantidad = (int)r.Cantidad,
                            Folio = r.Folio,
                            Tramo = r.Tramo,
                            Atencion = r.Atencion,
                            NombreCliente = r.NombreCliente,
                            RFCCliente = r.RFCCliente,
                            Direccion = r.Direccion,
                            NumINT = r.NumINT,
                            CP = r.CP,
                            Ciudad = r.Ciudad,
                            Estado = r.Estado,
                            Pais = r.Pais,
                            Activo = (bool)r.Activo,
                            PrecioProducto = (decimal)r.PrecioUnitario,
                            NombreProducto = ObtenerDescripcionProducto(db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault(), r),
                            Unidad = (db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault() == null ? "" : db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault().unidad),
                            img = ObtenerImagen(db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault() == null ? "" : db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault().img)
                            ,
                            Codigo = (db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault() == null ? "" : db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault().codigo)
                            ,
                            Medida = r.Medida
                            ,
                            Reflejante = r.Reflejante
                            ,
                            CodigoP = (db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault() == null ? "" : db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault().codigoP)
                        }).ToList();
                        int conta = 0;
                        foreach (var item in lstOrdenDeTrabajo)
                        {
                            conta++;
                            item.id = conta;
                        }
                        var lstOrT = DetectarMultiplesYClasificar(lstOrdenDeTrabajo);

                        //rd.Database.Tables[0].SetDataSource(lstGeneral);
                        rd.Database.Tables[0].SetDataSource(obtenerEncabezado());
                        rd.Database.Tables[1].SetDataSource(lstOrT);
                        rd.SetParameterValue("Fecha", "HERMOSILLO, SONORA " + ConvertirFechaALetras(lstOrdenDeTrabajo.FirstOrDefault().FechaCreacion));
                        rd.SetParameterValue("Folio", lstOrdenDeTrabajo.FirstOrDefault().Folio);
                        //rd.SetParameterValue("DineroConLetra", "SON - " + ConvertirNumeroALetras(cantidad) + " " + n + "/100 MN");
                        //rd.SetParameterValue("subtotal", total);
                        //rd.SetParameterValue("total", cantidad);
                        //rd.SetParameterValue("iva", total * .16);
                        StreamPDF = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                        buffer = new byte[StreamPDF.Length];
                        StreamPDF.Seek(0, SeekOrigin.Begin);
                        StreamPDF.CopyTo(ms);
                        addBitacora("Reporte", "Generación de la orden de trabajo del usuario : " + vSessiones.sesionUsuarioDTO.Nombre);
                        //    ruta = "C:\\ValesCorte";
                        //    Archivo = "C:\\ValesCorte\\" + NombreDocumento + "-" + Primero + "-" + Ultimo + "-" + cont + ".pdf";

                        //    if (!Directory.Exists(ruta))
                        //        Directory.CreateDirectory(ruta);
                        //    StreamPDF.Read(buffer, 0, buffer.Length);
                        //    File.WriteAllBytes(Archivo, buffer);
                        break;
                    case (int)ReportesEnum.EnvioCorreoCotizacion:

                        rd = new rptCotizacionNueva();
                        var lstCotizacionCorreo = db.CatCotizacion.Where(r => r.IdCotizacion == parametros.IdCotizacion).ToList().Select(r => new ModCotizacion
                        {
                            id = r.id,
                            IdCotizacion = (int)r.IdCotizacion,
                            FechaCreacion = (DateTime)r.FechaCreacion,
                            FechaTexto = r.FechaTexto,
                            ProductoId = (int)r.ProductoId,
                            PrecioVenta = (decimal)r.PrecioVenta,
                            Cantidad = (int)r.Cantidad,
                            Folio = r.Folio,
                            Tramo = r.Tramo,
                            Atencion = r.Atencion,
                            NombreCliente = r.NombreCliente,
                            RFCCliente = r.RFCCliente,
                            Direccion = r.Direccion,
                            NumINT = r.NumINT,
                            CP = r.CP,
                            Ciudad = r.Ciudad,
                            Estado = r.Estado,
                            Pais = r.Pais,
                            Activo = (bool)r.Activo,
                            PrecioProducto = (decimal)r.PrecioUnitario,
                            NombreProducto = ObtenerDescripcionProducto(db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault(), r),
                            Unidad = (db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault() == null ? "" : db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault().unidad),
                            img = ObtenerImagen(db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault() == null ? "" : db.CatProductos.Where(y => y.id == r.ProductoId).FirstOrDefault().img)
                        }).ToList();
                        a = 1;
                        total = 0;
                        foreach (var item in lstCotizacionCorreo)
                        {
                            item.id = a++;
                            total += Convert.ToDouble(item.PrecioVenta);
                        }
                        cantidad = (total * .16) + total;
                        cantidad = obtenerDecimal(cantidad);

                        nacion = cantidad.ToString().Split('.');
                        n = "";
                        if (nacion.Length > 1)
                        {
                            n = nacion[1];
                        }
                        else
                        {
                            n = "00";
                        }
                        if (Convert.ToInt32(n) >= 1 && Convert.ToInt32(n) <= 9)
                        {
                            n = nacion[1] + "0";
                        }
                        //rd.Database.Tables[0].SetDataSource(lstGeneral);
                        rd.Database.Tables[0].SetDataSource(obtenerEncabezado());
                        rd.Database.Tables[1].SetDataSource(lstCotizacionCorreo);
                        rd.SetParameterValue("FechaConLetra", "HERMOSILLO, SONORA " + ConvertirFechaALetras(lstCotizacionCorreo.FirstOrDefault().FechaCreacion));
                        rd.SetParameterValue("DineroConLetra", "SON - " + ConvertirNumeroALetras(cantidad) + " " + n + "/100 MN");
                        rd.SetParameterValue("subtotal", total);
                        rd.SetParameterValue("total", cantidad);
                        rd.SetParameterValue("iva", total * .16);
                        StreamPDF = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                        buffer = new byte[StreamPDF.Length];
                        StreamPDF.Seek(0, SeekOrigin.Begin);
                        StreamPDF.CopyTo(ms);

                        parametrosCorreos parametros3 = new parametrosCorreos();
                        //parametros3.Archivo = ms;
                        //parametros3.SMTPPuerto = 8889;
                        //parametros3.SMTPServidor = "mail5016.site4now.net";
                        //parametros3.SMTPUsuario = "cotizacion@seison.com.mx";
                        //parametros3.SMTPUsuarioContrasenia = "Tuj3f4ak0n.";
                        //parametros3.HTMLDestinatario = "adangzlz22@gmail.com";
                        //parametros3.HTMLDestinatarioCopia = "adangzlz22@gmail.com";
                        //parametros3.SMTPSSL = true;

                        parametros3.Archivo = ConvertToBase64(ms);

                        parametros3.SMTPPuerto = 587;
                        parametros3.SMTPServidor = "smtp.gmail.com";
                        parametros3.SMTPUsuario = "seison.cotizacion@gmail.com";
                        parametros3.SMTPUsuarioContrasenia = "d d w q r y p f o v a h h b x d";
                        parametros3.Asunto = "Envio de correo de seison a " + (lstCotizacionCorreo.FirstOrDefault().NombreCliente == null ? "" : lstCotizacionCorreo.FirstOrDefault().NombreCliente);
                        parametros3.HTMLDestinatario = parametros.destinatario;
                        parametros3.HTMLDestinatarioCopia = parametros.destinatarioAdjunto;
                        parametros3.SMTPSSL = true;

                        EnviosDeCorreo.EnviarCorreoParaAcceso(parametros3);
                        addBitacora("Correos", "Envió de correo del usuario : " + vSessiones.sesionUsuarioDTO.Nombre);
                        //    ruta = "C:\\ValesCorte";
                        //    Archivo = "C:\\ValesCorte\\" + NombreDocumento + "-" + Primero + "-" + Ultimo + "-" + cont + ".pdf";

                        //    if (!Directory.Exists(ruta))
                        //        Directory.CreateDirectory(ruta);
                        //    StreamPDF.Read(buffer, 0, buffer.Length);
                        //    File.WriteAllBytes(Archivo, buffer);
                        break;

                }






                clsModResponse.ITEMS = ConvertToBase64(ms);
                clsModResponse.MENSAJE = "";
                clsModResponse.SUCCESS = true;
                ms.Close();
                rd.Close();

            }
            catch (Exception ex)
            {
                clsModResponse.ITEMS = ex.Message;
            }
            return clsModResponse;
        }

        public string ObtenerDescripcionProductoCompras(CatProductos objProductos, CatOrdenDeCompra r)
        {
            string descripcion = "";
            if (objProductos != null)
            {
                if (objProductos.codigo != null && objProductos.codigo != "")
                {
                    descripcion += objProductos.codigo + ", ";
                }
                else
                {
                    descripcion = descripcion.Replace(',', ' ');
                }
                if (r.Medida != null && r.Medida != "")
                {
                    descripcion += r.Medida + ", ";
                }
                else
                {
                    descripcion = descripcion.Replace(',', ' ');
                }
                if (r.Reflejante != null && r.Reflejante != "")
                {
                    descripcion += r.Reflejante + ", ";
                }
                else
                {
                    descripcion = descripcion.Replace(',', ' ');
                }
                if (objProductos.descripcion != null && objProductos.descripcion != "")
                {
                    descripcion += objProductos.descripcion;
                }

            }
            return descripcion;
        }
        public string ObtenerDescripcionProducto(CatProductos objProductos, CatCotizacion r)
        {
            string descripcion = "";
            if (objProductos != null)
            {
                if (objProductos.codigo != null && objProductos.codigo != "")
                {
                    descripcion += objProductos.codigo + " ";
                }
                else
                {
                    descripcion = descripcion.Replace(',', ' ');
                }

                if (r.Medida != null && r.Medida != "")
                {
                    descripcion += r.Medida + ", ";
                }
                else
                {
                    descripcion = descripcion.Replace(',', ' ');
                }
                if (r.Reflejante != null && r.Reflejante != "")
                {
                    descripcion += r.Reflejante + ", ";
                }
                else
                {
                    descripcion = descripcion.Replace(',', ' ');
                }
                if (objProductos.descripcion != null && objProductos.descripcion != "")
                {
                    descripcion += objProductos.descripcion;
                }

            }
            return descripcion;
        }

        public byte[] ObtenerImagen(string _Ruta)
        {
            byte[] imagen = new byte[0];
            try
            {
                imagen = System.IO.File.ReadAllBytes(_Ruta);
            }
            catch (Exception)
            {
                imagen = new byte[0];
            }
            return imagen;
        }

        public string addBitacora(string tipo, string descripcion)
        {
            string guardado = "";
            try
            {
                MEBABitacoraAcc objBitacora = new MEBABitacoraAcc();
                objBitacora.Fecha = DateTime.Now;
                objBitacora.Tipo = tipo;
                objBitacora.Origen = "";
                objBitacora.Usuario = vSessiones.sesionUsuarioDTO.UserName;
                objBitacora.Grupo = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                objBitacora.Zona = "";
                objBitacora.Estacion = "(N/A)";
                objBitacora.Descripcion = descripcion;
                objBitacora.IdAsociado = vSessiones.sessionUsuarioCONFIGDTO.IDAsociado;
                db.MEBABitacoraAcc.Add(objBitacora);
                db.SaveChanges();
                guardado = "exitoso";
            }
            catch (Exception ex)
            {
                guardado = "";
            }
            return guardado;
        }



        static List<DTOOrdenDeTrabajo> DetectarMultiplesYClasificar(List<ModOt> lst)
        {

            List<DTOOrdenDeTrabajo> registros = new List<DTOOrdenDeTrabajo>();
            DTOOrdenDeTrabajo obj = new DTOOrdenDeTrabajo();
            for (int i = 0; i < lst.Count; i++)
            {
                int residuo = lst[i].id % 4;
                if (residuo == 1)
                {
                    obj = new DTOOrdenDeTrabajo();
                    obj.imagen1 = lst[i].img;
                    obj.cantidad1 = (lst[i].Cantidad == null ? "" : lst[i].Cantidad.ToString()) + " " + (lst[i].Unidad == null ? "" : lst[i].Unidad.ToString());
                    obj.medida1 = lst[i].Medida == null ? "" : lst[i].Medida.ToString();
                    obj.codigo1 = lst[i].CodigoP == null ? "" : lst[i].CodigoP.ToString();
                    obj.reflejante1 = lst[i].Reflejante == null ? "" : lst[i].Reflejante.ToString();
                    if ((i + 1) == lst.Count())
                    {
                        registros.Add(obj);
                    }
                }

                if (residuo == 2)
                {
                    obj.imagen2 = lst[i].img;
                    obj.cantidad2 = (lst[i].Cantidad == null ? "" : lst[i].Cantidad.ToString()) + " " + (lst[i].Unidad == null ? "" : lst[i].Unidad.ToString());
                    obj.medida2 = lst[i].Medida == null ? "" : lst[i].Medida.ToString();
                    obj.codigo2 = lst[i].CodigoP == null ? "" : lst[i].CodigoP.ToString();
                    obj.reflejante2 = lst[i].Reflejante == null ? "" : lst[i].Reflejante.ToString();
                    if ((i + 1) == lst.Count())
                    {
                        registros.Add(obj);
                    }
                }
                if (residuo == 3)
                {
                    obj.imagen3 = lst[i].img;
                    obj.cantidad3 = (lst[i].Cantidad == null ? "" : lst[i].Cantidad.ToString()) + " " + (lst[i].Unidad == null ? "" : lst[i].Unidad.ToString());
                    obj.medida3 = lst[i].Medida == null ? "" : lst[i].Medida.ToString();
                    obj.codigo3 = lst[i].CodigoP == null ? "" : lst[i].CodigoP.ToString();
                    obj.reflejante3 = lst[i].Reflejante == null ? "" : lst[i].Reflejante.ToString();
                    if ((i + 1) == lst.Count())
                    {
                        registros.Add(obj);
                    }
                }
                if (residuo == 0)
                {
                    obj.imagen4 = lst[i].img;
                    obj.cantidad4 = (lst[i].Cantidad == null ? "" : lst[i].Cantidad.ToString()) + " " + (lst[i].Unidad == null ? "" : lst[i].Unidad.ToString());
                    obj.medida4 = lst[i].Medida == null ? "" : lst[i].Medida.ToString();
                    obj.codigo4 = lst[i].CodigoP == null ? "" : lst[i].CodigoP.ToString();
                    obj.reflejante4 = lst[i].Reflejante == null ? "" : lst[i].Reflejante.ToString();
                    registros.Add(obj);
                }

            }

            return registros;
        }
        public static string ConvertToBase64(MemoryStream stream)
        {
            byte[] bytes;
            bytes = stream.ToArray();
            return Convert.ToBase64String(bytes);
        }

        public DataTable obtenerEncabezado(int tipoEnc = 1)
        {
            DataTable tablaEncabezado = new DataTable();
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;

            tablaEncabezado.Columns.Add("Logo", System.Type.GetType("System.Byte[]"));
            tablaEncabezado.Columns.Add("LogoD", System.Type.GetType("System.Byte[]"));
            tablaEncabezado.Columns.Add("Titulo", System.Type.GetType("System.String"));
            tablaEncabezado.Columns.Add("Descripcion", System.Type.GetType("System.String"));
            string ubicacion = "";
            string ubicacionD = "";

            if (tipoEnc == 1)
            {
                ubicacion = baseDirectory + @"\Content\img\Logo.png";
            }
            else
            {
                ubicacion = baseDirectory + @"\Content\img\Logo.png";
            }
            ubicacionD = baseDirectory + @"\Content\img\Logo.png";

            byte[] logo = System.IO.File.ReadAllBytes(ubicacion);
            byte[] logoD = System.IO.File.ReadAllBytes(ubicacionD);
            string titulo = "SECRETARÍA DE EDUCACIÓN Y CULTURA";
            string descripcion = "REPORTE DE SOLICITUD";
            tablaEncabezado.Rows.Add(logo, logoD, titulo, descripcion);
            return tablaEncabezado;
        }

        private static readonly string[] UNIDADES = {
        "CERO", "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE"
    };

        private static readonly string[] DECENAS = {
        "", "DIEZ", "VEINTE", "TREINTA", "CUARENTA", "CINCUENTA", "SESENTA", "SETENTA",
        "OCHENTA", "NOVENTA"
    };

        private static readonly string[] DECENAS_ESPECIALES = {
        "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISÉIS", "DIECICIETE",
        "DIECIOCHO", "DIECINUEVE"
    };

        private static readonly string[] CENTENAS = {
        "", "CIENTO", "DOCIENTOS", "TRECIENTOS", "CUATROCIENTOS", "QUINIENTOS",
        "SEICIENTOS", "SETECIENTOS", "OCHOCIENTOS", "NOVECIENTOS"
    };

        private static readonly string[] MIL = {
        "", "MIL ", " MILLONES ", " MIL MILLONES "
    };

        public static string ConvertirNumeroALetras(double numero)
        {
            // Dividimos el número en parte entera y parte decimal
            long parteEntera = (long)numero;
            int parteDecimal = (int)Math.Round((numero - parteEntera) * 100);

            // Convertimos la parte entera a letras
            string parteEnteraEnLetras = ConvertirParteEnteraALetras(parteEntera);

            // Convertimos la parte decimal a letras
            string parteDecimalEnLetras = ConvertirParteDecimalALetras(parteDecimal);

            // Construimos el resultado final
            string resultado = parteEnteraEnLetras + " PESOS";
            //if (!string.IsNullOrEmpty(parteDecimalEnLetras))
            //{
            //    resultado += " CON " + parteDecimalEnLetras + " CENTAVOS";
            //}

            return resultado;
        }

        private static string ConvertirParteEnteraALetras(long numero)
        {
            if (numero == 0)
            {
                return "CERO";
            }

            string letras = "";
            int contador = 0;

            do
            {
                int grupo = (int)(numero % 1000);
                if (grupo > 0)
                {
                    string letrasGrupo = ConvertirGrupoALetras(grupo);
                    if (contador > 0)
                    {
                        letrasGrupo += MIL[contador];
                    }
                    letras = letrasGrupo + letras;
                }
                numero /= 1000;
                contador++;
            } while (numero > 0);

            return letras.Trim();
        }

        private static string ConvertirGrupoALetras(int numero)
        {
            string letras = "";

            // Centenas
            if (numero == 100)
            {
                letras += "CIEN";
                numero %= 100;
            }
            else
            {
                if (numero >= 100)
                {
                    letras += CENTENAS[numero / 100] + " ";
                    numero %= 100;
                }
            }

            // Decenas y unidades
            if (numero >= 20)
            {
                letras += DECENAS[numero / 10] + " Y ";
                numero %= 10;
            }
            else if (numero >= 10)
            {
                letras += DECENAS_ESPECIALES[numero - 10] + " ";
                numero = 0;
            }

            // Unidades
            if (numero > 0)
            {
                letras += UNIDADES[numero] + " ";
            }

            return letras;
        }

        private static string ConvertirParteDecimalALetras(int numero)
        {
            if (numero == 0)
            {
                return "";
            }

            string letras = "";

            // Decenas y unidades
            if (numero >= 20)
            {
                letras += DECENAS[numero / 10] + " ";
                numero %= 10;
            }
            else if (numero >= 10)
            {
                letras += DECENAS_ESPECIALES[numero - 10] + " ";
                numero = 0;
            }

            // Unidades
            if (numero > 0)
            {
                letras += UNIDADES[numero] + " ";
            }

            return letras.Trim();
        }

        private static readonly string[] NOMBRES_MESES = {
        "", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO",
        "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"
    };

        public static string ConvertirFechaALetras(DateTime fecha)
        {
            int dia = fecha.Day;
            int mes = fecha.Month;
            int anio = fecha.Year;

            string fechaEnLetras = $"{dia} DE {NOMBRES_MESES[mes]} DE {anio}";

            return fechaEnLetras;
        }

    }
}