﻿using MEBA.Models.ModGenerales;
using MEBA.Models.ModReportes;
using MEBA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MEBA.Models.ModProductos;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using MEBA.Clases;

namespace MEBA.Controllers
{
    public class ProductosController : Controller
    {
        ClsModResponse objResultado = new ClsModResponse();
        MEBAEntities db = new MEBAEntities();
        // GET: Productos
        public ActionResult Index()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                addBitacora("Operativo", "Acceso a los productos");

                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }

        public string addBitacora(string tipo, string descripcion)
        {
            string guardado = "";
            try
            {
                MEBABitacoraAcc objBitacora = new MEBABitacoraAcc();
                objBitacora.Fecha = DateTime.Now;
                objBitacora.Tipo = tipo;
                objBitacora.Origen = "";
                objBitacora.Usuario = vSessiones.sesionUsuarioDTO.UserName;
                objBitacora.Grupo = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                objBitacora.Zona = "";
                objBitacora.Estacion = "(N/A)";
                objBitacora.Descripcion = descripcion;
                objBitacora.IdAsociado = vSessiones.sessionUsuarioCONFIGDTO.IDAsociado;
                db.MEBABitacoraAcc.Add(objBitacora);
                db.SaveChanges();
                guardado = "exitoso";
            }
            catch (Exception ex)
            {
                guardado = "";
            }
            return guardado;
        }



        [HttpPost]
        [AllowAnonymous]
        [ActionName("postSpdObtenerListadoCatProductos")]
        public JsonResult postSpdObtenerListadoCatProductos(parametrosProductos parametros)
        {
            objResultado = new ClsModResponse();
            try
            {
                parametros.descripcion = parametros.descripcion == null ? "" : parametros.descripcion;
                using (db = new MEBAEntities())
                {
                    var listado = db.spdObtenerListadoProductos1(parametros.Pag, parametros.numPag, parametros.descripcion).ToList().Select(y => new parametrosProductos
                    {
                        id = y.id,
                        codigo = y.codigo,
                        codigoP = y.codigoP,
                        descripcion = y.descripcion,
                        img = obtenerImagen(y.img, y.tipoFormato),
                        precioVenta = y.precioVenta,
                        tipoFormato = y.tipoFormato,
                        unidad = y.unidad
                    }).ToList();
                    objResultado.ITEMS = listado;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }
        public string obtenerImagen(string _Ruta, string formato)
        {
            string img = "";
            try
            {
                byte[] imagen = System.IO.File.ReadAllBytes(_Ruta);
                if (formato == "image/jpg")
                {
                    img = "data:image/jpg;base64," + Convert.ToBase64String(imagen);
                }
                else
                {
                    img = "data:image/png;base64," + Convert.ToBase64String(imagen);
                }

            }
            catch (Exception ex)
            {
                img = "";
            }
            return img;
        }
        [HttpPost]
        public JsonResult postSpdAgregarModificarEliminar(HttpPostedFile Archivo, parametrosProductos parametros)
        {
            objResultado = new ClsModResponse();
            try
            {
                using (db = new MEBAEntities())
                {
                    CatProductos objProductos = new CatProductos();
                    CatProductos objRegistro = new CatProductos();
                    string rutaArchivo = "";
                    switch (parametros.Opcion)
                    {
                        case "A":

                            rutaArchivo = CrearArchivoYDirectorio(Guid.NewGuid().ToString(), parametros.img64, parametros.tipoFormato);

                            objProductos = new CatProductos();
                            objProductos.codigo = parametros.codigo == null ? "" : parametros.codigo;
                            objProductos.codigoP = parametros.codigoP == null ? "" : parametros.codigoP;
                            objProductos.descripcion = parametros.descripcion == null ? "" : parametros.descripcion;
                            objProductos.precioVenta = parametros.precioVenta == null ? 0 : parametros.precioVenta;
                            if (rutaArchivo != "")
                            {
                                objProductos.img = rutaArchivo;
                            }
                            objProductos.tipoFormato = parametros.tipoFormato == null ? "" : parametros.tipoFormato;
                            objProductos.unidad = parametros.unidad == null ? "" : parametros.unidad;
                            db.CatProductos.Add(objProductos);
                            db.SaveChanges();


                            break;
                        case "B":
                            objRegistro = db.CatProductos.Where(r => r.id == parametros.id).FirstOrDefault();
                            if (objRegistro != null)
                            {
                                rutaArchivo = CrearArchivoYDirectorio(Guid.NewGuid().ToString(), parametros.img64, parametros.descripcion);

                                objRegistro.descripcion = parametros.descripcion == null ? "" : parametros.descripcion;
                                objRegistro.precioVenta = parametros.precioVenta;
                                objRegistro.codigo = parametros.codigo == null ? "" : parametros.codigo;
                                objRegistro.codigoP = parametros.codigoP == null ? "" : parametros.codigoP;
                                if (rutaArchivo != "")
                                {
                                    objRegistro.img = rutaArchivo;
                                }
                                objRegistro.tipoFormato = parametros.tipoFormato == null ? "" : parametros.tipoFormato;
                                objRegistro.unidad = parametros.unidad == null ? "" : parametros.unidad;
                                db.SaveChanges();
                            }
                            else
                            {
                                objResultado.ITEMS = null;
                                objResultado.SUCCESS = false;
                                objResultado.MENSAJE = "no se encuentra en la base de datos.";
                            }
                            break;
                        case "C":
                            objRegistro = db.CatProductos.Where(r => r.id == parametros.id).FirstOrDefault();
                            if (objRegistro != null)
                            {
                                db.CatProductos.Remove(objRegistro);
                                db.SaveChanges();
                            }
                            else
                            {
                                objResultado.ITEMS = null;
                                objResultado.SUCCESS = false;
                                objResultado.MENSAJE = "no se encuentra en la base de datos.";
                            }
                            break;
                    }


                    objResultado.ITEMS = objProductos;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }
        public string CrearArchivoYDirectorio(string filename, string base64String, string tipoFormato)
        {
            string ruta = "";

            try
            {
                byte[] Arreglo = Convert.FromBase64String(base64String);
                string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                string carpeta = "Almacenamiento\\Productos\\";
                ruta = baseDirectory + carpeta;
                string rutaCarpeta = ruta;
                if (tipoFormato == "image/jpg")
                {
                    ruta += filename + ".jpg";
                }
                else if (tipoFormato == "image/jpeg")
                {
                    ruta += filename + ".jpeg";
                }
                else
                {
                    ruta += filename + ".png";
                }
                Stream stream = new MemoryStream(Arreglo);
                if (!Directory.Exists(rutaCarpeta))
                {
                    Directory.CreateDirectory(rutaCarpeta);
                }
                using (var fileStream = System.IO.File.Create(ruta))
                {
                    stream.Seek(0, SeekOrigin.Begin);
                    stream.CopyTo(fileStream);
                }
            }
            catch (Exception ex)
            {
                //var fileName = Directory.GetCurrentDirectory() + @"/errorlogRespponse.txt";
                //FileStream fs = System.IO.File.Create(fileName);
                //var sr = new StreamWriter(fs);
                //sr.WriteLine(ex.Message + "SI TRONE EN EL RESPONSE");
                ruta = "";
            }
            return ruta;
        }


        public string SaveImagesToFileSystem(string foto, string tipoFormato)
        {
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;

            string carpeta = "Almacenamiento\\Productos";
            Image image;
            string filename, savePath = baseDirectory + "\\" + carpeta + "\\";
            if (foto != null)
            {
                if (!Directory.Exists(savePath))
                    Directory.CreateDirectory(savePath);
                savePath = baseDirectory + "\\" + carpeta + "\\";
                if (!Directory.Exists(savePath))
                    Directory.CreateDirectory(savePath);
                using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(foto)))
                    image = Image.FromStream(ms);

                if (tipoFormato == "image/jpg")
                {
                    filename = savePath + Guid.NewGuid() + ".jpg";
                }
                else if (tipoFormato == "image/jpeg")
                {
                    filename = savePath + Guid.NewGuid() + ".jpeg";
                }
                else
                {
                    filename = savePath + Guid.NewGuid() + ".png";
                }
                try
                {
                    using (Bitmap tempImage = new Bitmap(image))
                    {
                        if (tipoFormato == "image/jpg")
                        {
                            tempImage.Save(filename, ImageFormat.Jpeg);
                        }
                        else if (tipoFormato == "image/jpeg")
                        {
                            tempImage.Save(filename, ImageFormat.Jpeg);
                        }
                        else
                        {
                            tempImage.Save(filename, ImageFormat.Png);
                        }
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            else
            {
                filename = "";
            }
            return filename;
        }

    }
}