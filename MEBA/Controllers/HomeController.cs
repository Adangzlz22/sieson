﻿using Dapper;
using MEBA.Clases;
using MEBA.Models;
using MEBA.Models.ModEstaciones;
using MEBA.Models.ModGenerales;
using MEBA.Models.ModNexus;
using MEBA.Models.ModProductos;
using MEBA.Models.ModUsuarios;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TCITools;

namespace MEBA.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult Estaciones()
        {

            ViewBag.Message = "Your application description page.";
            if (vSessiones.sesionUsuarioDTO != null)
            {
                if (vSessiones.sesionUsuarioDTO.idRol == 4)
                {
                    return Redirect("/Reportes/Index");
                }
                else
                {
                    ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                    ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                    using (MEBAEntities oDB2 = new MEBAEntities())
                    {
                        MEBAProEmpresas objEmpresas = oDB2.MEBAProEmpresas.Where(r => r.GuidEmpresa == vSessiones.sesionUsuarioDTO.GuidEmpresa).FirstOrDefault();
                        MEBABitacoraAcc objBitacora = new MEBABitacoraAcc();

                        objBitacora.Fecha = DateTime.Now;
                        objBitacora.Tipo = "Operativo";
                        objBitacora.Origen = vSessiones.sessionUsuarioCONFIGDTO.SitioWEB;
                        objBitacora.Usuario = vSessiones.sesionUsuarioDTO.UserName;
                        objBitacora.Grupo = objEmpresas.NombreComercial;
                        objBitacora.Zona = "";
                        objBitacora.Estacion = "";
                        objBitacora.Descripcion = "Acceso ala seccion de estaciones";
                        objBitacora.IdAsociado = vSessiones.sessionUsuarioCONFIGDTO.IDAsociado;
                        var FechaAnterior = oDB2.spdObtenerFechaAnterior("0", vSessiones.sessionUsuarioEmpresa.NombreComercial).FirstOrDefault();
                        if (FechaAnterior != null)
                        {
                            objBitacora.FechaAnterior = FechaAnterior;
                        }
                        oDB2.MEBABitacoraAcc.Add(objBitacora);
                        oDB2.SaveChanges();
                    }
                    return View();
                }
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            if (vSessiones.sesionUsuarioDTO != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult bitacora()
        {
            ViewBag.Message = "Your application description page.";
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult Catalogos()
        {
            ViewBag.Message = "Your application description page.";
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult perfil()
        {
            ViewBag.Message = "Your application description page.";
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }

        public ActionResult Reportes()
        {
            ViewBag.Message = "Your contact page.";
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult menu()
        {
            ViewBag.Message = "Your contact page.";
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult menuReportes()
        {
            ViewBag.Message = "Your contact page.";
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult menuConfiguracion()
        {
            ViewBag.Message = "Your contact page.";
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }


        public ActionResult ObtenerConfig()
        {
            ClsModResponse objResultado = new ClsModResponse();
            if (vSessiones.sesionUsuarioDTO == null)
            {
                return Redirect("/Login/Login");
            }
            else
            {
                try
                {
                    MEBAEntities db = new MEBAEntities();
                    ClsModAsociadosConfig objAsociadosConfig = new ClsModAsociadosConfig();
                    objAsociadosConfig.IDAsociadoConfig = vSessiones.sessionUsuarioCONFIGDTO.IDAsociadoConfig;
                    objAsociadosConfig.IDAsociado = vSessiones.sessionUsuarioCONFIGDTO.IDAsociado;
                    objAsociadosConfig.SitioWEB = vSessiones.sessionUsuarioCONFIGDTO.SitioWEB;
                    objAsociadosConfig.ServerSeguridad = vSessiones.sessionUsuarioCONFIGDTO.ServerSeguridad;
                    objAsociadosConfig.UsuarioSQLSeguridad = vSessiones.sessionUsuarioCONFIGDTO.UsuarioSQLSeguridad;
                    objAsociadosConfig.PasswordSQLSeguridad = vSessiones.sessionUsuarioCONFIGDTO.PasswordSQLSeguridad;
                    objAsociadosConfig.BaseProduccion = vSessiones.sessionUsuarioCONFIGDTO.BaseProduccion;
                    objAsociadosConfig.urlApi = vSessiones.sessionUsuarioCONFIGDTO.urlApi;
                    objAsociadosConfig.usuarioApi = vSessiones.sessionUsuarioCONFIGDTO.usuarioApi;
                    objAsociadosConfig.passwordApi = vSessiones.sessionUsuarioCONFIGDTO.passwordApi;
                    objAsociadosConfig.IdRol = vSessiones.sesionUsuarioDTO.idRol;
                    int idEmpresa = vSessiones.sessionUsuarioEmpresa.IDEmpresa;
                    if (db.MEBARelUsuarioZonas.Where(r => r.IdUsuario == vSessiones.sesionUsuarioDTO.IdUsuario).FirstOrDefault() != null)
                    {
                        objAsociadosConfig.primerZona = db.MEBARelUsuarioZonas.Where(r => r.IdUsuario == vSessiones.sesionUsuarioDTO.IdUsuario).FirstOrDefault().IdZona;
                    }

                    objResultado.ITEMS = objAsociadosConfig;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }
                catch (Exception ex)
                {
                    objResultado.ITEMS = null;
                    objResultado.SUCCESS = false;
                    objResultado.MENSAJE = ex.Message.ToString();
                }
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);

        }

        public JsonResult ObtenerConfigGrupos(int IDAsociado)
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {
                MEBAEntities db = new MEBAEntities();
                MEBAAsociadosConfig objAsociadosConfig = new MEBAAsociadosConfig();

                objAsociadosConfig = db.MEBAAsociadosConfig.Where(r => r.IDAsociado == IDAsociado).FirstOrDefault();

                objResultado.ITEMS = objAsociadosConfig;
                objResultado.SUCCESS = true;
                objResultado.MENSAJE = "";
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);

        }


        public JsonResult StpGetLastCoreProcessDate(int identificador)
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {
                DynamicParameters _params2 = new DynamicParameters();

                string Consulta = @"Sync.StpGetLastCoreProcessDate";
                string Consulta1 = @"SELECT order_number,station_id,number,short_name,current_shift,commercial_name FROM inv_station";
                string Consulta2 = @"SELECT product_id,short_description,color,ProductSubKey FROM inv_product WHERE type=0";
                string Consulta3 = @"SELECT station_id,tank_number,product_id,inventory,LastUpdate FROM opr_stationTankInventory";
                string pass = Funciones.EncriptaClave("*tci123");
                string pass1 = Funciones.EncriptaClaveSQL("*tci123");
                string passwordsql = Funciones.Decrypt(vSessiones.sessionUsuarioCONFIGDTO.PasswordSQLSeguridad);
                using (var ctx = new SqlConnection(ClsConexion.db(vSessiones.sessionUsuarioCONFIGDTO.ServerSeguridad,
                                                                                                    vSessiones.sessionUsuarioCONFIGDTO.BaseProduccion,
                                                                                                    vSessiones.sessionUsuarioCONFIGDTO.UsuarioSQLSeguridad,
                                                                                                    passwordsql)))
                {
                    //var lstDatos = ctx.Query<dynamic>(Consulta).ToList();
                    List<clsModEstacion> lstEstaci = new List<clsModEstacion>();
                    if (identificador == 50)
                    {
                        lstEstaci = ctx.Query<clsModEstacion>(Consulta, null, null, true, 300, CommandType.StoredProcedure).ToList();
                    }
                    else
                    {
                        lstEstaci = ctx.Query<clsModEstacion>(Consulta, null, null, true, 300, CommandType.StoredProcedure).ToList().Where(r => r.Identifier == identificador).ToList();
                    }
                    var lstEstaciones = ctx.Query<clsModEstaciones>(Consulta1, null, null, true, 300, CommandType.Text).ToList();
                    var lstProductos = ctx.Query<clsModProductos>(Consulta2, null, null, true, 300, CommandType.Text).ToList();
                    var lstInventario = ctx.Query<clsModInventario>(Consulta3, null, null, true, 300, CommandType.Text).ToList();

                    var lstDatos = lstEstaci.ToList().Select(y => new
                    {
                        StationId = y.StationId,
                        StationNumber = y.StationNumber,
                        CommercialName = y.CommercialName,
                        Identifier = y.Identifier,
                        objEstaciones = lstEstaciones.Where(r => r.station_id == y.StationId).FirstOrDefault(),
                        lstInventario = lstInventario.Where(r => r.station_id == y.StationId).ToList(),
                        lstProductos = lstProductos.ToList(),
                        objTurno = traertUltimoTurno(y.StationId),
                        objIdentificador = traerIdentificador(y.Identifier)
                    }).ToList();

                    objResultado.ITEMS = lstDatos;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);

        }
        public JsonResult StpGetLastCoreProcessDate2()
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {
                DynamicParameters _params2 = new DynamicParameters();

                string Consulta = @"Sync.StpGetLastCoreProcessDate";
                string passwordsql = Funciones.Decrypt(vSessiones.sessionUsuarioCONFIGDTO.PasswordSQLSeguridad);
                using (var ctx = new SqlConnection(ClsConexion.db(vSessiones.sessionUsuarioCONFIGDTO.ServerSeguridad,
                                                                                                    vSessiones.sessionUsuarioCONFIGDTO.BaseProduccion,
                                                                                                    vSessiones.sessionUsuarioCONFIGDTO.UsuarioSQLSeguridad,
                                                                                                    passwordsql)))
                {
                    //var lstDatos = ctx.Query<dynamic>(Consulta).ToList();
                    List<clsModEstacion> lstEstaci = new List<clsModEstacion>();
                    lstEstaci = ctx.Query<clsModEstacion>(Consulta, null, null, true, 300, CommandType.StoredProcedure).ToList();

                    var lstDatos = lstEstaci.ToList();

                    objResultado.ITEMS = lstDatos;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);

        }
        public clsModTurno traertUltimoTurno(int station_id)
        {
            clsModTurno objTurno = new clsModTurno();
            clsModTurno objTurnoretorno = new clsModTurno();
            try
            {
                DynamicParameters _params2 = new DynamicParameters();
                #region Consulta4
                string Consulta4 = @"DECLARE @Default INT = 1

                                SELECT
	                                @Default = ISNULL(MAX(shift_id), 1)
                                FROM adm_shift

                                ;WITH TEMP AS (
	                                SELECT TOP 1 
		                                Date, 
		                                shift_id, 
		                                station_id
	                                FROM opr_shiftChange 
	                                WHERE 
		                                station_id = " + station_id + @" 
	                                ORDER BY 
		                                Date DESC, 
		                                shift_id DESC
                                )
                                SELECT
	                                Date AS UltimaFecha, 
	                                shift_id AS UltimoTurno,
	                                ISNULL(shiftC.shift_count, @Default) AS CantidadTurnos
                                FROM TEMP t
                                LEFT JOIN opr_stationShiftCount shiftC
	                                ON t.station_id = shiftC.station_id";
                #endregion

                string consulta5 = @"select datediff(MINUTE, LastCoreProcessCheck, getdate()) AS MinutosActualizado FROM inv_stationServer WHERE station_id=" + station_id;


                string passwordsql = Funciones.Decrypt(vSessiones.sessionUsuarioCONFIGDTO.PasswordSQLSeguridad);
                using (var ctx = new SqlConnection(ClsConexion.db(vSessiones.sessionUsuarioCONFIGDTO.ServerSeguridad,
                                                                                                    vSessiones.sessionUsuarioCONFIGDTO.BaseProduccion,
                                                                                                    vSessiones.sessionUsuarioCONFIGDTO.UsuarioSQLSeguridad,
                                                                                                    passwordsql)))
                {
                    objTurno = ctx.Query<clsModTurno>(Consulta4, null, null, true, 300, CommandType.Text).FirstOrDefault();
                    var minutos = ctx.Query<int>(consulta5, null, null, true, 300, CommandType.Text).FirstOrDefault();
                    if (objTurno != null)
                    {
                        objTurnoretorno = objTurno;
                        objTurnoretorno.minutos = minutos;
                    }
                    else
                    {
                        objTurnoretorno = new clsModTurno();
                        objTurnoretorno.minutos = minutos;
                    }

                }
            }
            catch (Exception ex)
            {
            }
            return objTurnoretorno;
        }

        public MEBARelStateImage traerIdentificador(int identificador)
        {

            MEBARelStateImage objIdentificador = new MEBARelStateImage();
            try
            {
                using (var db = new MEBAEntities())
                {
                    objIdentificador = db.MEBARelStateImage.Where(r => r.IdentificadorSP == identificador).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
            }
            return objIdentificador;
        }

        public JsonResult TodosIdentificadores()
        {
            ClsModResponse objResultado = new ClsModResponse();

            List<MEBARelStateImage> objIdentificador = new List<MEBARelStateImage>();
            try
            {
                using (var db = new MEBAEntities())
                {
                    objIdentificador = db.MEBARelStateImage.ToList();
                    objResultado.ITEMS = objIdentificador;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }


        public JsonResult CambiarStatus(int identificador, int station_id, string estacion, string StationNumber)
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {
                MEBARelStateImage objRel = new MEBARelStateImage();
                using (var db = new MEBAEntities())
                {
                    objRel = db.MEBARelStateImage.Where(r => r.IdentificadorSP == identificador).FirstOrDefault();
                }
                string Consulta = "Sync.StpSaveCoreProcessTransaction";
                DynamicParameters _params2 = new DynamicParameters();
                _params2.Add("@TransactionCode", objRel.IdentificadorDestino);
                _params2.Add("@StationId", station_id);
                string passwordsql = Funciones.Decrypt(vSessiones.sessionUsuarioCONFIGDTO.PasswordSQLSeguridad);
                using (var ctx = new SqlConnection(ClsConexion.db(vSessiones.sessionUsuarioCONFIGDTO.ServerSeguridad,
                                                                                                    vSessiones.sessionUsuarioCONFIGDTO.BaseProduccion,
                                                                                                    vSessiones.sessionUsuarioCONFIGDTO.UsuarioSQLSeguridad,
                                                                                                    passwordsql)))
                {
                    var lstCambiarStatus = ctx.Query<dynamic>(Consulta, _params2, null, true, 300, CommandType.StoredProcedure).ToList();
                    objResultado.ITEMS = lstCambiarStatus;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }
                var objParams = new
                {
                    Identificador = identificador,
                    station_id = station_id
                };
                using (MEBAEntities oDB2 = new MEBAEntities())
                {
                    MEBAProEmpresas objEmpresas = oDB2.MEBAProEmpresas.Where(r => r.GuidEmpresa == vSessiones.sesionUsuarioDTO.GuidEmpresa).FirstOrDefault();
                    MEBABitacoraAcc objBitacora = new MEBABitacoraAcc();

                    objBitacora.Fecha = DateTime.Now;
                    objBitacora.Tipo = "Operativo";
                    objBitacora.Origen = vSessiones.sessionUsuarioCONFIGDTO.SitioWEB;
                    objBitacora.Usuario = vSessiones.sesionUsuarioDTO.UserName;
                    objBitacora.Grupo = objEmpresas.NombreComercial;
                    objBitacora.Zona = "";
                    objBitacora.identifier = objRel.IdentificadorDestino;
                    objBitacora.stationid = station_id.ToString();
                    objBitacora.numberstation = StationNumber;
                    objBitacora.Estacion = StationNumber + " - " + estacion;
                    objBitacora.Descripcion = "Cambio de status a " + objRel.IdentificadorDestino;
                    objBitacora.IdAsociado = vSessiones.sessionUsuarioCONFIGDTO.IDAsociado;
                    var FechaAnterior = oDB2.spdObtenerFechaAnterior("0", vSessiones.sessionUsuarioEmpresa.NombreComercial).FirstOrDefault();
                    if (FechaAnterior != null)
                    {
                        objBitacora.FechaAnterior = FechaAnterior;
                    }
                    oDB2.MEBABitacoraAcc.Add(objBitacora);
                    oDB2.SaveChanges();
                }
                using (MEBAEntities dbCon = new MEBAEntities())
                {
                    if (vSessiones.sessionUsuarioEmpresa.Telefono != null)
                    {
                        //if (objRel.IdentificadorDestino != "0" || objRel.IdentificadorDestino != "40")
                        //{
                        //    OPEWatchEnvios objEnvio1 = new OPEWatchEnvios();
                        //    objEnvio1.idTipoEnvio = 2;
                        //    objEnvio1.Fecha = DateTime.Now;
                        //    objEnvio1.Tipo = "Operativo";
                        //    objEnvio1.Plataforma = vSessiones.sessionUsuarioCONFIGDTO.SitioWEB;
                        //    objEnvio1.Asunto = "Operativo";
                        //    objEnvio1.De = "";
                        //    objEnvio1.Para = vSessiones.sessionUsuarioEmpresa.Telefono;
                        //    objEnvio1.CC = "";
                        //    objEnvio1.CCO = "";
                        //    objEnvio1.Prioridad = "ALTA";
                        //    objEnvio1.Mensaje = "Se detecto cambio de estado a " + objRel.IdentificadorDestino + " En la estacion " + station_id + " - " + estacion;
                        //    objEnvio1.Html = false;
                        //    objEnvio1.AccionSQLAntes = "";
                        //    objEnvio1.AccionSQLDespues = "";
                        //    objEnvio1.Enviado = false;
                        //    objEnvio1.UsuarioQueGenera = vSessiones.sesionUsuarioDTO.UserName;
                        //    objEnvio1.MensajeError = "";

                        //    dbCon.OPEWatchEnvios.Add(objEnvio1);
                        //    dbCon.SaveChanges();
                        //}
                    }


                    var lstContactos = dbCon.MEBAAsociadosContactos.Where(r => r.IDAsociado == vSessiones.sessionUsuarioCONFIGDTO.IDAsociado).ToList();
                    if (lstContactos.Count() != 0)
                    {
                        //if (objRel.IdentificadorDestino != "0" || objRel.IdentificadorDestino != "40")
                        //{
                        //    foreach (var item in lstContactos)
                        //    {
                        //        OPEWatchEnvios objEnvio = new OPEWatchEnvios();
                        //        objEnvio.idTipoEnvio = 2;
                        //        objEnvio.Fecha = DateTime.Now;
                        //        objEnvio.Tipo = "Operativo";
                        //        objEnvio.Plataforma = vSessiones.sessionUsuarioCONFIGDTO.SitioWEB;
                        //        objEnvio.Asunto = "Operativo";
                        //        objEnvio.De = "";
                        //        objEnvio.Para = item.Telefono;
                        //        objEnvio.CC = "";
                        //        objEnvio.CCO = "";
                        //        objEnvio.Prioridad = "ALTA";
                        //        objEnvio.Mensaje = "Se detecto cambio de estado a " + objRel.IdentificadorDestino + " En la estacion " + station_id + " - " + estacion;
                        //        objEnvio.Html = false;
                        //        objEnvio.AccionSQLAntes = "";
                        //        objEnvio.AccionSQLDespues = "";
                        //        objEnvio.Enviado = false;
                        //        objEnvio.UsuarioQueGenera = vSessiones.sesionUsuarioDTO.UserName;
                        //        objEnvio.MensajeError = "";

                        //        dbCon.OPEWatchEnvios.Add(objEnvio);
                        //        dbCon.SaveChanges();
                        //    }
                        //}
                    }
                }


            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoginApiSistema(ParametrosLogearseApi parametros)
        {
            string response = "";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(parametros.Url + parametros.EndPoint);
                string data = JsonConvert.SerializeObject(new { usuario = parametros.Usuario, constrasena = parametros.Password });

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
                requestWriter.Write(data);
                requestWriter.Close();
                try
                {
                    WebResponse webResponse = request.GetResponse();
                    Stream webStream = webResponse.GetResponseStream();
                    StreamReader responseReader = new StreamReader(webStream);
                    response = responseReader.ReadToEnd();
                    responseReader.Close();

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
            catch (Exception)
            {

                throw;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EndPointNexus(ParametrosEndPointNexus parametros)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(parametros.Url + parametros.EndPoint);
            string data = parametros.objParams;

            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = data.Length;
            StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
            requestWriter.Write(data);
            requestWriter.Close();
            string response = "";
            try
            {
                WebResponse webResponse = request.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                response = responseReader.ReadToEnd();
                responseReader.Close();
               
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult obtenerConfigEstaciones()
        {

            try
            {
                using (var ctx = new MEBAEntities())
                {
                    var objse = vSessiones.sessionUsuarioEmpresa;
                    if (objse != null)
                    {
                        int sessionAso = vSessiones.sessionUsuarioEmpresa.IDAsociado;
                        var config = ctx.MEBAAsociados.Where(r => r.IDAsociado == sessionAso).FirstOrDefault();
                        return Json(config, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(null, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            return Json(null, JsonRequestBehavior.AllowGet);
        }


        public JsonResult CambiarStatusApi(ParametrosEndpointCambioStatus parametros)
        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(parametros.Url + parametros.EndPoint);
            string data = JsonConvert.SerializeObject(parametros.objParams);

            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = data.Length;
            StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
            requestWriter.Write(data);
            requestWriter.Close();
            string response = "";
            try
            {

                WebResponse webResponse = request.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                response = responseReader.ReadToEnd();
                responseReader.Close();
                using (MEBAEntities oDB2 = new MEBAEntities())
                {
                    MEBAProEmpresas objEmpresas = oDB2.MEBAProEmpresas.Where(r => r.GuidEmpresa == vSessiones.sesionUsuarioDTO.GuidEmpresa).FirstOrDefault();
                    MEBABitacoraAcc objBitacora = new MEBABitacoraAcc();

                    var FechaAnterior = oDB2.spdObtenerFechaAnterior(parametros.StationNumber, vSessiones.sessionUsuarioEmpresa.NombreComercial).FirstOrDefault();

                    objBitacora.Fecha = DateTime.Now;
                    objBitacora.Tipo = "Operativo";
                    objBitacora.Origen = vSessiones.sessionUsuarioCONFIGDTO.SitioWEB;
                    objBitacora.Usuario = vSessiones.sesionUsuarioDTO.UserName;
                    objBitacora.Grupo = objEmpresas.NombreComercial;
                    objBitacora.Zona = "";
                    objBitacora.identifier = parametros.objParams.transactionCode;
                    objBitacora.stationid = parametros.objParams.stationId;
                    objBitacora.numberstation = parametros.StationNumber;
                    objBitacora.Estacion = parametros.StationNumber + " - " + parametros.estacion;
                    objBitacora.Descripcion = "Cambio de status a " + parametros.objParams.transactionCode;
                    objBitacora.IdAsociado = vSessiones.sessionUsuarioCONFIGDTO.IDAsociado;
                    if (FechaAnterior != null)
                    {
                        objBitacora.FechaAnterior = FechaAnterior;
                    }

                    oDB2.MEBABitacoraAcc.Add(objBitacora);
                    oDB2.SaveChanges();
                    var idRol = vSessiones.sesionUsuarioDTO.idRol;
                    var Usuario = vSessiones.sesionUsuarioDTO.UserName;
                    var GuidEmpresa = vSessiones.sessionUsuarioEmpresa.GuidEmpresa;


                    if (idRol == 3)
                    {
                        var lstUsuarios = oDB2.MEBAProEmpresasUsuarios.Where(r => r.idRol != 3 && r.idRol != 4 && r.idRol != 5 && r.GuidEmpresa == GuidEmpresa).ToList();
                        foreach (var item in lstUsuarios)
                        {
                            var objAlertas = new GenAlertas();

                            objAlertas.Activo = true;
                            objAlertas.IdUsuario = item.IdUsuario;
                            objAlertas.urlAccion = "/Admin/Admin/Bitacora";
                            objAlertas.Mensaje = "El Usuario : " + Usuario + " Cambio de status a " + parametros.objParams.transactionCode;
                            oDB2.GenAlertas.Add(objAlertas);
                            oDB2.SaveChanges();
                        }
                    }
                    else if (idRol == 2)
                    {
                        var lstUsuarios = oDB2.MEBAProEmpresasUsuarios.Where(r => r.idRol != 2 && r.idRol != 3 && r.idRol != 4 && r.idRol != 5 && r.GuidEmpresa == GuidEmpresa).ToList();
                        foreach (var item in lstUsuarios)
                        {
                            var objAlertas = new GenAlertas();

                            objAlertas.Activo = true;
                            objAlertas.IdUsuario = item.IdUsuario;
                            objAlertas.urlAccion = "/Admin/Admin/Bitacora";
                            objAlertas.Mensaje = "El Usuario : " + Usuario + " Cambio de status a " + parametros.objParams.transactionCode;
                            oDB2.GenAlertas.Add(objAlertas);
                            oDB2.SaveChanges();
                        }
                    }

                }
                using (MEBAEntities dbCon = new MEBAEntities())
                {

                    if (vSessiones.sessionUsuarioEmpresa.Telefono != null)
                    {
                        //if (objRel.IdentificadorDestino != "0" || objRel.IdentificadorDestino != "40")
                        //{
                        //    OPEWatchEnvios objEnvio1 = new OPEWatchEnvios();
                        //    objEnvio1.idTipoEnvio = 2;
                        //    objEnvio1.Fecha = DateTime.Now;
                        //    objEnvio1.Tipo = "Operativo";
                        //    objEnvio1.Plataforma = vSessiones.sessionUsuarioCONFIGDTO.SitioWEB;
                        //    objEnvio1.Asunto = "Operativo";
                        //    objEnvio1.De = "";
                        //    objEnvio1.Para = vSessiones.sessionUsuarioEmpresa.Telefono;
                        //    objEnvio1.CC = "";
                        //    objEnvio1.CCO = "";
                        //    objEnvio1.Prioridad = "ALTA";
                        //    objEnvio1.Mensaje = "Se detecto cambio de estado a " + objRel.IdentificadorDestino + " En la estacion " + parametros.stationId + " - " + parametros.estacion;
                        //    objEnvio1.Html = false;
                        //    objEnvio1.AccionSQLAntes = "";
                        //    objEnvio1.AccionSQLDespues = "";
                        //    objEnvio1.Enviado = false;
                        //    objEnvio1.UsuarioQueGenera = vSessiones.sesionUsuarioDTO.UserName;
                        //    objEnvio1.MensajeError = "";

                        //    dbCon.OPEWatchEnvios.Add(objEnvio1);
                        //    dbCon.SaveChanges();
                        //}

                    }
                    var lstContactos = dbCon.MEBAAsociadosContactos.Where(r => r.IDAsociado == vSessiones.sessionUsuarioCONFIGDTO.IDAsociado).ToList();
                    if (lstContactos.Count() != 0)
                    {
                        //if (objRel.IdentificadorDestino != "0" || objRel.IdentificadorDestino != "40")
                        //{
                        //    foreach (var item in lstContactos)
                        //    {
                        //        OPEWatchEnvios objEnvio = new OPEWatchEnvios();
                        //        objEnvio.idTipoEnvio = 2;
                        //        objEnvio.Fecha = DateTime.Now;
                        //        objEnvio.Tipo = "Operativo";
                        //        objEnvio.Plataforma = vSessiones.sessionUsuarioCONFIGDTO.SitioWEB;
                        //        objEnvio.Asunto = "Operativo";
                        //        objEnvio.De = "";
                        //        objEnvio.Para = item.Telefono;
                        //        objEnvio.CC = "";
                        //        objEnvio.CCO = "";
                        //        objEnvio.Prioridad = "ALTA";
                        //        objEnvio.Mensaje = "Se detecto cambio de estado a " + objRel.IdentificadorDestino + " En la estacion " + parametros.stationId + " - " + parametros.estacion;
                        //        objEnvio.Html = false;
                        //        objEnvio.AccionSQLAntes = "";
                        //        objEnvio.AccionSQLDespues = "";
                        //        objEnvio.Enviado = false;
                        //        objEnvio.UsuarioQueGenera = vSessiones.sesionUsuarioDTO.UserName;
                        //        objEnvio.MensajeError = "";

                        //        dbCon.OPEWatchEnvios.Add(objEnvio);
                        //        dbCon.SaveChanges();
                        //    }
                        //}
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ExtraerEstacionesXZonas(int IdZona)
        {
            List<MEBARelZonasEstaciones> lstAsignadas = new List<MEBARelZonasEstaciones>();
            try
            {
                using (var ctx = new MEBAEntities())
                {
                    //if (vSessiones.sesionUsuarioDTO.idRol == 1 || vSessiones.sesionUsuarioDTO.idRol == 2)
                    //{
                    //if (vSessiones.sesionUsuarioDTO.idRol == 1)
                    //{
                    //    lstAsignadas = new List<MEBARelZonasEstaciones>();
                    //    var ZonasAsignadas = ctx.MEBAZonas.Where(r => r.IdEmpresa == vSessiones.sessionUsuarioEmpresa.IDEmpresa).OrderBy(r => r.Orden).Select(y => y.IdZona).ToList();
                    //    var lstAsignadas2 = ctx.MEBARelZonasEstaciones.Where(r => ZonasAsignadas.Contains((int)r.IdZona)).ToList().Select(r => new
                    //    {
                    //        IdEstaciones = r.IdEstaciones
                    //    }).Distinct().ToList();
                    //    return Json(lstAsignadas2, JsonRequestBehavior.AllowGet);
                    //}
                    //else if (vSessiones.sesionUsuarioDTO.idRol == 2)
                    //{
                    lstAsignadas = new List<MEBARelZonasEstaciones>();
                    var ZonasAsignadas = ctx.MEBAZonas.Where(r => r.IdEmpresa == vSessiones.sessionUsuarioEmpresa.IDEmpresa && r.IdZona == IdZona).OrderBy(r => r.Orden).Select(y => y.IdZona).ToList();
                    var lstAsignadas2 = ctx.MEBARelZonasEstaciones.Where(r => ZonasAsignadas.Contains((int)r.IdZona)).ToList().Select(r => new
                    {
                        IdEstaciones = r.IdEstaciones
                    }).Distinct().ToList();
                    return Json(lstAsignadas2, JsonRequestBehavior.AllowGet);
                    //    }
                    //}
                    //else
                    //{
                    //    lstAsignadas = new List<MEBARelZonasEstaciones>();
                    //    var ZonasAsignadas = ctx.MEBARelUsuarioZonas.Where(r => r.IdZona == IdZona).Select(y => y.IdZona).ToList();
                    //    lstAsignadas = ctx.MEBARelZonasEstaciones.Where(r => ZonasAsignadas.Contains(r.IdZona)).ToList();
                    //}
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            return Json(lstAsignadas, JsonRequestBehavior.AllowGet);
        }

        public JsonResult cboZona()
        {
            List<MEBAZonas> lstAsignadas = new List<MEBAZonas>();
            try
            {
                using (var ctx = new MEBAEntities())
                {
                    lstAsignadas = new List<MEBAZonas>();
                    // mario me pidio cambiar la logica de esto solo el administrador puede ver todos lo demas funciona normal con restricciones
                    if (vSessiones.sesionUsuarioDTO.idRol == 1)
                    {
                        lstAsignadas = ctx.MEBAZonas.Where(r => r.IdEmpresa == vSessiones.sessionUsuarioEmpresa.IDEmpresa).OrderBy(r => r.Orden).ToList();

                    }
                    else
                    {
                        var ZonasAsignadas = ctx.MEBARelUsuarioZonas.Where(r => r.IdUsuario == vSessiones.sesionUsuarioDTO.IdUsuario).Select(y => y.IdZona).ToList();
                        lstAsignadas = ctx.MEBAZonas.Where(r => r.IdEmpresa == vSessiones.sessionUsuarioEmpresa.IDEmpresa && ZonasAsignadas.Contains(r.IdZona)).OrderBy(r => r.Orden).ToList();

                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            return Json(lstAsignadas, JsonRequestBehavior.AllowGet);
        }



    }

}