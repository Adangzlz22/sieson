﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MEBA.Reportes
{
    public enum ReportesEnum
    {
        [DescriptionAttribute("Cotizacion")]
        Cotizacion = 1,
        [DescriptionAttribute("Ordenes De Compra")]
        OrdenesDeCompra = 2,

        [DescriptionAttribute("Ordenes De Trabajo")]
        OrdenDeTrabajo = 3,

        [DescriptionAttribute("Envio de Cotizacion")]
        EnvioCorreoCotizacion = 4,
    }
}