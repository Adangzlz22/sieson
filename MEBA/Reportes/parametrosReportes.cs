﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Reportes
{
    public class parametrosReportes
    {
        public int IdReporte { get; set; }
        public int IdCotizacion { get; set; }
        public string destinatario { get; set; }
        public string destinatarioAdjunto { get; set; }

    }
}