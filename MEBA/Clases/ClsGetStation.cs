﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Clases
{
    public class ClsGetStation
    {
        public bool Error { get; set; }
        public List<ClsGetStationData> Data { get; set; }
        public string Comment { get; set; }
    }
}