﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Clases
{
    public class ModCotizacion
    {
        public int id { get; set; }
        public int IdCotizacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string FechaTexto { get; set; }
        public int ProductoId { get; set; }
        public string NombreProducto { get; set; }

        public decimal PrecioProducto { get; set; }
        public decimal PrecioVenta { get; set; }
        public decimal Cantidad { get; set; }
        public string Folio { get; set; }
        public string Tramo { get; set; }
        public string Atencion { get; set; }
        public string NombreCliente { get; set; }
        public string RFCCliente { get; set; }
        public string Direccion { get; set; }
        public string NumINT { get; set; }
        public string CP { get; set; }
        public string Ciudad { get; set; }
        public string Estado { get; set; }
        public string Pais { get; set; }
        public bool Activo { get; set; }
        public byte[] img { get; set; }
        public string Unidad { get; set; }
        public string codigo { get; set; }
    }
    
}