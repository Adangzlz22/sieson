﻿using MEBA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TCITools;

namespace MEBA.Clases
{
    public class PLDComm
    {
        private string _Usuario;
        private string _Password;
        private string _Servidor;
        public string Servidor
        {
            get
            {
                return _Servidor;
            }
        }
        public string Usuario
        {
            get
            {
                return _Usuario;
            }
        }
        public string Password
        {
            get
            {
                return _Password;
            }
        }

        public bool ObtenerCredencialesAsociado(string _GuidAsociado)
        {
            using (var oContext = new MEBAEntities())
            {
                var oAsociado = oContext.MEBAAsociados.FirstOrDefault(ss => ss.GUIDAsocuado == _GuidAsociado && ss.Estatus == true);
                if (oAsociado != null)
                {
                    var oAsociadoConfig = oContext.MEBAAsociadosConfig.FirstOrDefault(ss => ss.IDAsociado == oAsociado.IDAsociado);
                    if (oAsociadoConfig != null)
                    {
                        Herramientas herramientas = new Herramientas();
                        _Servidor = oAsociadoConfig.ServerSeguridad;
                        _Usuario = oAsociadoConfig.UsuarioSQLSeguridad;
                        _Password = Funciones.Decrypt(oAsociadoConfig.PasswordSQLSeguridad);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
    }
}