﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEBA.Clases
{
    public class ClsGetStationData
    {
        public int StationId { get; set; }
        public int StationNumber { get; set; }
        public string StationName { get; set; }
        public string StationShortName { get; set; }
        public int CurrentShift { get; set; }
        public string CurrentDate { get; set; }
        public int Identifier { get; set; }
        public double StampBalance { get; set; }
        public string LastCoreProcessCheck { get; set; }
    }
}