﻿using MEBA.Models;
using MEBA.Models.ModReportes;
using MEBA.Models.ModReportes.ReporteResumen;
using MEBA.Models.ModUsuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace MEBA.Clases
{
    public static class vSessiones
    {
        private static HttpSessionState session { get { return HttpContext.Current.Session; } }

        public static MEBAProEmpresasUsuarios sesionUsuarioDTO
        {
            get { return session["objUsuario"] as MEBAProEmpresasUsuarios; }
            set { session["objUsuario"] = value; }
        }
        public static MEBAAsociadosConfig sessionUsuarioCONFIGDTO
        {
            get { return session["objAsociadosConfig"] as MEBAAsociadosConfig; }
            set { session["objAsociadosConfig"] = value; }
        }
        public static MEBAProEmpresas sessionUsuarioEmpresa
        {
            get { return session["objEmpresa"] as MEBAProEmpresas; }
            set { session["objEmpresa"] = value; }
        }
        public static ModConse consecutivoReporte
        {
            get { return session["consecutivo"] as ModConse ; }
            set { session["consecutivo"] = value; }
        }

        public static List<MEBAReporteViewResumen> lstSessionResumen
        {
            get { return session["lstSessionResumen"] as List<MEBAReporteViewResumen>; }
            set { session["lstSessionResumen"] = value; }
        }

    }
}