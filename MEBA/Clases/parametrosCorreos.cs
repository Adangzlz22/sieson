﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace MEBA.Clases
{
    public class parametrosCorreos
    {
        public string Asunto { get; set; }
        public string HTMLCuerpoCorreo { get; set; }
        public string HTMLDestinatario { get; set; }
        public string HTMLDestinatarioCopia { get; set; }
        public string SMTPUsuario { get; set; }
        public string SMTPUsuarioContrasenia { get; set; }
        public string SMTPServidor { get; set; }
        public int SMTPPuerto { get; set; }
        public bool SMTPSSL { get; set; }
        public string Archivo { get; set; }
    }
}