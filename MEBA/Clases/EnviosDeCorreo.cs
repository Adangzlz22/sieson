﻿using MEBA.Models.ModGenerales;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Web;
using System.Net.Mime;

namespace MEBA.Clases
{
    public class EnviosDeCorreo
    {
        public static ClsModResponse EnviarCorreoParaAcceso(parametrosCorreos parametros)
        {
            ClsModResponse clsModResponse = new ClsModResponse();
            try
            {
                var correosDestinatarios = parametros.HTMLDestinatario.Split(',');
                var correosDestinatariosCopias = parametros.HTMLDestinatarioCopia.Split(',');
                SmtpClient smtp = new SmtpClient();
                smtp.Host = parametros.SMTPServidor;
                smtp.Port = parametros.SMTPPuerto;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(parametros.SMTPUsuario, parametros.SMTPUsuarioContrasenia);
                smtp.EnableSsl = parametros.SMTPSSL;

                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                MailMessage mm = new MailMessage();
                mm.IsBodyHtml = true;
                mm.Priority = MailPriority.Normal;
                mm.From = new MailAddress(parametros.SMTPUsuario);
                mm.Sender = new MailAddress(correosDestinatarios[0]);
                mm.Subject = parametros.Asunto + "";
                mm.Body = parametros.HTMLCuerpoCorreo;

                byte[] pdfBytes = Convert.FromBase64String(parametros.Archivo);
                MemoryStream pdfStream = new MemoryStream(pdfBytes);
                Attachment attachment = new Attachment(pdfStream, "Cotizacion-Seison.pdf", MediaTypeNames.Application.Pdf);

                mm.Attachments.Add(attachment);
                foreach (var item in correosDestinatarios)
                {
                    mm.To.Add(new MailAddress(item));
                }
                foreach (var item in correosDestinatariosCopias)
                {
                    mm.To.Add(new MailAddress(item));
                }
                smtp.Send(mm); // Enviar el mensaje

                clsModResponse.ITEMS = null;
                clsModResponse.SUCCESS = true;
                clsModResponse.MENSAJE = "Correo generado con exito";
            }
            catch (Exception ex)
            {
                clsModResponse.ITEMS = null;
                clsModResponse.SUCCESS = false;
                clsModResponse.MENSAJE = ex.Message;
            }
            return clsModResponse;
        }


    }
}