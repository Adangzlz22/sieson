﻿using MEBA.Models.ModUsuarios;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using MEBA.Models;

namespace MEBA.Clases
{
    public static class GlobalUtils
    {

        public static string CodigoUnico = ConfigurationManager.AppSettings["claveDeCodigo"];

        public static string obtenerSinEspacios(string n)
        {
            if (n != null)
            {
                return n.Trim();
            }
            else
            {
                return "";
            }
        }

   


    }
}