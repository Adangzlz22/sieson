﻿var ZonasControllers = function () {
    const url = '/Home/'

    let dtZonas;
    let lstGetStationData;

    const tblZonas = $('#tblZonas');
    const cboEstaciones = $('#cboEstaciones');
    const btnCancelar = $('#btnCancelar');
    const btnGuardar = $('#btnGuardar');
    const txtNombre = $('#txtNombre');
    const btnNuevo = $('#btnNuevo');
    const txtIdZona = $('#txtIdZona');
    const txtAccion = $('#txtAccion');
    const txtAccionEliminar = $('#txtAccionEliminar');
    const txtIdZonaEliminar = $('#txtIdZonaEliminar');
    const btnEliminarZona = $('#btnEliminarZona');
    
    

    var Inicializar = function () {
            initUsuarios();
            obtener();
            btnCancelar.click(function () {
                $('#mdlAgregarEditar').modal('hide');
        })
        btnGuardar.click(function () {
            postListadoZonas();
        })
        btnNuevo.click(function () {
            txtAccion.val('A')
            txtIdZona.val(0)
            $('#mdlAgregarEditar').modal('show');
        })
        btnEliminarZona.click(function () {
            var accion = txtAccionEliminar.val();
            var idzona = txtIdZonaEliminar.val();
            txtAccion.val(accion);
            txtIdZona.val(idzona);
            postListadoZonas();
        })
        }
        const obtener = function () {
            let parametros = {
            }
            const options = url + 'ObtenerConfig';
            axios.post(options, parametros).then(function (response) {
                const result = response.data;
                if (result.SUCCESS == true) {
                    if (result.ITEMS.SitioWEB != 'local') {
                        TodosIdentificadores(result.ITEMS);
                    } else {
                        local();
                    }
                } else {
                }
            }).catch(function (error) {
                console.error(error);
            });
        }

        const TodosIdentificadores = function (objUrl) {
            let parametros = {
            }
            const options = url + 'TodosIdentificadores';
            axios.post(options, parametros).then(function (response) {
                const result = response.data;
                if (result.SUCCESS == true) {
                    GenerarUrlMedianteApi(objUrl, result.ITEMS);
                } else {
                }
            }).catch(function (error) {
                console.error(error);
            });
        }

        const GenerarUrlMedianteApi = function (objUrl, Identificadores) {

            let parametros = {
                Usuario: objUrl.usuarioApi,
                Password: objUrl.passwordApi,
                Url: objUrl.urlApi,
                EndPoint: 'GetToken'
            }
            const options = url + 'LoginApiSistema';
            axios.post(options, parametros).then(function (response) {
                const result = JSON.parse(response.data);
                GetStationData(objUrl, result.Data, Identificadores)


            }).catch(function (error) {
                console.error(error);
            });
        }
        const GetStationData = function (objUrl, Token, Identificadores) {
            let item = {
                token: Token,
                stationId: -1,
                Identifier: -1
            }
            let parametros = {
                Url: objUrl.urlApi,
                EndPoint: 'GetStationData',
                objParams: JSON.stringify(item)
            }
            const options = url + 'EndPointNexus';
            axios.post(options, parametros).then(function (response) {
                const result = JSON.parse(response.data);

                lstGetStationData = result.Data;
                let html = ``;
                lstGetStationData.forEach(x => {
                    if (x != undefined) {
                        html += `<option value="${x.StationId}">${x.StationNumber} -  ${x.StationName}</option>`;
                    }
                });
                html += ``;
                cboEstaciones.append(html);
               

                obtenerUsuarios();

            }).catch(function (error) {
                console.error(error);
            });
        }

        const local = function () {
            let parametros = {
                identificador: 50,
            }
            const options = url + 'StpGetLastCoreProcessDate';
            axios.post(options, parametros).then(function (response) {
                const result = response.data;

            }).catch(function (error) {
                console.error(error);
            });
        }

    var initUsuarios = function () {
        dtZonas = tblZonas.DataTable({
                language: 'dtDicEsp',
                ordering: true,
                paging: true,
                searching: true,
                bFilter: false,
                info: false,
                columns: [
                    { data: 'IdZona', title: 'id', visible: false },
                    {
                        title: 'Zona', render: (data, type, row) => {
                            let html = validarEspacios(row.Zona);
                            return html;
                        }
                    },
                    {
                        title: 'Estaciones', width:'60%', render: (data, type, row) => {
                            let html = '';
                            let cont = 0;
                            row.lstEstacionesAsignadas.forEach(x => {
                                if (cont==3) {
                                    html += '<br>';
                                    cont = 0;
                                }
                                html += `<span class='bg-estaciones'> ${ExtraerNombre(x)}</span>&nbsp `
                                cont++;
                            });
                            return html;
                        }
                    },
                    {
                        title: 'Acciones', render: function (data, type, row) {
                            var html = `<button class='btn-eliminar btn btn-danger EliminarUsuarios' data-id='${row.IdUsuario}'>` +
                                `<i class='fas fa-trash' style='font-size:18px'></i>` +
                                `</button>`;

                            return `<button class='btn-editar btn btn-primary editarUsuarios' data-id='${row.IdUsuario}'>` +
                                `<i class='fas fa-pencil-alt' style='font-size:18px'></i>` +
                                `</button>&nbsp;` + html;
                        }
                    }
                ],
                columnDefs: [
                    { className: 'dt-center', 'targets': '_all' },
                    { 'width': '20%', 'targets': [1, 3] }
                ],
                initComplete: function (settings, json) {

                    tblZonas.on('click', '.editarUsuarios', function (e) {
                        const rowData = dtZonas.row($(this).closest('tr')).data();
                        $('#mdlAgregarEditar').modal('show');
                        txtAccion.val('M');
                        txtNombre.val(rowData.Zona);
                        txtIdZona.val(rowData.IdZona);

                        cboEstaciones.val(rowData.lstEstacionesAsignadas);
                        cboEstaciones.trigger('change'); 


                    });


                    tblZonas.on('click', '.EliminarUsuarios', function (e) {
                        const rowData = dtZonas.row($(this).closest('tr')).data();
                        $('#mdlBorrarEditar').modal('show');
                        txtAccionEliminar.val('B');
                        txtIdZonaEliminar.val(rowData.IdZona);



                    });


                }
            });

        $('#tblZonas_length').css('margin-bottom', '5px');

        //$('#tblZonas_length').css('margin-left', '30px');
        $('#tblZonas_length').find('select').css('border', '1px solid #e5eaed');
        $('#tblZonas_length').find('select').css('border-radius', '.5rem');
        $('#tblZonas_length').find('select').css('padding', '.5rem .75rem');
        $('#tblZonas_length').find('select').css('color', '#5e6278');
        $('#tblZonas_length').find('select').css('margin-left', '10px');
        $('#tblZonas_length').find('select').css('margin-right', '10px');
        $('#tblZonas_length').find('select').css('outline', 'none');
        $('#tblZonas_length').find('label').css('color', '#05a692');
        $('#tblZonas_length').find('label').css('font-weight', '600');
        $('#tblZonas_length').find('label').css('font-size', '15px');
        $('#tblZonas_paginate').css('text-align', 'right');
        $('#tblZonas_paginate').css('min-height', '30px');
       

        $('#tblZonas_filter').find('label').css('color', '#05a692');
        $('#tblZonas_filter').find('label').css('font-weight', '900');
        $('#tblZonas_filter').find('label').css('font-size', '20px');
        $('#tblZonas_filter').find('label').css('order', '2');
        $('#tblZonas_filter').find('input').css('margin-left', '5px');
        $('#tblZonas_filter').find('input').css('border', '1px solid #e5eaed');
        $('#tblZonas_filter').find('input').css('border-radius', '.5rem');
        $('#tblZonas_filter').find('input').css('padding', '.5rem .75rem');
        $('#tblZonas_filter').find('input').css('outline', 'none');
        $('#tblZonas_filter').find('input').css('color', '#5e6278');
        $('#tblZonas_filter').find('input').css('font-size', '15px');
        $('#tblZonas_filter').css('text-align', 'right');


    }
    var ExtraerNombre = function (stationId) {
        let Estacion = "";
        if (lstGetStationData.lenght != 0) {
            let d = $.grep(lstGetStationData, function (el, index) { return (el.StationId == stationId) });
            Estacion = d[0].StationNumber + " - " + d[0].StationName
        }
        return Estacion;
    }
        var validarEspacios = function (str) {
            if (str == null) {
                str = '';
            }
            return str;
        }
        const obtenerUsuarios = function () {
            let parametros = {
            }
            const options = '/Admin/Admin/' + 'GetListadoZonas';
            axios.post(options, parametros).then(function (response) {
                const result = response.data;
                if (result.SUCCESS == true) {
                    dtZonas.clear();
                    dtZonas.rows.add(result.ITEMS);
                    dtZonas.draw();
                } else {
                }
            }).catch(function (error) {
                console.error(error);
            });
    }
  

    const postListadoZonas = function () {
        let parametros = {
            Accion: txtAccion.val(),
            IdZona: txtIdZona.val(),
            Zona: txtNombre.val(),
            lstEstacionesAsignadas: cboEstaciones.val(),
        }
        const options = '/Admin/Admin/' + 'postListadoZonas';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                obtenerUsuarios();
                $('#mdlAgregarEditar').modal('hide');
                $('#mdlBorrarEditar').modal('hide');
            } else {
            }
        }).catch(function (error) {
            console.error(error);
        });
    }


        return {
            Inicializar: Inicializar,
        }
    };
