﻿var UsuariosControllers = function () {
    const url = '/Admin/Admin/'
    const tblUsuarios = $('#tblUsuarios');
    let dtUsuarios;

    const txtNombre = $('#txtNombre');
    const txtApeidoP = $('#txtApeidoP');
    const txtApeidoM = $('#txtApeidoM');
    const txtCorreo = $('#txtCorreo');
    const txtTelefono = $('#txtTelefono');
    const txtUsuario = $('#txtUsuario');
    const txtContrasena = $('#txtContrasena');
    const cmbGrupo = $('#cmbGrupo');
    const cmbRol = $('#cmbRol');
    const btnGuardar = $('#btnGuardar');
    const btnCancelar = $('#btnCancelar');
    const txtIdUsuario = $('#txtIdUsuario');
    const btnAgregar = $('#btnAgregar');
    const titleUsuario = $('#titleUsuario');
    const txtIdUsuarioEliminar = $('#txtIdUsuarioEliminar');
    const btnEliminar = $('#btnEliminar');
    const txtAccion = $('#txtAccion');
    const txtBorrar = $('#txtBorrar');
    const inpEstatus = $('#inpEstatus');
    const cboZonas = $('#cboZonas');
    const conSelects = $('#conSelects');
    const btnGuardarMenu = $('#btnGuardarMenu');


    let lstResultados = [];

    var Inicializar = function () {
        GetlistaadoZonas();
        functionListar();
    }
    const functionListar = function () {
        initUsuarios();
        obtener();
        obtenerListaRol();
        GetListadoEmpresas();




        cmbGrupo.trigger('change');
        btnGuardar.click(function () {
            postAgregarModificarEliminar(txtAccion.val());
        });
        btnCancelar.click(function () {
            $('#mdlAgregarEditar').modal('hide');
        });
        btnAgregar.click(function () {
            cboZonas.val(0);
            cboZonas.trigger('change');

            cmbGrupo.val('b64027dd93594c4dadbe1fb74afd0553    ');
            cmbGrupo.trigger('change');

            cmbRol.val(1);
            cmbRol.trigger('change');

            $('#mdlAgregarEditar').modal('show');
            $('.select2').css('width', '100%')
            txtAccion.val('A');
            txtIdUsuario.val(0);


            txtNombre.val('');
            txtApeidoP.val('');
            txtApeidoM.val('');
            txtCorreo.val('');
            txtTelefono.val('');
            txtUsuario.val('');
            txtContrasena.val('');
            cmbGrupo.val('');
            cmbRol.val('');

            titleUsuario.text('Agregar Usuario');
            $('.btn-close').css('background-color', '#FFF');
        });
        btnEliminar.click(function () {
            $('#mdlEliminar').modal('hide');
            postAgregarModificarEliminar('B');
        })
        btnGuardarMenu.click(function () {
            funcGuardar();
        })
    }
    var initUsuarios = function () {
        dtUsuarios = tblUsuarios.DataTable({
            language: 'dtDicEsp',
            ordering: true,
            paging: true,
            searching: true,
            bFilter: false,
            info: false,
            columns: [
                { data: 'IdUsuario', title: 'id', visible: false },
                {
                    title: 'Estatus', render: (data, type, row) => {
                        let html = '';
                        if (row.Estatus == true) {
                            html = 'Activo';
                        } else {
                            html = 'Inactivo';
                        }
                        return html;
                    }
                },
                {
                    title: 'Grupo', render: (data, type, row) => {
                        let html = validarEspacios(row.objEmpresa.NombreComercial);
                        return html;
                    }
                },
                {
                    title: 'Nombre', render: (data, type, row) => {
                        let html = validarEspacios(row.Nombre) + ' ' + validarEspacios(row.ApellidoPaterno) + ' ' + validarEspacios(row.ApellidoMaterno);
                        return html;
                    }
                },
                {
                    title: 'Rol', render: (data, type, row) => {
                        let html = '';

                        if (row.objRol != null) {
                            html = validarEspacios(row.objRol.Descripcion);
                        }
                        return html;
                    }
                },
                { data: 'UserName', title: 'Usuario' },
                { data: 'Correo', title: 'Correo' },
                { data: 'Telefono', title: 'Teléfono' },

                {
                    title: 'Acciones', render: function (data, type, row) {
                        let btnEliminar = '';
                        let btnMdlMenu = '';


                        btnMdlMenu = `<button class='btn-AgregarMenu btn btn-warning menuUsuarios' data-id='${row.IdUsuario}'>` +
                            `<i class='fas fa-bars' style='font-size:18px'></i></button>`;


                        if (row.Estatus == true) {
                            btnEliminar = `<button class='btn-eliminar btn btn-danger eliminarUsuarios' data-id='${row.IdUsuario}'>` +
                                `<i class='fas fa-toggle-on' style='font-size:18px'></i></button>`;
                        } else {
                            btnEliminar = `<button class='btn-eliminar btn btn-warning ActivarUsuarios' data-id='${row.IdUsuario}'>` +
                                `<i class='fas fa-toggle-on' style='font-size:18px'></i></button>`;
                        }
                        btnMdlMenu += `<button class='btn-editar btn btn-primary editarUsuarios' data-id='${row.IdUsuario}'>` +
                            `<i class='fas fa-pencil-alt' style='font-size:18px'></i>` +
                            `</button>` + btnEliminar;
                        //btnMdlMenu += `<button class='btn-AgregarMenu btn btn-primary estaciones' data-id='${row.IdUsuario}'>` +
                        //    `<i class='fa-solid fa-gas-pump' style='font-size:18px'></i></button>`;
                        btnMdlMenu += `<button class='btn-AgregarTema btn btn-primary temas' data-id='${row.IdUsuario}'>` +
                            `<i class='fa-solid fa-paint-brush' style='font-size:18px'></i></button>`;

                        return btnMdlMenu;

                    }
                }
            ],
            columnDefs: [
                { className: 'dt-center', 'targets': '_all' },
                { 'width': '20%', 'targets': [0, 2, 3] },
                { 'width': '25%', 'targets': [8] }
            ],
            initComplete: function (settings, json) {
                tblUsuarios.on('click', '.menuUsuarios', function () {
                    const rowData = dtUsuarios.row($(this).closest('tr')).data();


                    postCargarModalMenu(rowData.IdUsuario);

                    $('#mdlMenu').modal('show');

                });
                tblUsuarios.on('click', '.estaciones', function () {
                    const rowData = dtUsuarios.row($(this).closest('tr')).data();


                    postCargarModalMenu(rowData.IdUsuario);

                    $('#mdlEstaciones').modal('show');

                });
                tblUsuarios.on('click', '.temas', function () {
                    const rowData = dtUsuarios.row($(this).closest('tr')).data();


                    postCargarModalMenu(rowData.IdUsuario);

                    $('#mdlTemasUsuarios').modal('show');

                });
                tblUsuarios.on('click', '.ActivarUsuarios', function () {
                    const rowData = dtUsuarios.row($(this).closest('tr')).data();
                    var str = "";
                    if (rowData.Estatus == true) {
                        str = " ¿Esta seguro de querer Desactivar este usuario?"
                        btnEliminar.text('Desactivar');
                        inpEstatus.val(false);
                    } else {
                        str = " ¿Esta seguro de querer Activar este usuario?"
                        btnEliminar.text('Activar');
                        inpEstatus.val(true);
                    }
                    txtBorrar.text(str);
                    txtIdUsuario.val(rowData.IdUsuario);
                    txtIdUsuarioEliminar.val(rowData.IdUsuario);
                    $('#mdlEliminar').modal('show');

                });
                tblUsuarios.on('click', '.eliminarUsuarios', function () {
                    const rowData = dtUsuarios.row($(this).closest('tr')).data();
                    var str = "";
                    if (rowData.Estatus == true) {
                        str = " ¿Esta seguro de querer Desactivar este usuario?"
                        btnEliminar.text('Desactivar');
                        inpEstatus.val(false);
                    } else {
                        str = " ¿Esta seguro de querer Activar este usuario?"
                        btnEliminar.text('Activar');
                        inpEstatus.val(true);
                    }
                    txtBorrar.text(str);
                    txtIdUsuario.val(rowData.IdUsuario);
                    txtIdUsuarioEliminar.val(rowData.IdUsuario);
                    $('#mdlEliminar').modal('show');

                });
                tblUsuarios.on('click', '.editarUsuarios', function (e) {
                    const rowData = dtUsuarios.row($(this).closest('tr')).data();
                    $('#mdlAgregarEditar').modal('show');
                    $('.select2').css('width', '100%')
                    txtIdUsuario.val(rowData.IdUsuario);
                    txtAccion.val('M');
                    titleUsuario.text('Editar Usuario');
                    txtNombre.val(rowData.Nombre);
                    txtApeidoP.val(rowData.ApellidoPaterno);
                    txtApeidoM.val(rowData.ApellidoMaterno);
                    txtCorreo.val(rowData.Correo);
                    txtTelefono.val(rowData.Telefono);
                    txtUsuario.val(rowData.UserName);
                    txtContrasena.val(rowData.Password);
                    cmbGrupo.val(rowData.GuidEmpresa);
                    cmbGrupo.trigger('change');
                    cmbRol.val(rowData.idRol);
                    cmbRol.trigger('change');
                    cboZonas.val(rowData.lstIdZonas);
                    cboZonas.trigger('change');
                });
            }
        });

        $('#tblUsuarios_length').css('margin-bottom', '5px');

        //$('#tblUsuarios_length').css('margin-left', '30px');
        $('#tblUsuarios_length').find('select').css('border', '1px solid #e5eaed');
        $('#tblUsuarios_length').find('select').css('border-radius', '.5rem');
        $('#tblUsuarios_length').find('select').css('padding', '.5rem .75rem');
        $('#tblUsuarios_length').find('select').css('color', '#5e6278');
        $('#tblUsuarios_length').find('select').css('margin-left', '10px');
        $('#tblUsuarios_length').find('select').css('margin-right', '10px');
        $('#tblUsuarios_length').find('select').css('outline', 'none');
        //$('#tblUsuarios_length').find('label').css('color', '#05a692');
        //$('#tblUsuarios_length').find('label').css('font-weight', '600');
        //$('#tblUsuarios_length').find('label').css('font-size', '15px');
        $('#tblUsuarios_paginate').css('text-align', 'right');
        $('#tblUsuarios_paginate').css('min-height', '30px');

        //$('#tblUsuarios_filter').find('label').css('color', '#05a692');
        //$('#tblUsuarios_filter').find('label').css('font-weight', '900');
        //$('#tblUsuarios_filter').find('label').css('font-size', '20px');
        $('#tblUsuarios_filter').find('label').css('order', '2');
        $('#tblUsuarios_filter').find('input').css('margin-left', '5px');
        $('#tblUsuarios_filter').find('input').css('border', '1px solid #e5eaed');
        $('#tblUsuarios_filter').find('input').css('border-radius', '.5rem');
        $('#tblUsuarios_filter').find('input').css('padding', '.5rem .75rem');
        $('#tblUsuarios_filter').find('input').css('outline', 'none');
        $('#tblUsuarios_filter').find('input').css('color', '#5e6278');
        $('#tblUsuarios_filter').find('input').css('font-size', '15px');
        $('#tblUsuarios_filter').css('text-align', 'right');


    }
    var validarEspacios = function (str) {
        if (str == null) {
            str = '';
        }
        return str;
    }
    const obtener = function () {
        funesperar(0, 'Porfavor espere unos segundos.');

        let parametros = {
        }
        const options = url + 'GetListadoUsuarios';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                dtUsuarios.clear();
                dtUsuarios.rows.add(result.ITEMS);
                dtUsuarios.draw();
                funesperar(1, '');

            } else {
            }
        }).catch(function (error) {
            console.error(error);
        });
    }
    const obtenerListaRol = function () {
        let parametros = {
        }
        const options = url + 'GetListadoRol';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                let html = "";
                result.ITEMS.forEach(x => {
                    html += `<option value="${x.value}">${x.text}</option>`;
                });
                cmbRol.append(html);
                cmbRol.select2();
            } else {
            }
        }).catch(function (error) {
            console.error(error);
        });
    }
    const GetListadoEmpresas = function () {
        let parametros = {
        }
        const options = url + 'GetListadoEmpresas';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                let html = "";
                result.ITEMS.forEach(x => {
                    html += `<option value="${x.value}">${x.text}</option>`;
                });
                cmbGrupo.append(html);
                cmbGrupo.select2();




            } else {
            }
        }).catch(function (error) {
            console.error(error);
        });
    }
    const GetlistaadoZonas = function () {
        let parametros = {
        }
        const options = url + 'cboZonas';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                let html = "";
                result.ITEMS.forEach(x => {
                    html += `<option value="${x.value}">${x.text}</option>`;
                });
                cboZonas.append(html);
                cboZonas.select2();

            } else {
            }
        }).catch(function (error) {
            console.error(error);
        });
    }
    const postAgregarModificarEliminar = function (Accion) {
        if (Accion == "B") {

            let parametros = {
                Accion: Accion,
                IdUsuario: txtIdUsuario.val(),
                Nombre: txtNombre.val(),
                ApeidoP: txtApeidoP.val(),
                ApeidoM: txtApeidoM.val(),
                Correo: txtCorreo.val(),
                Telefono: txtTelefono.val(),
                Usuario: txtUsuario.val(),
                Contrasena: txtContrasena.val(),
                Grupo: cmbGrupo.val(),
                Rol: cmbRol.val(),
                Estatus: inpEstatus.val(),
                lstIdZonas: cboZonas.val()
            }
            const options = url + 'postAgregarModificarEliminar';
            axios.post(options, parametros).then(function (response) {
                const result = response.data;
                obtener();
            }).catch(function (error) {
                console.error(error);
            });
        } else {
            if (cmbRol.val() == null && cmbGrupo.val() == null) {
                Swal.fire('Seleccione los campos de rol y grupo');
                return;
            } else {

                let parametros = {
                    Accion: Accion,
                    IdUsuario: txtIdUsuario.val(),
                    Nombre: txtNombre.val(),
                    ApeidoP: txtApeidoP.val(),
                    ApeidoM: txtApeidoM.val(),
                    Correo: txtCorreo.val(),
                    Telefono: txtTelefono.val(),
                    Usuario: txtUsuario.val(),
                    Contrasena: txtContrasena.val(),
                    Grupo: cmbGrupo.val(),
                    Rol: cmbRol.val(),
                    Estatus: inpEstatus.val(),
                    lstIdZonas: cboZonas.val()
                }
                const options = url + 'postAgregarModificarEliminar';
                axios.post(options, parametros).then(function (response) {
                    const result = response.data;
                    console.log(result)
                    if (result.SUCCESS == true) {
                        $('#mdlAgregarEditar').modal('hide');
                        obtener();
                    } else {
                        funesperar(4000, result.ITEMS);
                    }
                }).catch(function (error) {
                    console.error(error);
                });
            }
        }
    }

    const postCargarModalMenu = function (idUsuario) {

        const options = url + 'postCargarModalMenu';
        axios.post(options, { idUsuario: idUsuario }).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                let html = '';


                conSelects.find('tr').remove();


                let Cabezera = '';
                let descripcion = '';
                let finalparte = 0;
                let cuerpo = '';
                let footer = '';
                for (var i = 0; i < result.ITEMS.length; i++) {

                    //if (result.ITEMS[i].contador >= 1) {
                    //    finalparte++;

                    //    if (descripcion != result.ITEMS[i].MenuDescripcionPadre) {
                    //        descripcion = result.ITEMS[i].MenuDescripcionPadre;

                    //        Cabezera = '';
                    //        cuerpo = '';
                    //        footer = '';

                    //        Cabezera = `
                    //        <tr>
                    //            <td style="text-align:left!important"><p>${result.ITEMS[i].MenuDescripcionPadre}</p></td>
                    //            <td>
                    //                <i class="fa-solid fa-circle-plus" style="font-size:25px; color:#cdcfd1" data-bs-toggle="collapse" href="#collapseOne"></i>
                    //            </td>
                    //        </tr>
                    //        <tr>
                    //            <td colspan="2">
                    //                <div class="card collapse" id="collapseOne"  data-bs-parent="#accordion">
                    //                    <div class="card-body text-left">
                    //                     `;
                    //    }

                    //    cuerpo += `
                    //                            <div class="col-lg-6">
                    //                                <p>${result.ITEMS[i].MenuDescripcionHijo}</p>
                    //                            </div>
                    //                            <div class="col-lg-6">
                    //                                <div class="form-check">
                    //                                    <input class="form-check-input" type="checkbox" id="check7" name="option1" value="${result.ITEMS[i].check}" style="float:initial!important">
                    //                                </div>
                    //                            </div>
                    //    `;
                    //    if (finalparte == result.ITEMS[i].contador) {
                    //        footer = `
                    //                        </div>
                    //                    </div>
                    //                </td>
                    //            </tr>
                    //        `;
                    //        finalparte = 0;

                    //        html += Cabezera + cuerpo + footer;
                    //    }

                    //} else {
                    html += `
                            <tr>
                                <td style="text-align:left!important"><p>${result.ITEMS[i].MenuDescripcionPadre}  ${result.ITEMS[i].MenuDescripcionHijo == '' ? '' : ' - ' + result.ITEMS[i].MenuDescripcionHijo}</p></td>
                                <td>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="${result.ITEMS[i].MenuDescripcionPadre + result.ITEMS[i].OIDMenuPadre + result.ITEMS[i].OIDMenuHijo}" 
                                             data-OIDUsuario="${result.ITEMS[i].OIDUsuario}" data-OIDMenuPadre="${result.ITEMS[i].OIDMenuPadre}" data-OIDMenuHijo="${result.ITEMS[i].OIDMenuHijo}"   name="option1" style="float:initial!important">
                                    </div>
                                </td>
                            </tr>
                        `;
                    //}
                }

                conSelects.append(html);
                lstResultados = [];
                for (var i = 0; i < result.ITEMS.length; i++) {
                    lstResultados.push(result.ITEMS[i]);
                    $(`#${result.ITEMS[i].MenuDescripcionPadre + result.ITEMS[i].OIDMenuPadre + result.ITEMS[i].OIDMenuHijo}`).prop('checked', result.ITEMS[i].check);
                }

            } else {
            }
        }).catch(function (error) {
            console.error(error);
        });
    }
    const funcGuardar = function () {
        let Guardar = [];
        for (var i = 0; i < lstResultados.length; i++) {
            let item = {
                contador: 0,
                OIDUsuario: $(`#${lstResultados[i].MenuDescripcionPadre + lstResultados[i].OIDMenuPadre + lstResultados[i].OIDMenuHijo}`).attr('data-OIDUsuario'),
                OIDMenuPadre: $(`#${lstResultados[i].MenuDescripcionPadre + lstResultados[i].OIDMenuPadre + lstResultados[i].OIDMenuHijo}`).attr('data-OIDMenuPadre'),
                OIDMenuHijo: $(`#${lstResultados[i].MenuDescripcionPadre + lstResultados[i].OIDMenuPadre + lstResultados[i].OIDMenuHijo}`).attr('data-OIDMenuHijo'),
                MenuDescripcionPadre: '',
                MenuDescripcionHijo: '',
                check: $(`#${lstResultados[i].MenuDescripcionPadre + lstResultados[i].OIDMenuPadre + lstResultados[i].OIDMenuHijo}`).prop('checked'),
            }
            Guardar.push(item);
        }

        postObtenerMenuSeleccionado(Guardar);
    }
    const postObtenerMenuSeleccionado = function (lstParametros) {

        let parametros = lstParametros;

        const options = url + 'postInsertarMenu';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                funesperarExito(3000, 'Guardado Exitoso');
            } else {
                funesperar(5000, result.MENSAJE);
            }
        }).catch(function (error) {
            console.error(error);
        });
    }


    const funesperar = function (timer, texto) {
        $('.select2').css('width', '100%')
        let timerInterval
        Swal.fire({
            title: 'Alerta!',
            text: texto,
            icon: 'info',
            timer: timer,
            timerProgressBar: true,
            allowOutsideClick: false,
            didOpen: () => {
                $('.select2').css('width', '100%')
                Swal.showLoading()
                const b = Swal.getHtmlContainer().querySelector('b')
                timerInterval = setInterval(() => {
                    //b.textContent = Swal.getTimerLeft()
                }, 100)
                $('.select2').css('width', '100%')
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
            }
        })
    }

    const funesperarExito = function (timer, texto) {
        let timerInterval
        Swal.fire({
            title: 'Mensaje!',
            text: texto,
            icon: 'success',
            timer: timer,
            timerProgressBar: true,
            allowOutsideClick: false,
            didOpen: () => {
                Swal.showLoading()
                const b = Swal.getHtmlContainer().querySelector('b')
                $('#mdlMenu').modal('hide');
                timerInterval = setInterval(() => {
                    //b.textContent = Swal.getTimerLeft()
                }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
            }
        })
    }

    return {
        Inicializar: Inicializar,
    }
};