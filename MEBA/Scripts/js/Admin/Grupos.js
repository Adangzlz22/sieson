﻿var GruposControllers = function () {
    const url = '/Admin/Admin/'
    const tblGrupos = $('#tblGrupos');
    let dtGrupos;
    // const mdlGrupo = $('#mdlGrupo');
    const txtTituloModal = $('#txtTituloModal');

    const btnAgregarGrupo = $('#btnAgregarGrupo');
    const txtNombreComercial = $('#txtNombreComercial');
    const txtTelefono = $('#txtTelefono');
    const txtCalle = $('#txtCalle');
    const txtCodigoPostal = $('#txtCodigoPostal');
    // const txtPais = $('#txtPais');
    // const txtEstado = $('#txtEstado');
    // const txtMunicipio = $('#txtMunicipio');
    const cmbConexion = $('#cmbConexion');
    const txtSitioWeb = $('#txtSitioWeb');
    const txtServerSeguridad = $('#txtServerSeguridad');
    const txtUsuarioSQL = $('#txtUsuarioSQL');
    const txtPasswordSQL = $('#txtPasswordSQL');
    const txtBaseProduccion = $('#txtBaseProduccion');
    const inpIdEmpresa = $('#inpIdEmpresa');

    const txtUsuarioApi = $('#txtUsuarioApi');
    const txtPasswordApi = $('#txtPasswordApi');
    const txtUrlApi = $('#txtUrlApi');

    const conApi = $('#conApi');
    const conLocal = $('#conLocal');
    const btnGuardarEditar = $('#btnGuardarEditar');
    const inpTipoAccion = $('#inpTipoAccion');
    const btnEliminar = $('#btnEliminar');

    const btnGuardarEditarAsociado = $('#btnGuardarEditarAsociado');
    const btnAgregarContacto = $('#btnAgregarContacto');
    const btnCancelarAsociado = $('#btnCancelarAsociado');

    const txtNombreAsociado = $('#txtNombreAsociado');
    const txtTelefonoAsociado = $('#txtTelefonoAsociado');
    const txtCorreoAsociado = $('#txtCorreoAsociado');
    const inpGuidEmpresa = $('#inpGuidEmpresa');
    const inpIdAsociado = $('#inpIdAsociado');
    const inpIdAsociadoAccion = $('#inpIdAsociadoAccion');
    const inpIdContacto = $('#inpIdContacto');
    const btnEliminarContacto = $('#btnEliminarContacto');
    const txtBorrar = $('#txtBorrar');
    const inpEstatus = $('#inpEstatus');

    const cmbEstado = $('#cmbEstado');
    const cmbMunicipio = $('#cmbMunicipio');
    const cmbRol = $('#cmbRol');

    const btnVerificar = $('#btnVerificar');
    const lblVerificarApi = $('#lblVerificarApi');

    const checkTiempo = $('#checkTiempo');
    const checkTurno = $('#checkTurno');
    const checkXml = $('#checkXml');
    const checkInventario = $('#checkInventario');



    const tblAsociados = $('#tblContactosAsosiados');
    let dtAsociados;




    var Inicializar = function () {


        functionListar();
    }
    const functionListar = function () {
        obtenerListaRol();
        initGrupos();
        initAsociados();
        obtener();
        obtenerEstados();
        obtenerMunicipio(1);
        cmbEstado.change(function () {
            obtenerMunicipio(cmbEstado.val());
        });
        btnAgregarGrupo.click(function () {
            lblVerificarApi.val('');
            inpTipoAccion.val('A');
            inpIdEmpresa.val(0);
            obtenerEstados();
            cmbEstado.val(1);
            obtenerMunicipio(1);
            cmbMunicipio.val(1);

            checkTiempo.prop('checked', false)
            checkTurno.prop('checked', false)
            checkXml.prop('checked', false)
            checkInventario.prop('checked', false)
            

            cmbConexion.val(1)
            txtSitioWeb.val('');
            inpIdEmpresa.val('');
            txtServerSeguridad.val('');
            txtUsuarioSQL.val('');
            txtPasswordSQL.val('');
            txtBaseProduccion.val('');
            txtUrlApi.val('');
            txtUsuarioApi.val('');
            txtPasswordApi.val('');
            txtNombreComercial.val('');
            txtTelefono.val('');
            txtCalle.val('');
            txtCodigoPostal.val('');
            if (cmbConexion.val() == 1) {
                conLocal.css('display', 'block');
                conApi.css('display', 'none');
            } else {
                conLocal.css('display', 'none');
                conApi.css('display', 'block');
            }

            $('#mdlGrupo').modal('show');
            txtTituloModal.text('Agregar Grupo');
        });
        if (cmbConexion.val() == 1) {
            conApi.css('display', 'none');
        }
        cmbConexion.change(function () {
            if (cmbConexion.val() == 1) {
                conLocal.css('display', 'block');
                conApi.css('display', 'none');
            } else {
                conLocal.css('display', 'none');
                conApi.css('display', 'block');
            }
        });
        btnGuardarEditar.click(function () {
            postAgregarModificarEliminarGrupos();
        });
        btnEliminar.click(function () {
            postAgregarModificarEliminarGrupos();
        });
        btnCancelarAsociado.click(function () {
            $('#mdlAsociadosAgrEditar').modal('hide')
        });
        btnAgregarContacto.click(function () {
            inpIdAsociadoAccion.val('A');
            $('#mdlAsociadosAgrEditar').modal('show')

            inpIdContacto.val(0);
            txtNombreAsociado.val('');
            txtTelefonoAsociado.val('');
            txtCorreoAsociado.val('');
        });
        btnGuardarEditarAsociado.click(function () {
            postAgregarModificarEliminarAsociados();
        });
        btnEliminarContacto.click(function () {
            postAgregarModificarEliminarAsociados();
        });
        btnVerificar.click(function () {
            postObtenerConfirg();
        });

    }
    const postObtenerConfirg = function () {
        let parametros = {
            IDAsociado: inpIdAsociado.val()
        }
        const options = '/Home/' + 'ObtenerConfigGrupos';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                postToken(result.ITEMS);
            } else {
            }
        }).catch(function (error) {
            console.error(error);
        });
    }
    const postToken = function (objUrl) {

        let parametros = {
            Usuario: objUrl.usuarioApi,
            Password: objUrl.passwordApi,
            Url: objUrl.urlApi,
            EndPoint: 'GetToken'
        }
        const options = '/Home/' + 'LoginApiSistema';
        axios.post(options, parametros).then(function (response) {
            const result = JSON.parse(response.data);
            if (result != null) {
                console.log(result)
                lblVerificarApi.text('Conexion exitosa con la api. Respuesta de Api : ' + result.Data == '' ? result.Comment : result.Data);
            } else {
                lblVerificarApi.text('Fallo conexion con la api.');
            }
        }).catch(function (error) {
            lblVerificarApi.text('Servidor no encontrado.');
        });
    }
    const obtenerListaRol = function () {
        let parametros = {
        }
        const options = url + 'GetListadoRolContatos';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                let html = "";
                result.ITEMS.forEach(x => {
                    html += `<option value="${x.value}">${x.text}</option>`;
                });
                cmbRol.append(html);

            } else {
            }
        }).catch(function (error) {
            console.error(error);
        });
    }

    const obtenerEstados = function () {
        let parametros = {
        }
        const options = url + 'GetListadoEstados';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                let html = '';
                result.ITEMS.forEach(x => {
                    html += `<option value='${x.value}'>${x.text}</option>`;
                });
                cmbEstado.append(html);
            } else {
            }
        }).catch(function (error) {
            console.error(error);
        });
    }
    const obtenerMunicipio = function (IdEstado, IdMunicipio = 1) {
        cmbMunicipio.find('option').remove();
        let parametros = {

        }
        const options = url + 'GetListadoMunicipios?IdEstado=' + IdEstado;
        axios.get(options).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                let html = '';
                result.ITEMS.forEach(x => {
                    html += `<option value='${x.value}'>${x.text}</option>`;
                });
                cmbMunicipio.append(html);
            } else {
            }
            cmbMunicipio.val(IdMunicipio);

        }).catch(function (error) {
            console.error(error);
        });
    }



    var initGrupos = function () {
        dtGrupos = tblGrupos.DataTable({
            language: 'dtDicEsp',
            ordering: true,
            paging: true,
            searching: true,
            bFilter: false,
            info: false,
            columns: [
                {
                    title: 'id', visible: false, render: (data, type, row) => {
                        let html = validarEspacios(row.objEmpresa.IDEmpresa);
                        return html;
                    }
                }, {
                    title: 'Estatus', render: (data, type, row) => {
                        let html = '';
                        if (row.objEmpresa.Estatus == true) {
                            html = 'Activo';
                        } else {
                            html = 'Desactivo';
                        }
                        return html;
                    }
                },
                {
                    title: 'Nombre', render: (data, type, row) => {
                        let html = validarEspacios(row.objEmpresa.NombreComercial);
                        return html;
                    }
                },
                {
                    title: 'Estado', render: (data, type, row) => {
                        let html = validarEspacios(row.objEstados.Estado);
                        return html;
                    }
                },
                {
                    title: 'Municipio', render: (data, type, row) => {
                        let html = '';
                        if (row.objMunicipios != null && row.objMunicipios.Municipio != '') {
                            html = validarEspacios(row.objMunicipios.Municipio);
                        }
                        return html;
                    }
                },
                {
                    title: 'Calle', visible: false, render: (data, type, row) => {
                        let html = validarEspacios(row.objEmpresa.Calle);
                        return html;
                    }
                }, {
                    title: 'CP', visible: false, render: (data, type, row) => {
                        let html = validarEspacios(row.objEmpresa.CP);
                        return html;
                    }
                }, {
                    title: 'Teléfono', visible: false, render: (data, type, row) => {
                        let html = validarEspacios(row.objEmpresa.Telefono);
                        return html;
                    }
                },


                {
                    title: 'Acciones', with: '20%', render: function (data, type, row) {
                        let btnEliminar = '';
                        if (row.objEmpresa.Estatus == true) {
                            btnEliminar = `<button class='btn-eliminar btn btn-danger eliminarGrupos' data-id='${row.IDEmpresa}'>` +
                                `<i class='fas fa-toggle-on' style='font-size:18px'></i></button>`;
                        } else {
                            btnEliminar = `<button class='btn-eliminar btn btn-warning ActivarGrupos' data-id='${row.IDEmpresa}'>` +
                                `<i class='fas fa-toggle-on' style='font-size:18px'></i></button>`;
                        }
                        return `<button class='btn-editar btn btn-primary editarGrupos' data-id='${row.IDEmpresa}'>` +
                            `<i class='fas fa-pencil-alt' style='font-size:18px'></i>` +
                            `</button>&nbsp;` + btnEliminar + `&nbsp;` + `<button class='btn-editar btn btn-info asociadosContacto' data-id='${row.IDEmpresa}' data-bs-toggle='modal' data-bs-target='#mdlAsociadostbl'>` +
                            `<i class='fa fa-user-circle' style='font-size:18px'></i>` + `</button>&nbsp;`;
                    }
                }
            ],
            columnDefs: [
                { className: 'dt-center', 'targets': '_all' },
                { 'width': '20%', 'targets': [0, 1, 2] }
            ],
            initComplete: function (settings, json) {

                tblGrupos.on('click', '.asociadosContacto', function () {
                    const rowData = dtGrupos.row($(this).closest('tr')).data();
                    $('#mdlAsociadostbl').modal('show');
                    inpGuidEmpresa.val(rowData.objEmpresa.IDAsociado);
                    inpIdAsociado.val(rowData.objEmpresa.IDAsociado);
                    postObtenerAsociados();

                });
                tblGrupos.on('click', '.ActivarGrupos', function () {
                    const rowData = dtGrupos.row($(this).closest('tr')).data();
                    var str = "";
                    if (rowData.objEmpresa.Estatus == true) {
                        str = " ¿Esta seguro de querer Desactivar este Grupo?"
                        btnEliminar.text('Desactivar');
                        inpEstatus.val(false);
                    } else {
                        str = " ¿Esta seguro de querer Activar este Grupo?"
                        btnEliminar.text('Activar');
                        inpEstatus.val(true);
                    }
                    txtBorrar.text(str);
                    inpIdEmpresa.val(rowData.objEmpresa.IDEmpresa);
                    inpTipoAccion.val('B');
                    $('#mdlEliminar').modal('show');

                });
                tblGrupos.on('click', '.eliminarGrupos', function () {
                    const rowData = dtGrupos.row($(this).closest('tr')).data();
                    var str = "";
                    if (rowData.objEmpresa.Estatus == true) {
                        str = " ¿Esta seguro de querer Desactivar este Grupo?"
                        btnEliminar.text('Desactivar');
                        inpEstatus.val(false);
                    } else {
                        str = " ¿Esta seguro de querer Activar este Grupo?"
                        btnEliminar.text('Activar');
                        inpEstatus.val(true);
                    }
                    txtBorrar.text(str);
                    inpIdEmpresa.val(rowData.objEmpresa.IDEmpresa);
                    inpTipoAccion.val('B');
                    $('#mdlEliminar').modal('show');

                });
                tblGrupos.on('click', '.editarGrupos', function (e) {
                    const rowData = dtGrupos.row($(this).closest('tr')).data();
                    inpTipoAccion.val('M');
                    if (rowData.objAsociadosConfig.SitioWEB == 'local') {
                        cmbConexion.val(1)
                    } else {
                        cmbConexion.val(2)
                    }
                    txtSitioWeb.val(rowData.objAsociadosConfig.SitioWEB)
                    lblVerificarApi.text('');
                    inpIdEmpresa.val(rowData.objEmpresa.IDEmpresa)
                    inpIdAsociado.val(rowData.objEmpresa.IDAsociado);
                    txtServerSeguridad.val(rowData.objAsociadosConfig.ServerSeguridad)
                    txtUsuarioSQL.val(rowData.objAsociadosConfig.UsuarioSQLSeguridad)
                    txtPasswordSQL.val(rowData.objAsociadosConfig.PasswordSQLSeguridad)
                    txtBaseProduccion.val(rowData.objAsociadosConfig.BaseProduccion)
                    txtUrlApi.val(rowData.objAsociadosConfig.urlApi)
                    txtUsuarioApi.val(rowData.objAsociadosConfig.usuarioApi)
                    txtPasswordApi.val(rowData.objAsociadosConfig.passwordApi)
                    obtenerEstados();
                    cmbEstado.val(rowData.objAsociados.Estado);
                    obtenerMunicipio(rowData.objAsociados.Estado, rowData.objAsociados.Municipio);

                    cmbMunicipio.val(rowData.objAsociados.Municipio);
                    txtNombreComercial.val(rowData.objEmpresa.NombreComercial)
                    txtTelefono.val(rowData.objEmpresa.Telefono)
                    txtCalle.val(rowData.objEmpresa.Calle)
                    txtCodigoPostal.val(rowData.objEmpresa.CP)

                    checkTiempo.prop('checked', rowData.objAsociados.MostrarTiempo)
                    checkTurno.prop('checked', rowData.objAsociados.MostrarTurno)
                    checkXml.prop('checked', rowData.objAsociados.MostrarXml)
                    checkInventario.prop('checked', rowData.objAsociados.MostrarInventarios)
                    
                    // txtPais.val(rowData.objEmpresa.Pais)
                    // txtEstado.val(rowData.objEmpresa.Estado)
                    // txtMunicipio.val(rowData.objEmpresa.Municipio)
                    if (cmbConexion.val() == 1) {
                        conLocal.css('display', 'block');
                        conApi.css('display', 'none');
                    } else {
                        conLocal.css('display', 'none');
                        conApi.css('display', 'block');
                    }
                    $('#mdlGrupo').modal('show');


                });
            }
        });


        $('#tblGrupos_length').css('margin-bottom', '5px');
        //$('#tblGrupos_length').css('margin-left', '30px');
        $('#tblGrupos_length').find('select').css('border', '1px solid #e5eaed');
        $('#tblGrupos_length').find('select').css('border-radius', '.5rem');
        $('#tblGrupos_length').find('select').css('padding', '.5rem .75rem');
        $('#tblGrupos_length').find('select').css('color', '#5e6278');
        $('#tblGrupos_length').find('select').css('margin-left', '10px');
        $('#tblGrupos_length').find('select').css('margin-right', '10px');
        $('#tblGrupos_length').find('select').css('outline', 'none');
        $('#tblGrupos_length').find('label').css('color', '#05a692');
        $('#tblGrupos_length').find('label').css('font-weight', '600');
        $('#tblGrupos_length').find('label').css('font-size', '15px');
        $('#tblGrupos_paginate').css('text-align', 'right');
        $('#tblGrupos_paginate').css('min-height', '30px');

        $('#tblGrupos_filter').find('label').css('color', '#05a692');
        $('#tblGrupos_filter').find('label').css('font-weight', '900');
        $('#tblGrupos_filter').find('label').css('font-size', '20px');
        $('#tblGrupos_filter').find('label').css('order', '2');
        $('#tblGrupos_filter').find('input').css('margin-left', '5px');
        $('#tblGrupos_filter').find('input').css('border', '1px solid #e5eaed');
        $('#tblGrupos_filter').find('input').css('border-radius', '.5rem');
        $('#tblGrupos_filter').find('input').css('padding', '.5rem .75rem');
        $('#tblGrupos_filter').find('input').css('outline', 'none');
        $('#tblGrupos_filter').find('input').css('color', '#5e6278');
        $('#tblGrupos_filter').find('input').css('font-size', '15px');
        $('#tblGrupos_filter').css('text-align', 'right');
    }
    var validarEspacios = function (str) {
        if (str == null) {
            str = '';
        }
        return str;
    }
    const obtener = function () {
        funesperar(0, 'Porfavor espere unos segundos.');

        let parametros = {
        }
        const options = url + 'GetListadoGrupo';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                console.log(result.ITEMS)
                dtGrupos.clear();
                dtGrupos.rows.add(result.ITEMS);
                dtGrupos.draw();
                funesperar(1, '');

            } else {
                $('#mdlMensaje').modal('show')
            }
        }).catch(function (error) {
            console.error(error);
        });
    }


    const postAgregarModificarEliminarGrupos = function (Accion) {
        let parametros = {
            Accion: inpTipoAccion.val(),
            TipoConexion: cmbConexion.val(),
            IDEmpresa: inpIdEmpresa.val(),
            SitioWEB: txtSitioWeb.val(),
            ServerSeguridad: txtServerSeguridad.val(),
            UsuarioSQLSeguridad: txtUsuarioSQL.val(),
            PasswordSQLSeguridad: txtPasswordSQL.val(),
            BaseProduccion: txtBaseProduccion.val(),
            urlApi: txtUrlApi.val(),
            usuarioApi: txtUsuarioApi.val(),
            passwordApi: txtPasswordApi.val(),
            RazonSocial: txtUrlApi.val(),
            NombreComercial: txtNombreComercial.val(),
            Telefono: txtTelefono.val(),
            Calle: txtCalle.val(),
            CP: txtCodigoPostal.val(),
            Estatus: inpEstatus.val(),
            // Pais : txtPais.val(),
            Estado: cmbEstado.val(),
            Municipio: cmbMunicipio.val(),
            ReprocesarEstados: false,
            MostrarTiempo: checkTiempo.prop('checked'),
            MostrarTurno: checkTurno.prop('checked'),
            MostrarXml: checkXml.prop('checked'),
            MostrarInventarios: checkInventario.prop('checked'),
            
        }
        const options = url + 'postAgregarModificarEliminarGrupos';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                obtener();
                $('#mdlGrupo').modal('hide');
                $('#mdlEliminar').modal('hide');
            } else {
                funesperar(4000, result.ITEMS);
            }


        }).catch(function (error) {
            console.error(error);
            $('#mdlGrupo').modal('hide');
            $('#mdlEliminar').modal('hide');
        });
    }



    const postObtenerAsociados = function () {
        let parametros = {
            IDAsociado: inpGuidEmpresa.val(),
        }
        const options = url + 'postObtenerAsociados';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                dtAsociados.clear();
                dtAsociados.rows.add(result.ITEMS);
                dtAsociados.draw();
            } else {
            }
            $('#mdlGrupo').modal('hide');
            $('#mdlEliminar').modal('hide');

        }).catch(function (error) {
            console.error(error);
            $('#mdlGrupo').modal('hide');
            $('#mdlEliminar').modal('hide');
        });
    }

    var initAsociados = function () {
        dtAsociados = tblAsociados.DataTable({
            language: 'dtDicEsp',
            ordering: true,
            paging: true,
            searching: false,
            bFilter: false,
            info: false,
            columns: [
                {
                    title: 'Nombre Asociado', render: (data, type, row) => {
                        let html = validarEspacios(row.Nombre);
                        return html;
                    }
                }, {
                    title: 'Rol', render: (data, type, row) => {
                        let html = validarEspacios(row.Rol);
                        return html;
                    }
                }, {
                    title: 'Telefono', render: (data, type, row) => {
                        let html = validarEspacios(row.Telefono);
                        return html;
                    }
                },


                {
                    title: 'Acciones', with: '20%', render: function (data, type, row) {
                        return `<button class='btn-editar btn btn-primary editarAsociados' data-id='${row.IDEmpresa}'>` +
                            `<i class='fas fa-pencil-alt' style='font-size:18px'></i>` +
                            `</button>&nbsp;<button class='btn-eliminar btn btn-danger eliminarAsociados' data-id='${row.IDEmpresa}'>` +
                            `<i class='fas fa-toggle-on' style='font-size:18px'></i></button>`;
                    }
                }
            ],
            columnDefs: [
                { className: 'dt-center', 'targets': '_all' },
                { 'width': '20%', 'targets': [0, 1, 2] }
            ],
            initComplete: function (settings, json) {

                tblAsociados.on('click', '.editarAsociados', function () {
                    const rowData = dtAsociados.row($(this).closest('tr')).data();
                    $('#mdlAsociadosAgrEditar').modal('show');
                    inpIdAsociadoAccion.val('M')
                    inpIdAsociado.val(rowData.IDAsociado);
                    inpIdContacto.val(rowData.IDContacto);
                    txtNombreAsociado.val(rowData.Nombre);
                    txtTelefonoAsociado.val(rowData.Telefono);
                    cmbRol.val(rowData.IdRol);
                });
                tblAsociados.on('click', '.eliminarAsociados', function () {
                    const rowData = dtAsociados.row($(this).closest('tr')).data();
                    inpIdContacto.val(rowData.IDContacto);
                    inpIdAsociadoAccion.val('B');
                    $('#mdlEliminarContacto').modal('show');

                });

            }
        });

        $('#tblContactosAsosiados_length').css('margin-bottom', '20px');

        $('#tblContactosAsosiados_length').find('select').css('border', '1px solid #e5eaed');
        $('#tblContactosAsosiados_length').find('select').css('border-radius', '.5rem');
        $('#tblContactosAsosiados_length').find('select').css('padding', '.75rem .75rem');
        $('#tblContactosAsosiados_length').find('select').css('color', '#5e6278');
        $('#tblContactosAsosiados_length').find('select').css('margin-left', '10px');
        $('#tblContactosAsosiados_length').find('select').css('margin-right', '10px');
        $('#tblContactosAsosiados_length').find('select').css('outline', 'none');
        $('#tblContactosAsosiados_length').find('label').css('color', '#05a692');
        $('#tblContactosAsosiados_length').find('label').css('font-weight', '600');
        $('#tblContactosAsosiados_length').find('label').css('font-size', '15px');
        $('#tblContactosAsosiados_paginate').css('text-align', 'right');
        $('#tblContactosAsosiados_paginate').css('min-height', '30px');


    }



    const postAgregarModificarEliminarAsociados = function (Accion) {
        let parametros = {
            Accion: inpIdAsociadoAccion.val(),
            IDAsociado: inpIdAsociado.val(),
            IDContacto: inpIdContacto.val(),
            Nombre: txtNombreAsociado.val(),
            Telefono: txtTelefonoAsociado.val(),
            IdRol: cmbRol.val(),
         
        }
        const options = url + 'postAgregarModificarEliminarAsociados';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                postObtenerAsociados();
                $('#mdlAsociadosAgrEditar').modal('hide')
                $('#mdlEliminarContacto').modal('hide');

            } else {
                funesperar(4000, result.ITEMS);

            }


        }).catch(function (error) {
            console.error(error);
            $('#mdlAsociadosAgrEditar').modal('hide')
            $('#mdlEliminarContacto').modal('hide');

        });
    }




    const funesperar = function (timer, texto) {
        let timerInterval
        Swal.fire({
            title: 'Alerta!',
            text: texto,
            icon: 'info',
            timer: timer,
            timerProgressBar: true,
            allowOutsideClick: false,
            didOpen: () => {
                Swal.showLoading()
                const b = Swal.getHtmlContainer().querySelector('b')
                timerInterval = setInterval(() => {
                    //b.textContent = Swal.getTimerLeft()
                }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
            }
        })
    }







    return {
        Inicializar: Inicializar,
    }
};