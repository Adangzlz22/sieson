﻿var BitacoraControllers = function () {
    const url = '/Admin/Admin/'
    const tblBitacora = $('#tblBitacora');
    let dtBitacora;

   

    var Inicializar = function () {

        functionListar();
    }
    const functionListar = function () {
        initBitacora();
        obtener();
        
    }
    var initBitacora = function () {
        dtBitacora = tblBitacora.DataTable({
            language: 'dtDicEsp',
            ordering: false,
            paging: true,
            searching: true,
            bFilter: false,
            info: false,
            columns: [
                { data: 'IDSecuencia', title: 'IDSecuencia' ,visible:false},
                {
                    title: 'Fecha', render: (data, type, row) => {
                        let html = moment(row.Fecha).format("YYYY-MM-DD HH:mm:ss");
                        return html;
                    }
                },
                { data: 'Tipo', title: 'Tipo' },
                { data: 'Usuario', title: 'Usuario' },
                { data: 'Estacion', title: 'Estacion' },
                { data: 'Descripcion', title: 'Descripcion' }
            ],
            columnDefs: [
                { className: 'dt-center', 'targets': '_all' },
                { 'width': '15%', 'targets': [0,1, 2, 3] },
                { 'width': '13%', 'targets': [4] }
            ],
            initComplete: function (settings, json) {
           
            }
        });
        $('#tblBitacora_length').css('margin-bottom', '5px');
        //$('#tblBitacora_length').css('margin-left', '30px');
        $('#tblBitacora_length').find('select').css('border', '1px solid #e5eaed');
        $('#tblBitacora_length').find('select').css('border-radius', '.5rem');
        $('#tblBitacora_length').find('select').css('padding', '.75rem .75rem');
        $('#tblBitacora_length').find('select').css('color', '#5e6278');
        $('#tblBitacora_length').find('select').css('margin-left', '10px');
        $('#tblBitacora_length').find('select').css('margin-right', '10px');
        $('#tblBitacora_length').find('select').css('outline', 'none');
        $('#tblBitacora_length').find('label').css('color', '#05a692');
        $('#tblBitacora_length').find('label').css('font-weight', '600');
        $('#tblBitacora_length').find('label').css('font-size', '15px');
        $('#tblBitacora_paginate').css('text-align', 'right');
        $('#tblBitacora_paginate').css('min-height', '30px');

        $('#tblBitacora_filter').find('label').css('color', '#05a692');
        $('#tblBitacora_filter').find('label').css('font-weight', '900');
        $('#tblBitacora_filter').find('label').css('font-size', '20px');
        $('#tblBitacora_filter').find('label').css('order', '2');
        $('#tblBitacora_filter').find('input').css('margin-left', '5px');
        $('#tblBitacora_filter').find('input').css('border', '1px solid #e5eaed');
        $('#tblBitacora_filter').find('input').css('border-radius', '.5rem');
        $('#tblBitacora_filter').find('input').css('padding', '.5rem .75rem');
        $('#tblBitacora_filter').find('input').css('outline', 'none');
        $('#tblBitacora_filter').find('input').css('color', '#5e6278');
        $('#tblBitacora_filter').find('input').css('font-size', '15px');
        $('#tblBitacora_filter').css('text-align', 'right');

    }
    var validarEspacios = function (str) {
        if (str == null) {
            str = '';
        }
        return str;
    }
    const obtener = function () {
        funesperar(0);

        let parametros = {
        }
        const options = url + 'GetListadoBitacora';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            console.log(result);
            if (result.SUCCESS == true) {
                dtBitacora.clear();
                dtBitacora.rows.add(result.ITEMS);
                dtBitacora.draw();
            funesperar(1);
        } else {
            }
        }).catch(function (error) {
            console.error(error);
        });
    }

    const funesperar = function (timer) {
        let timerInterval
        Swal.fire({
            title: 'El contenido se está cargando',
            text: 'Esto puede demorar unos segundos.',
            icon: 'info',
            timer: timer,
            timerProgressBar: true,
            allowOutsideClick: false,
            didOpen: () => {
                Swal.showLoading()
                const b = Swal.getHtmlContainer().querySelector('b')
                timerInterval = setInterval(() => {
                    //b.textContent = Swal.getTimerLeft()
                }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
            }
        })
    }


    return {
        Inicializar: Inicializar,
    }
};
