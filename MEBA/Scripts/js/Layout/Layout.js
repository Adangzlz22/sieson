﻿var LayoutControllers = function () {
    const url = '/Loyout/'
    const url2 = '/Login/'

    
    const contenedorDeAlertas = $('#contenedorDeAlertas');
    const lblCantidad = $('#lblCantidad');
    const btnCerrarSession = $('#btnCerrarSession');
    const configuracion = $('#configuracion');
    
    var Inicializar = function () {
        postGenerarAlertas();
        functionListar();
        obtenerMenu();
    }
    const functionListar = function () {
        btnCerrarSession.click(function () {
            obtener();
        });
    }
    const obtener = function () {
        let parametros = {
        
        }
        const options = url2 + 'postCerrarSession';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                window.location.href = "/Login/Login";
            } else {
            }
        }).catch(function (error) {
            console.error(error);
        });
    }
    
    const obtenerMenu = function () {
        let parametros = {

        }
        const options = url + 'obtenerMenu';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            configuracion.append(result);

        }).catch(function (error) {
            console.error(error);
        });
    }
    
    const postGenerarAlertas = function () {
        let parametros = {

        }
        const options = url + 'postGenerarAlertas';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            var html = '';
            if (result.length != 0) {
                console.log(result)
                lblCantidad.text(result.length);

                for (var i = 0; i < result.length; i++) {
                    html += `<div class="dropdown-divider"></div>
                            <a id="btn${result[i].IdAlerta}" class="dropdown-item" title="${result[i].Mensaje}">
                                ${result[i].Mensaje}
                            </a>`;
                  
                }

                contenedorDeAlertas.append(html)

                for (var i = 0; i < result.length; i++) {
                    var variable = result[i].IdAlerta;
                    var urlAccion = result[i].urlAccion;
                    
                    $(`#btn${result[i].IdAlerta}`).click(function () {
                        btnEjecutarDesactivarAlerta(variable, urlAccion);
                    })
                }
               
            } else {
                lblCantidad.text(0);

            }

        }).catch(function (error) {
            console.error(error);
        });
    }
    const btnEjecutarDesactivarAlerta = function (IdAlerta,urlAccion) {
        let parametros = {
            IdAlerta: IdAlerta
        }
        const options = url + 'btnEjecutarDesactivarAlerta';
        axios.post(options, parametros).then(function (response) {
            location.href = urlAccion;

        }).catch(function (error) {
            console.error(error);
        });
    }

    return {
        Inicializar: Inicializar,
    }
};