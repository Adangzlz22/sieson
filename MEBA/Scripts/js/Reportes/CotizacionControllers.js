﻿var ReportesCrysControllers = function () {
    const url = '/ReportesCrys/';
    const urlReportes = '/Reportes/';
    const urlHome = '/Home/';
    const tblCotizacion = $('#tblCotizacion');
    let dtCotizacion;

    const btnBuscar = $('#btnBuscar');
    const btnAgregar = $('#btnAgregar');
    const btnCancelar = $('#btnCancelar');

    const txtFOLIO = $('#txtFOLIO');
    const txtATENCION = $('#txtATENCION');
    const txtTRAMO = $('#txtTRAMO');
    const txtNombre = $('#txtNombre');
    const txtRFC = $('#txtRFC');
    const txtDIRECCIÓN = $('#txtDIRECCIÓN');
    const txtNUM = $('#txtNUM');
    const txtCP = $('#txtCP');
    const txtCIUDAD = $('#txtCIUDAD');
    const txtESTADO = $('#txtESTADO');
    const txtPAIS = $('#txtPAIS');
    const btnGuardar = $('#btnGuardar');

    const tblProductoCot = $('#tblProductoCot');
    let dtProductoCot;
    const spanSec = $('#spanSec');
    const seccionDatoEmpresa = $('#seccionDatoEmpresa');
    const btnAgregarProducto = $('#btnAgregarProducto');
    const cmbProducto = $('#cmbProducto');
    const txtCANTIDAD = $('#txtCANTIDAD');
    const txtPRECIO = $('#txtPRECIO');
    const txtId = $('#txtId');


    const cmbMedida = $('#cmbMedida');
    const txtIdCotizacionAdd = $('#txtIdCotizacionAdd');
    const btnGuardarProd = $('#btnGuardarProd');
    const txtAccion = $('#txtAccion');
    const dtFecha = $('#dtFecha');

    const mdlEliminarCot = $('#mdlEliminarCot');
    const btnCancelarCot = $('#btnCancelarCot');
    const btnEliminarCot = $('#btnEliminarCot');
    const mdlEliminarProd = $('#mdlEliminarProd');
    const btnCancelarProd = $('#btnCancelarProd');
    const btnEliminarProd = $('#btnEliminarProd');
    const btnCancelarEliProd = $('#btnCancelarEliProd');

    const txtBorrarProd = $('#txtBorrarProd');
    const txtBorrarCot = $('#txtBorrarCot');


    const txtIdUsuarioEliminarCot = $('#txtIdUsuarioEliminarCot');
    const txtIdUsuarioEliminarProd = $('#txtIdUsuarioEliminarProd');


    const cmbReflejante = $('#cmbReflejante');


    const btnBuscarCliente = $('#btnBuscarCliente');
    const btnObtenerCliente = $('#btnObtenerCliente');
    const tablaCliente = $('#tablaCliente');
    let dtCliente;


    const txtdestinatario = $('#txtdestinatario');
    const txtdestinatarioAdjunto = $('#txtdestinatarioAdjunto');
    const txtIdCoCorreo = $('#txtIdCoCorreo');
    const btnEnviarCorreo = $('#btnEnviarCorreo');
    const btnCancelarCorreo = $('#btnCancelarCorreo');


    var Inicializar = function () {
        //postGenerarReporteExcel();
        console.log('Cotizacion');

        functionListar();
        spanSec.click(function () {
            var div = document.getElementById("seccionDatoEmpresa");
            if (div.style.display === "none") {
                div.style.display = "block";
            } else {
                div.style.display = "none";
            }
        })
    }
    const functionListar = function () {
        initCotizacion();
        initProductoCot();
        initCliente();
        postCmbObtenerProductos();
        postSpdObtenerListadoCotizacion();
        btnEnviarCorreo.click(function () {
            postEnviarCorreo(txtIdCoCorreo.val());
        });
        btnCancelarCorreo.click(function () {
            $('#mdlCorreo').modal('hide');
        });
        btnGuardar.click(function () {
            txtAccion.val('C');
            postSpdObtenerListadoCotizacion();
            postSpdAgregarModificarEliminar();
            $('#myModal').modal('hide');
        });
        btnBuscar.click(function () {
            postSpdObtenerListadoCotizacion();
        });
        btnAgregar.click(function () {
            postExtraerUltimoFolio();
            txtATENCION.val('');
            txtTRAMO.val('');
            txtNombre.val('');
            txtRFC.val('');
            txtDIRECCIÓN.val('');
            txtNUM.val('');
            txtCP.val('');
            txtCIUDAD.val('');
            txtESTADO.val('');
            txtPAIS.val('');
            $('#myModal').modal('show');
            $('.btn-close').css('background-color', '#FFF');
            dtProductoCot.clear();
            dtProductoCot.rows.add([]);
            dtProductoCot.draw();

        });
        btnCancelar.click(function () {
            $('#myModal').modal('hide');
            $('.btn-close').css('background-color', '#FFF');
        });
        btnAgregarProducto.click(function () {
            txtId.val(0);
            txtIdCotizacionAdd.val(0);
            cmbProducto.val(0);
            txtCANTIDAD.val('');
            txtPRECIO.val(0);
            txtAccion.val('A');
            cmbMedida.val('');
            cmbProducto.val('');
            cmbProducto.trigger('change');
            cmbReflejante.val('');
            cmbReflejante.trigger('change');
            $('#myModalProd').modal('show');
        });
        btnCancelarProd.click(function () {
            txtId.val(0);
            cmbProducto.val(0);
            txtCANTIDAD.val('');
            txtPRECIO.val(0);
            $('#myModalProd').modal('hide');
        });
        btnGuardarProd.click(function () {
            postSpdAgregarModificarEliminar();
        });

        btnEliminarProd.click(function () {
            postbtnEliminarProd();
        });
        btnEliminarCot.click(function () {
            postbtnEliminarCot();
        });
        btnCancelarEliProd.click(function () {
            $('#mdlEliminarProd').modal('hide');
        });
        btnCancelarCot.click(function () {
            $('#mdlEliminarCot').modal('hide');
        });
        btnBuscarCliente.click(function () {
            $('#mdlObtenerCliente').modal('show');
            spdBuscarCliente();
        });
        btnObtenerCliente.click(function () {
            $('#mdlObtenerCliente').modal('hide');
        });
    }
    var initCotizacion = function () {
        dtCotizacion = tblCotizacion.DataTable({
            language: 'dtDicEsp',
            ordering: true,
            paging: true,
            searching: true,
            bFilter: false,
            info: false,
            order: [[1, "desc"]] ,
            columns: [
                { data: 'IdCotizacion', title: 'id', visible: false },
                {
                    title: 'Folio', render: (data, type, row) => {
                        let html = validarEspacios(row.Folio);
                        if (!row.Activo) {
                            html += '/Cancelada';
                        }
                        return html;
                    }
                },
                {
                    title: 'NombreCliente', render: (data, type, row) => {
                        let html = validarEspacios(row.NombreCliente);
                        return html;
                    }
                },
                {
                    title: 'RFCCliente', render: (data, type, row) => {
                        let html = validarEspacios(row.RFCCliente);
                        return html;
                    }
                },
                {
                    title: 'FechaCreacion', render: (data, type, row) => {
                        let html = moment(row.Fecha).format("DD-MM-YYYY");
                        return html;
                    }
                },
                {
                    title: 'Acciones', render: function (data, type, row) {
                        let btnEliminar = '';


                        btnEliminar += `<button class='btn-editar btn btn-success EnviarCorreo' data-id='${row.IdCotización}' style="background: #0061f2 !important;">` +
                            `<i class='fas fa-email' style='font-size:18px'></i>` +
                            `</button>`;
                        btnEliminar += `<button class='btn-editar btn btn-success ImprimirOrdenDeTrabajo' data-id='${row.IdCotización}' style="background: #f76400 !important;">` +
                            `<i class='fas fa-print' style='font-size:18px'></i>` +
                            `</button>`;

                        btnEliminar += `<button class='btn-editar btn btn-primary editarCotizacion' data-id='${row.IdCotización}'>` +
                            `<i class='fas fa-pencil-alt' style='font-size:18px'></i>` +
                            `</button>`;
                        btnEliminar += `<button class='btn-editar btn btn-success ImprimirCotizacion' data-id='${row.IdCotización}'>` +
                            `<i class='fas fa-print' style='font-size:18px'></i>` +
                            `</button>`;
                        if (row.Activo) {

                            btnEliminar += `<button class='btn-eliminar btn btn-danger eliminarCotizacion' data-id='${row.IdCotización}'>` +
                                `<i class='fas fa-trash' style='font-size:18px'></i></button>`;
                        }

                        return btnEliminar;

                    }
                }
            ],
            columnDefs: [
                { className: 'dt-center', 'targets': '_all' },
                { 'width': '20%', 'targets': [1, 2, 3, 4] },
            ],
            initComplete: function (settings, json) {


                tblCotizacion.on('click', '.eliminarCotizacion', function () {
                    const rowData = dtCotizacion.row($(this).closest('tr')).data();

                    str = " ¿Esta seguro de querer eliminar este Cotización?"
                    $('#mdlEliminarCot').modal('show');
                    txtBorrarCot.text(str);
                    txtIdUsuarioEliminarCot.val(rowData.IdCotizacion);

                });
                tblCotizacion.on('click', '.editarCotizacion', function (e) {
                    const rowData = dtCotizacion.row($(this).closest('tr')).data();
                    postSpdObtenerListadoCotizacionxIdCotizacion(rowData.IdCotizacion);
                    $('#myModal').modal('show');
                    $('.btn-close').css('background-color', '#FFF');

                });

                tblCotizacion.on('click', '.ImprimirCotizacion', function (e) {
                    const rowData = dtCotizacion.row($(this).closest('tr')).data();
                    postGenerarReporteExcel(rowData.IdCotizacion);
                });
                tblCotizacion.on('click', '.ImprimirOrdenDeTrabajo', function (e) {
                    const rowData = dtCotizacion.row($(this).closest('tr')).data();
                    postGenerarReporteImprimirOrdenDeTrabajo(rowData.IdCotizacion);
                });
                tblCotizacion.on('click', '.EnviarCorreo', function (e) {
                    const rowData = dtCotizacion.row($(this).closest('tr')).data();
                    $('#mdlCorreo').modal('show');
                    txtIdCoCorreo.val(rowData.IdCotizacion);
                    txtdestinatario.val('');
                    txtdestinatarioAdjunto.val('');
                });

            }
        });

        $('#tblCotizacion_length').css('margin-bottom', '5px');

        //$('#tblCotizacion_length').css('margin-left', '30px');
        $('#tblCotizacion_length').find('select').css('border', '1px solid #e5eaed');
        $('#tblCotizacion_length').find('select').css('border-radius', '.5rem');
        $('#tblCotizacion_length').find('select').css('padding', '.5rem .75rem');
        $('#tblCotizacion_length').find('select').css('color', '#5e6278');
        $('#tblCotizacion_length').find('select').css('margin-left', '10px');
        $('#tblCotizacion_length').find('select').css('margin-right', '10px');
        $('#tblCotizacion_length').find('select').css('outline', 'none');
        //$('#tblCotizacion_length').find('label').css('color', '#05a692');
        //$('#tblCotizacion_length').find('label').css('font-weight', '600');
        //$('#tblCotizacion_length').find('label').css('font-size', '15px');
        $('#tblCotizacion_paginate').css('text-align', 'right');
        $('#tblCotizacion_paginate').css('min-height', '30px');

        //$('#tblCotizacion_filter').find('label').css('color', '#05a692');
        //$('#tblCotizacion_filter').find('label').css('font-weight', '900');
        //$('#tblCotizacion_filter').find('label').css('font-size', '20px');
        $('#tblCotizacion_filter').find('label').css('order', '2');
        $('#tblCotizacion_filter').find('input').css('margin-left', '5px');
        $('#tblCotizacion_filter').find('input').css('border', '1px solid #e5eaed');
        $('#tblCotizacion_filter').find('input').css('border-radius', '.5rem');
        $('#tblCotizacion_filter').find('input').css('padding', '.5rem .75rem');
        $('#tblCotizacion_filter').find('input').css('outline', 'none');
        $('#tblCotizacion_filter').find('input').css('color', '#5e6278');
        $('#tblCotizacion_filter').find('input').css('font-size', '15px');
        $('#tblCotizacion_filter').css('text-align', 'left');


    }
    var initProductoCot = function () {
        dtProductoCot = tblProductoCot.DataTable({
            language: 'dtDicEsp',
            ordering: true,
            paging: true,
            searching: true,
            bFilter: false,
            info: false,
            columns: [
                { data: 'id', title: 'id', visible: false },
                {
                    title: 'Producto', render: (data, type, row) => {
                        let html = validarEspacios(row.NombreProducto);
                        return html;
                    }
                },
                {
                    title: 'Medida', render: (data, type, row) => {
                        let html = row.Medida;
                        return html;
                    }
                },
                {
                    title: 'Cantidad', render: (data, type, row) => {
                        let html = row.Cantidad;
                        return html;
                    }
                },
                {
                    title: 'Precio', render: (data, type, row) => {
                        let html = row.PrecioUnitario;
                        return html;
                    }
                },
                {
                    title: 'Total', render: (data, type, row) => {
                        let html = row.PrecioVenta;
                        return html;
                    }
                },
                {
                    title: 'Acciones', render: function (data, type, row) {
                        let btnEliminar = '';



                        btnEliminar = `<button class='btn-eliminar btn btn-danger eliminarProductoCot' data-id='${row.id}'>` +
                            `<i class='fas fa-trash' style='font-size:18px'></i></button>`;

                        btnEliminar += `<button class='btn-editar btn btn-primary editarProductoCot' data-id='${row.id}'>` +
                            `<i class='fas fa-pencil-alt' style='font-size:18px'></i>` +
                            `</button>`;


                        return btnEliminar;

                    }
                }
            ],
            columnDefs: [
                { className: 'dt-center', 'targets': '_all' },
                { 'width': '25%', 'targets': [1, 2, 3] },
            ],
            initComplete: function (settings, json) {


                tblProductoCot.on('click', '.eliminarProductoCot', function () {
                    const rowData = dtProductoCot.row($(this).closest('tr')).data();
                    var str = "";
                    str = " ¿Esta seguro de querer eliminar este Cotización?"
                    $('#mdlEliminarProd').modal('show');
                    txtBorrarProd.text(str);
                    txtIdUsuarioEliminarProd.val(rowData.id);


                });
                tblProductoCot.on('click', '.editarProductoCot', function (e) {
                    const rowData = dtProductoCot.row($(this).closest('tr')).data();
                    txtAccion.val('B');
                    console.log(rowData.id)
                    txtIdCotizacionAdd.val(rowData.id)
                    cmbProducto.val(rowData.ProductoId);
                    cmbProducto.trigger('change');
                    txtCANTIDAD.val(rowData.Cantidad);
                    txtPRECIO.val(rowData.PrecioUnitario);
                    cmbMedida.val(rowData.Medida);
                    cmbMedida.trigger('change');
                    cmbReflejante.val(rowData.Reflejante);
                    cmbReflejante.trigger('change');
                    $('#myModalProd').modal('show');
                });


            }
        });

        $('#tblProductoCot_length').css('margin-bottom', '5px');

        //$('#tblProductoCot_length').css('margin-left', '30px');
        $('#tblProductoCot_length').find('select').css('border', '1px solid #e5eaed');
        $('#tblProductoCot_length').find('select').css('border-radius', '.5rem');
        $('#tblProductoCot_length').find('select').css('padding', '.5rem .75rem');
        $('#tblProductoCot_length').find('select').css('color', '#5e6278');
        $('#tblProductoCot_length').find('select').css('margin-left', '10px');
        $('#tblProductoCot_length').find('select').css('margin-right', '10px');
        $('#tblProductoCot_length').find('select').css('outline', 'none');
        //$('#tblProductoCot_length').find('label').css('color', '#05a692');
        //$('#tblProductoCot_length').find('label').css('font-weight', '600');
        //$('#tblProductoCot_length').find('label').css('font-size', '15px');
        $('#tblProductoCot_paginate').css('text-align', 'right');
        $('#tblProductoCot_paginate').css('min-height', '30px');

        //$('#tblProductoCot_filter').find('label').css('color', '#05a692');
        //$('#tblProductoCot_filter').find('label').css('font-weight', '900');
        //$('#tblProductoCot_filter').find('label').css('font-size', '20px');
        $('#tblProductoCot_filter').find('label').css('order', '2');
        $('#tblProductoCot_filter').find('input').css('margin-left', '5px');
        $('#tblProductoCot_filter').find('input').css('border', '1px solid #e5eaed');
        $('#tblProductoCot_filter').find('input').css('border-radius', '.5rem');
        $('#tblProductoCot_filter').find('input').css('padding', '.5rem .75rem');
        $('#tblProductoCot_filter').find('input').css('outline', 'none');
        $('#tblProductoCot_filter').find('input').css('color', '#5e6278');
        $('#tblProductoCot_filter').find('input').css('font-size', '15px');
        $('#tblProductoCot_filter').css('text-align', 'left');


    }

    var validarEspacios = function (str) {
        if (str == null) {
            str = '';
        }
        return str;
    }
    const postSpdObtenerListadoCotizacion = function () {
        let parametros = {
        }
        const options = urlReportes + 'postSpdObtenerListadoCotizacion';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            console.log(result.ITEMS)
            dtCotizacion.clear();
            dtCotizacion.rows.add(result.ITEMS);
            dtCotizacion.draw();
        }).catch(function (error) {
            console.error(error);
        });
    }


    const postSpdObtenerListadoCotizacionxIdCotizacion = function (IdCotizacion) {
        let parametros = {
            IdCotizacion: IdCotizacion
        }
        const options = urlReportes + 'postSpdObtenerListadoCotizacionxIdCotizacion';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            console.log(result.ITEMS)
            txtFOLIO.prop('disabled', true)
            txtFOLIO.val(result.ITEMS[0]["Folio"].split('/')[1]);
            txtATENCION.val(result.ITEMS[0]["Atencion"]);
            txtTRAMO.val(result.ITEMS[0]["Tramo"]);
            txtNombre.val(result.ITEMS[0]["NombreCliente"]);
            txtRFC.val(result.ITEMS[0]["RFCCliente"]);
            txtDIRECCIÓN.val(result.ITEMS[0]["Direccion"]);
            txtNUM.val(result.ITEMS[0]["NumINT"]);
            txtCP.val(result.ITEMS[0]["CP"]);
            txtCIUDAD.val(result.ITEMS[0]["Ciudad"]);
            txtESTADO.val(result.ITEMS[0]["Estado"]);
            txtPAIS.val(result.ITEMS[0]["Pais"]);
            dtFecha.val(moment(result.ITEMS[0]["FechaCreacion"]).format("YYYY-MM-DD"))

            dtProductoCot.clear();
            dtProductoCot.rows.add(result.ITEMS);
            dtProductoCot.draw();

        }).catch(function (error) {
            console.error(error);
        });
    }

    const postExtraerUltimoFolio = function (IdCotizacion) {
        let parametros = {
            IdCotizacion: IdCotizacion
        }
        const options = urlReportes + 'postExtraerUltimoFolio';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            console.log(result.ITEMS)
            txtFOLIO.prop('disabled', true)
            txtFOLIO.val(result.ITEMS);


        }).catch(function (error) {
            console.error(error);
        });
    }


    const postGenerarReporteExcel = function (IdCotizacion) {
        funesperar(0, 'Porfavor espere unos segundos.');
        let parametros = {
            "IdReporte": 1,
            "IdCotizacion": IdCotizacion
        }

        const options = url + 'postObtenerReporte';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            //dtReportes.clear();
            //dtReportes.rows.add(result.data);
            //dtReportes.draw();
            console.log(result.ITEMS)
            downloadPDF(result.ITEMS);
            funesperar(1, '');

        }).catch(function (error) {
            console.error(error);
        });
    }


    const downloadPDF = (pdf) => {
        const linkSource = `data:application/pdf;base64,${pdf}`;
        const downloadLink = document.createElement("a");
        downloadLink.href = linkSource;
        downloadLink.target = "_blank";
        downloadLink.download = "Reporte.pdf";
        document.body.appendChild(downloadLink)
        downloadLink.click()
        document.body.removeChild(downloadLink)
        funesperar(1);
    };
    const funesperar = function (timer) {
        let timerInterval
        Swal.fire({
            title: 'El contenido se está cargando',
            text: 'Esto puede demorar unos segundos.',
            icon: 'info',
            timer: timer,
            timerProgressBar: true,
            allowOutsideClick: false,
            didOpen: () => {
                Swal.showLoading()
                const b = Swal.getHtmlContainer().querySelector('b')
                timerInterval = setInterval(() => {
                    //b.textContent = Swal.getTimerLeft()
                }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
            }
        })
    }
    const postCmbObtenerProductos = function () {
        let parametros = {
        }
        const options = urlReportes + 'postCmbObtenerProductos';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                let html = "";
                result.ITEMS.forEach(x => {
                    html += `<option value="${x.value}">${x.text}</option>`;
                });
                cmbProducto.append(html);
                cmbProducto.select2({
                    width: '100%',
                    dropdownParent: cmbProducto.parent(),
                    placeholder: 'Selecciona una opción',
                    allowClear: true,
                    minimumResultsForSearch: 1
                });
                cmbProducto.css('width', '100% !important')
                $('.select2').css('width', '100% !important')



            } else {
            }
        }).catch(function (error) {
            console.error(error);
        });
    }

    const postSpdAgregarModificarEliminar = function () {

        if (txtNombre.val() == "") {
            mostrarMensajePro('Nombre.')
            return;
        }

        if (dtFecha.val() == "") {
            mostrarMensajePro('Fecha.')
            return;
        }

        let parametros = {
            Opcion: txtAccion.val(),
            id: txtIdCotizacionAdd.val(),
            IdCotizacion: txtFOLIO.val(),
            FechaCreacion: dtFecha.val(),
            ProductoId: cmbProducto.val(),
            PrecioUnitario: txtPRECIO.val(),
            Cantidad: txtCANTIDAD.val(),
            Folio: txtFOLIO.val(),
            Tramo: txtTRAMO.val(),
            Atencion: txtATENCION.val(),
            NombreCliente: txtNombre.val(),
            RFCCliente: txtRFC.val(),
            Direccion: txtDIRECCIÓN.val(),
            NumINT: txtNUM.val(),
            CP: txtCP.val(),
            Ciudad: txtCIUDAD.val(),
            Estado: txtESTADO.val(),
            Pais: txtPAIS.val(),
            Medida: cmbMedida.val(),
            Reflejante: cmbReflejante.val(),
        }
        const options = urlReportes + 'postSpdAgregarModificarEliminar';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            console.log(result)
            $('#myModalProd').modal('hide')
            postSpdObtenerListadoCotizacion();
            postSpdObtenerListadoCotizacionxIdCotizacion(result.ITEMS.IdCotizacion);
            funesperarExito(2000, result.MENSAJE)
        }).catch(function (error) {
            console.error(error);
        });
    }
    const funesperarExito = function (timer, texto) {
        let timerInterval
        Swal.fire({
            title: 'Mensaje!',
            text: texto,
            icon: 'success',
            timer: timer,
            timerProgressBar: true,
            allowOutsideClick: false,
            didOpen: () => {
                Swal.showLoading()
                const b = Swal.getHtmlContainer().querySelector('b')
                $('#mdlMenu').modal('hide');
                timerInterval = setInterval(() => {
                    //b.textContent = Swal.getTimerLeft()
                }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
            }
        })
    }

    const postbtnEliminarCot = function () {
        let parametros = {
            IdCotizacion: txtIdUsuarioEliminarCot.val()
        }
        const options = urlReportes + 'postSpdEliminarTodaLaCotizacion';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            console.log(result.ITEMS)
            txtFOLIO.prop('disabled', true)
            txtFOLIO.val(result.ITEMS);
            $('#mdlEliminarCot').modal('hide')
            postSpdObtenerListadoCotizacion();

        }).catch(function (error) {
            console.error(error);
        });
    }
    const postbtnEliminarProd = function () {
        let parametros = {
            id: txtIdUsuarioEliminarProd.val()
        }
        const options = urlReportes + 'postSpdEliminarSeleccionCotizacion';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            console.log(result.ITEMS)
            txtFOLIO.prop('disabled', true)
            txtFOLIO.val(result.ITEMS);
            $('#mdlEliminarProd').modal('hide')
            postSpdObtenerListadoCotizacionxIdCotizacion(result.ITEMS.IdCotizacion);

        }).catch(function (error) {
            console.error(error);
        });
    }

    const spdBuscarCliente = function () {

        let parametros = {
        }
        const options = urlReportes + 'postSpdBuscarCliente';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            console.log(result.ITEMS)
            dtCliente.clear();
            dtCliente.rows.add(result.ITEMS);
            dtCliente.draw();
        }).catch(function (error) {
            console.error(error);
        });

    }

    var initCliente = function () {
        dtCliente = tablaCliente.DataTable({
            language: 'dtDicEsp',
            ordering: true,
            paging: true,
            searching: true,
            bFilter: false,
            info: false,
            columns: [
                { data: 'RowId', title: 'id', visible: false },
                {
                    title: 'NombreCliente', render: (data, type, row) => {
                        let html = validarEspacios(row.NombreCliente);
                        return html;
                    }
                },
                {
                    title: 'Acciones', render: function (data, type, row) {
                        let btnEliminar = '';



                        btnEliminar = `<button class='btn-eliminar btn btn-dark SelectCliente' data-id='${row.id}'>` +
                            `Seleccionar Cliente</button>`;


                        return btnEliminar;

                    }
                }
            ],
            columnDefs: [
                { className: 'dt-center', 'targets': '_all' },
                { 'width': '25%', 'targets': [1] },
            ],
            initComplete: function (settings, json) {


                tablaCliente.on('click', '.SelectCliente', function () {
                    const rowData = dtCliente.row($(this).closest('tr')).data();
                    txtNombre.val(rowData.NombreCliente);
                    txtRFC.val(rowData.RFCCliente);
                    txtDIRECCIÓN.val(rowData.Direccion);
                    txtNUM.val(rowData.NumINT);
                    txtCP.val(rowData.CP);
                    txtCIUDAD.val(rowData.Ciudad);
                    txtESTADO.val(rowData.Estado);
                    txtPAIS.val(rowData.Pais);
                    $('#mdlObtenerCliente').modal('hide')


                });



            }
        });

        $('#tablaCliente_length').css('margin-bottom', '5px');

        //$('#tablaCliente_length').css('margin-left', '30px');
        $('#tablaCliente_length').find('select').css('border', '1px solid #e5eaed');
        $('#tablaCliente_length').find('select').css('border-radius', '.5rem');
        $('#tablaCliente_length').find('select').css('padding', '.5rem .75rem');
        $('#tablaCliente_length').find('select').css('color', '#5e6278');
        $('#tablaCliente_length').find('select').css('margin-left', '10px');
        $('#tablaCliente_length').find('select').css('margin-right', '10px');
        $('#tablaCliente_length').find('select').css('outline', 'none');
        //$('#tablaCliente_length').find('label').css('color', '#05a692');
        //$('#tablaCliente_length').find('label').css('font-weight', '600');
        //$('#tablaCliente_length').find('label').css('font-size', '15px');
        $('#tablaCliente_paginate').css('text-align', 'right');
        $('#tablaCliente_paginate').css('min-height', '30px');

        //$('#tablaCliente_filter').find('label').css('color', '#05a692');
        //$('#tablaCliente_filter').find('label').css('font-weight', '900');
        //$('#tablaCliente_filter').find('label').css('font-size', '20px');
        $('#tablaCliente_filter').find('label').css('order', '2');
        $('#tablaCliente_filter').find('input').css('margin-left', '5px');
        $('#tablaCliente_filter').find('input').css('border', '1px solid #e5eaed');
        $('#tablaCliente_filter').find('input').css('border-radius', '.5rem');
        $('#tablaCliente_filter').find('input').css('padding', '.5rem .75rem');
        $('#tablaCliente_filter').find('input').css('outline', 'none');
        $('#tablaCliente_filter').find('input').css('color', '#5e6278');
        $('#tablaCliente_filter').find('input').css('font-size', '15px');
        $('#tablaCliente_filter').css('text-align', 'left');


    }




    const postGenerarReporteImprimirOrdenDeTrabajo = function (IdCotizacion) {
        funesperar(0, 'Porfavor espere unos segundos.');
        let parametros = {
            "IdReporte": 3,
            "IdCotizacion": IdCotizacion
        }

        const options = url + 'postObtenerReporte';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            //dtReportes.clear();
            //dtReportes.rows.add(result.data);
            //dtReportes.draw();
            console.log(result.ITEMS)
            downloadPDF(result.ITEMS);
            funesperar(1, '');

        }).catch(function (error) {
            console.error(error);
        });
    }

    const postEnviarCorreo = function (IdCotizacion) {
        if (txtdestinatario.val() == "") {
            mostrarMensaje('destinatario.')
            return;
        }
        if (txtdestinatarioAdjunto.val() == "") {
            mostrarMensaje('destinatario adjunto.');
            return;
        }
        let parametros = {
            "IdReporte": 4,
            "IdCotizacion": IdCotizacion,
            "destinatario": txtdestinatario.val(),
            "destinatarioAdjunto": txtdestinatarioAdjunto.val(),
        }

        const options = url + 'postObtenerReporte';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            //dtReportes.clear();
            //dtReportes.rows.add(result.data);
            //dtReportes.draw();
            console.log(result.ITEMS)
            Swal.fire({
                title: "Correo",
                text: "El correo se ha enviado porfavor esperar unos minutos y revisar tu bandeja de enviados!",
                icon: "success"
            });
            $('#mdlCorreo').modal('hide');

        }).catch(function (error) {
            console.error(error);
        });
    }
    const mostrarMensajePro = function (destinatario) {
        Swal.fire({
            title: "Producto",
            text: "Campo vacio en " + destinatario,
            icon: "warning"
        });
    }
    const mostrarMensaje = function (destinatario) {
        Swal.fire({
            title: "Correo",
            text: "Campo vacio en " + destinatario,
            icon: "warning"
        });
    }
    return {
        Inicializar: Inicializar,
    }
};