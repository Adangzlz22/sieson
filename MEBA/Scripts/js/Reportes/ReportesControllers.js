﻿var ReportesCrysControllers = function () {
    const url = '/Reportes/';
    const urlHome = '/Home/';
    const btnReporte = $('#btnReporte');
    const btnExcel = $('#btnExcel');
    
    const cboEstaciones = $('#cboEstaciones');
    const cboZonas = $('#cboZonas');
    const dtFechaInicio = $('#dtFechaInicio');
    const dtFechaFin = $('#dtFechaFin');
    const tblreportes = $('#tblreportes');
    let dtReportes;
    let RevisarZonas;

    var Inicializar = function () {
        cboZona();

        initReportes();
        let date = new Date()
        let day = date.getDate()
        let month = date.getMonth() + 1
        let year = date.getFullYear()
        let fecha1="";
        if(month < 10){
        fecha1=`${year}-0${month}-${day}`;
        }else{
        fecha1=`${year}-${month}-${day}`;
        }  
        dtFechaInicio.val(fecha1);
        dtFechaFin.val(fecha1);
        //obtenerCombo();
        btnReporte.click(function () {
            let Arr = fncFiltritos(cboEstaciones.val())
            obtener(Arr);
        });
        btnExcel.click(function () {
            let Arr = fncFiltritos(cboEstaciones.val())
            obtenerExcel(Arr);
        });
    }

    var AplicarMetodosDeEstaciones = function () {
        postExtraerEstacionesXZonas();
        cboZonas.change(function () {
            postExtraerEstacionesXZonas();
        });
    }

    var fncFiltritos = function (valores) {
        let selCC;
        let arreglo = []

        for (let i = 0; i < valores.length; i++) {
            selCC = $('#cboEstaciones').find("option[value=" + valores[i] + "]").text();

            const element = {
                stationId : valores[i],
                commercialName : selCC,
            };
            arreglo.push(element);
        }

        return arreglo;
    }
    const obtener = function (lstDatos) {
        let parametros = {
        }
        const options = urlHome + 'ObtenerConfig';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                if (result.ITEMS.SitioWEB != 'local') {
                    postGenerarMebaRpt(lstDatos,result.ITEMS);
                } else {
                    local();
                }
            } else {
            }
        }).catch(function (error) {
            console.error(error);
        });
    }
    const postGenerarMebaRpt = function (lstDatos, ITEMS) {
        let parametros = {
        }
        const options = url + 'postGenerarMebaRpt';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
                if (ITEMS.SitioWEB != 'local') {
                    GenerarUrlMedianteApi(lstDatos, ITEMS);
                } else {
                    local();
                }
        }).catch(function (error) {
            console.error(error);
        });
    }
    const GenerarUrlMedianteApi = function (lstDatos, objUrl) {
        let parametros = {
            Usuario: objUrl.usuarioApi,
            Password: objUrl.passwordApi,
            Url: objUrl.urlApi,
            EndPoint: 'GetToken'
        }
        const options = urlHome + 'LoginApiSistema';
        axios.post(options, parametros).then(function (response) {
            const result = JSON.parse(response.data);
            //if (lstDatos.length != 0) {
            if (cboEstaciones.val() != '') {
                //    lstDatos.forEach(x => {
                postGenerarReporte(objUrl, result.Data, cboEstaciones.find("option[value=" + cboEstaciones.val() + "]").text())
                //});
            }else{
                alert('seleccione porfavor una estacion')
            }
            window.open('/Reportes/WebRptVentasVsInventario.aspx', '_blank');

        }).catch(function (error) {
            console.error(error);
        });
    }
    const obtenerTodosLosdelCB = function () {
        let lstArreglo = [];
        let cbo = $('#cboEstaciones').find('option');
        for (let i = 0; i < cbo.length; i++) {
            let id = $('#cboEstaciones').find("option[value=" + i + "]").val();
            let selCC = $('#cboEstaciones').find("option[value=" + i + "]").text();

            if (selCC != "") {
                if (selCC.split('-')[0] != "-1") {
                    const element = {
                        StationId: id,
                        Nombre: selCC,
                    };
            lstArreglo.push(element);
                }
            }
        }
        console.log(lstArreglo)
        return lstArreglo;
    }
    const obtenerTodosLosdelCBClass = function () {
        let lstArreglo = [];
        let cbo = $('#cboEstaciones').find('option');
        for (let i = 0; i < cbo.length; i++) {
            let selCC = $('#cboEstaciones').find("option[value=" + i + "]").val();
            if (selCC != undefined) {
                if (selCC != "NaN") {
                    if (selCC != "") {
                        if (selCC != "-1") {
                            const element = parseInt(selCC);
                            lstArreglo.push(element);
                        }
                    }
                }
            }
        
        }
        console.log(lstArreglo)
        return lstArreglo;
    }
    const postGenerarReporte = function (objUrl, Token, objEstacion) {
    

        let objParams = {
            "token": Token,
            "stationId": cboEstaciones.val(),
            //"stationId": objEstacion.stationId,
            "fromDate": dtFechaInicio.val(),
            "toDate": dtFechaFin.val(),
        }
        let parametros = {
            Url: objUrl.urlApi,
            objParams: JSON.stringify(objParams),
            Token: Token,
            stationId: cboEstaciones.val(),
            //stationId: objEstacion.stationId,
            commercialName: objEstacion,
            //commercialName: objEstacion.commercialName,
            fechaInicio:dtFechaInicio.val(), 
            fechaFin: dtFechaFin.val(),
            IdZonas: cboZonas.val(),
        }
        const options = url + 'postGenerarReporte';
        axios.post(options, parametros).then(function (response) {
            const result = response;
            //dtReportes.clear();
            //dtReportes.rows.add(result.data);
            //dtReportes.draw();

        }).catch(function (error) {
            console.error(error);
        });
    }


    const obtenerCombo = function () {
        let parametros = {
        }
        const options = urlHome + 'ObtenerConfig';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                if (result.ITEMS.SitioWEB != 'local') {
                    GenerarUrlMedianteApiCombo(result.ITEMS);
                } else {
                    local();
                }
            } else {
            }
        }).catch(function (error) {
            console.error(error);
        });
    }
    const GenerarUrlMedianteApiCombo = function (objUrl) {
        let parametros = {
            Usuario: objUrl.usuarioApi,
            Password: objUrl.passwordApi,
            Url: objUrl.urlApi,
            EndPoint: 'GetToken'
        }
        const options = urlHome + 'LoginApiSistema';
        axios.post(options, parametros).then(function (response) {
            const result = JSON.parse(response.data);
            GetStationData(objUrl, result.Data)

        }).catch(function (error) {
            console.error(error);
        });
    }
    const GetStationData = function (objUrl, Token) {
        let item = {
            token: Token,
            stationId: -1,
            Identifier:  -1 
        }
        let parametros = {
            Url: objUrl.urlApi,
            EndPoint: 'GetStationData',
            objParams: JSON.stringify(item)
        }
        const options = urlHome + 'EndPointNexus';
        axios.post(options, parametros).then(function (response) {
            const result = JSON.parse(response.data);
                if (RevisarZonas != null) {
            if (RevisarZonas.length != 0) {
                let rlst = [];
                RevisarZonas.forEach(element => {
                    let item = $.grep(result.Data, function (el, index) { return (el.StationId == element.IdEstaciones); })
                    rlst.push(item[0]);
                });
                result.Data = rlst;
            }
                } else {
                    result.Data = [];
                    alert('no tienes asignado estaciones en esas zonas')
            }
            cboEstaciones.find('optgroup').remove();
            let html = `<optgroup label="ESTACIONES">`;
                html += `<option value="-1">TODAS</option>`;
            result.Data.forEach(x => {
                if (x != undefined) {
                    html += `<option value="${x.StationId}">${x.StationNumber} -  ${x.StationName}</option>`;
                }
            });
            html +=`</optgroup>`;
            cboEstaciones.append(html);
            cboEstaciones.select2();
            cboEstaciones.trigger('change');


        }).catch(function (error) {
            console.error(error);
        });
    }

    const postExtraerEstacionesXZonas = function () {
        let parametros = {
            IdZona: cboZonas.val()
        }
        const options = urlHome + 'ExtraerEstacionesXZonas';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            RevisarZonas = result;
            obtenerCombo();
        }).catch(function (error) {
            console.error(error);
        });
    }

    const cboZona = function (objUrl, Token) {
        let parametros = {
        }
        const options = urlHome + 'cboZona';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            let html = ``;
            result.forEach(x => {
                html += `<option value="${x.IdZona}">${x.Descripcion}</option>`;
            });
            html += ``;
            cboZonas.append(html);
            cboZonas.select2();
            AplicarMetodosDeEstaciones();

        }).catch(function (error) {
            console.error(error);
        });
    }

    var initReportes = function () {
        dtReportes = tblreportes.DataTable({
            language: 'dtDicEsp',
            ordering: true,
            paging: true,
            searching: true,
            bFilter: false,
            info: false,
            columns: [
                { title: 'Estacion', data: 'Estacion' },
                {title: 'Fecha', data:'Fecha'},
                {title: 'Producto', data:'Producto'},
                { title: 'Inv Inicial', data: 'InvInicial', className: 'numerico'},
                { title: 'Compras', data: 'Compras', className: 'numerico'},
                { title: 'Ventas', data: 'Ventas', className: 'numerico'},
                { title: 'Inv Final Calculado', data: 'InvFinalTeorico', className: 'numerico'},
                { title: 'Lectura de Inventario', data: 'LecturaFinal', className: 'numerico'},
                { title: 'Dif Calculado vs Lectura', data: 'DifTeoricoVsLectura', className: 'numerico'},
                { title: 'Ajuste Estación', data: 'DifEstacion', className: 'numerico'},
                { title: 'Pendiente', data: 'Pendiente', className: 'numerico'},
            ],
            columnDefs: [
                { className: 'dt-center', 'targets': '_all' },
                { 'width': '9%', 'targets': [0, 1, 2,3,4,5,6,7,8] }
            ],
            initComplete: function (settings, json) {
                
              
            }
        });

        
        $('#tblreportes_length').css('margin-bottom', '5px');
        //$('#tblreportes_length').css('margin-left', '30px');
        $('#tblreportes_length').find('select').css('border', '1px solid #e5eaed');
        $('#tblreportes_length').find('select').css('border-radius', '.5rem');
        $('#tblreportes_length').find('select').css('padding', '.5rem .75rem');
        $('#tblreportes_length').find('select').css('color', '#5e6278');
        $('#tblreportes_length').find('select').css('margin-left', '10px');
        $('#tblreportes_length').find('select').css('margin-right', '10px');
        $('#tblreportes_length').find('select').css('outline', 'none');
        $('#tblreportes_length').find('label').css('color', '#05a692');
        $('#tblreportes_length').find('label').css('font-weight', '600');
        $('#tblreportes_length').find('label').css('font-size', '15px');
        $('#tblreportes_paginate').css('text-align', 'right');
        $('#tblreportes_paginate').css('min-height', '30px');
       
        $('#tblreportes_filter').find('label').css('color', '#05a692');
        $('#tblreportes_filter').find('label').css('font-weight', '900');
        $('#tblreportes_filter').find('label').css('font-size', '20px');
        $('#tblreportes_filter').find('label').css('order', '2');
        $('#tblreportes_filter').find('input').css('margin-left', '5px');
        $('#tblreportes_filter').find('input').css('border', '1px solid #e5eaed');
        $('#tblreportes_filter').find('input').css('border-radius', '.5rem');
        $('#tblreportes_filter').find('input').css('padding', '.5rem .75rem');
        $('#tblreportes_filter').find('input').css('outline', 'none');
        $('#tblreportes_filter').find('input').css('color', '#5e6278');
        $('#tblreportes_filter').find('input').css('font-size', '15px');
        $('#tblreportes_filter').css('text-align', 'right');
    }

    const obtenerExcel = function (lstDatos) {
            funesperar(0);
        let parametros = {
        }
        const options = urlHome + 'ObtenerConfig';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                if (result.ITEMS.SitioWEB != 'local') {
                    postGenerarMebaRptExcel(lstDatos, result.ITEMS);
                } else {
                    local();
                }
            } else {
            }
        }).catch(function (error) {
            console.error(error);
        });
    }
    const postGenerarMebaRptExcel = function (lstDatos, ITEMS) {
        let parametros = {
        }
        const options = url + 'postGenerarMebaRpt';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (ITEMS.SitioWEB != 'local') {
                GenerarUrlMedianteApiExcel(lstDatos, ITEMS);
            } else {
                local();
            }
        }).catch(function (error) {
            console.error(error);
        });
    }
    const GenerarUrlMedianteApiExcel = function (lstDatos, objUrl) {
        let parametros = {
            Usuario: objUrl.usuarioApi,
            Password: objUrl.passwordApi,
            Url: objUrl.urlApi,
            EndPoint: 'GetToken'
        }
        const options = urlHome + 'LoginApiSistema';
        axios.post(options, parametros).then(function (response) {
            const result = JSON.parse(response.data);
            //if (lstDatos.length != 0) {
            if (cboEstaciones.val() != '') {
                //    lstDatos.forEach(x => {
                postGenerarReporteExcel(objUrl, result.Data, cboEstaciones.find("option[value=" + cboEstaciones.val() + "]").text())
                //});
            } else {
                alert('seleccione porfavor una estacion')
            }

        }).catch(function (error) {
            console.error(error);
        });
    }
    const postGenerarReporteExcel = function (objUrl, Token, objEstacion) {


        let objParams = {
            "token": Token,
            "stationId": cboEstaciones.val(),
            //"stationId": objEstacion.stationId,
            "fromDate": dtFechaInicio.val(),
            "toDate": dtFechaFin.val(),
        }

        let parametros = {
            Url: objUrl.urlApi,
            objParams: JSON.stringify(objParams),
            Token: Token,
            stationId: cboEstaciones.val(),
            //stationId: objEstacion.stationId,
            commercialName: objEstacion,
            //commercialName: objEstacion.commercialName,
            fechaInicio: dtFechaInicio.val(),
            fechaFin: dtFechaFin.val(),
            IdZonas: cboZonas.val(),
        }
        const options = url + 'postGenerarResumenReporteAI';
        axios.post(options, parametros).then(function (response) {
            const result = response;
            //dtReportes.clear();
            //dtReportes.rows.add(result.data);
            //dtReportes.draw();
            console.log(result)
            downloadPDF(result.data);
        }).catch(function (error) {
            console.error(error);
        });
    }


    const downloadPDF = (pdf) => {
        const linkSource = `data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,${pdf}`;
        const downloadLink = document.createElement("a");
        downloadLink.href = linkSource;
        downloadLink.target = "_blank";
        downloadLink.download = "Reporte.xlsx";
        document.body.appendChild(downloadLink)
        downloadLink.click()
        document.body.removeChild(downloadLink)
        funesperar(1);
    };
    const funesperar = function (timer) {
        let timerInterval
        Swal.fire({
            title: 'El contenido se está cargando',
            text: 'Esto puede demorar unos segundos.',
            icon: 'info',
            timer: timer,
            timerProgressBar: true,
            allowOutsideClick: false,
            didOpen: () => {
                Swal.showLoading()
                const b = Swal.getHtmlContainer().querySelector('b')
                timerInterval = setInterval(() => {
                    //b.textContent = Swal.getTimerLeft()
                }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
            }
        })
    }

    return {
        Inicializar: Inicializar,
    }
};