﻿var ProductosControllers = function () {
    const url = '/Productos/';
    const urlReportes = '/Productos/';
    const urlHome = '/Home/';
    const tblProductos = $('#tblProductos');
    let dtProducto;

    const btnBuscar = $('#btnBuscar');
    const titleProducto = $('#titleProducto');
    const txtAccion = $('#txtAccion');
    const txtIdProducto = $('#txtIdProducto');
    const txtPrecio = $('#txtPrecio');
    const txtDescripcion = $('#txtDescripcion');
    const txtimg = $('#txtimg');
    const btnAgregar = $('#btnAgregar');
    const btnGuardar = $('#btnGuardar');
    let Archivo = "";
    const btnEliminar = $('#btnEliminar');
    const inpEstatus = $('#inpEstatus');
    const txtBorrar = $('#txtBorrar');
    const btnCancelar = $('#btnCancelar');
    const cbUnidad = $('#cbUnidad');
    const txtCodigo = $('#txtCodigo');
    const txtCodigoNombreDelProducto = $('#txtCodigoNombreDelProducto');

    const btnAtras = $('#btnAtras');
    const btnNum = $('#btnNum');
    const btnAdelante = $('#btnAdelante');
    const txtBuscador = $('#txtBuscador');
    const btnBuscador = $('#btnBuscador');
    
    let variable = 1;



    var Inicializar = function () {
        //postGenerarReporteExcel();
        functionListar();
    }
    const functionListar = function () {
        $('.btn-close').css('background-color', '#FFF');

        document.getElementById('txtimg').addEventListener('change', function (event) {
            const file = event.target.files[0];
            if (file) {
                const reader = new FileReader();
                reader.onload = function (e) {
                    const base64String = e.target.result.split(',')[1]; // Quita el prefijo `data:...`
                    console.log('Archivo en Base64:', base64String);
                    Archvo = base64String;
                };
                reader.onerror = function (error) {
                    console.error('Error al leer el archivo:', error);
                };
                reader.readAsDataURL(file); // Lee el archivo como una URL de datos (Base64)
            }
        });

        initProducto();
        postSpdObtenerListadoCatProductos();
        btnBuscar.click(function () {
            postSpdObtenerListadoCatProductos();
        });
        btnAgregar.click(function () {
            txtPrecio.val('');
            txtDescripcion.val('');
            txtAccion.val('A');
            cbUnidad.val('');
            txtCodigo.val('');
            txtCodigoNombreDelProducto.val('');
            $('#mdlAgregarEditar').modal('show');
            $('.btn-close').css('background-color', '#FFF');
            titleProducto.text('Agregar Producto');
            txtIdProducto.val(0);
        });
        btnGuardar.click(function () {
            postSpdAgregarModificarEliminar();
        });
        btnEliminar.click(function () {
            postEliminar();
        });
        btnCancelar.click(function () {
            $('#mdlAgregarEditar').modal('hide');

        });

        btnAtras.click(function () {
            if (btnNum.text() != '1') {
                variable--;
                btnNum.text(variable);
                postSpdObtenerListadoCatProductos();
            }
        });
        btnAdelante.click(function () {
                variable++;
                btnNum.text(variable);
                postSpdObtenerListadoCatProductos();
        });
    }
    var initProducto = function () {
        dtProducto = tblProductos.DataTable({
            language: 'dtDicEsp',
            ordering: true,
            paging: false,
            searching: false,
            bFilter: false,
            info: false,
            columns: [
                { data: 'id', title: 'id', visible: false },
                {
                    title: 'Codigo', render: (data, type, row) => {
                        let html = validarEspacios(row.codigoP);
                        return html;
                    }
                },
                {
                    title: 'Nombre', render: (data, type, row) => {
                        let html = validarEspacios(row.codigo);
                        return html;
                    }
                },
                {
                    title: 'Descripcion', render: (data, type, row) => {
                        let html = validarEspacios(row.descripcion);
                        return html;
                    }
                },
                {
                    title: 'Unidad', render: (data, type, row) => {
                        let html = `${row.unidad}`;
                        return html;
                    }
                },
                {
                    title: 'img', render: (data, type, row) => {
                        let html = `<img src="${row.img}" style="width:100%">`;
                        return html;
                    }
                },
                {
                    title: 'precioVenta', render: (data, type, row) => {
                        let html = validarEspacios(row.precioVenta);
                        return html;
                    }
                },
                {
                    title: 'Acciones', render: function (data, type, row) {
                        let btnEliminar = '';



                        btnEliminar = `<button class='btn-eliminar btn btn-danger eliminarProducto' data-id='${row.id}'>` +
                            `<i class='fas fa-trash' style='font-size:18px'></i></button>`;

                        btnEliminar += `<button class='btn-editar btn btn-primary editarProducto' data-id='${row.id}'>` +
                            `<i class='fas fa-pencil-alt' style='font-size:18px'></i>` +
                            `</button>`;

                        return btnEliminar;

                    }
                }
            ],
            columnDefs: [
                { className: 'dt-center', 'targets': '_all' },
                { 'width': '40%', 'targets': [1, 2] },
                { 'width': '15%', 'targets': [3, 4] },
            ],
            initComplete: function (settings, json) {


                tblProductos.on('click', '.eliminarProducto', function () {
                    const rowData = dtProducto.row($(this).closest('tr')).data();
                    var str = "";
                    str = " ¿Esta seguro de querer Eliminar este Producto?"
                    txtBorrar.text(str)
                    btnEliminar.text('Eliminar');
                    inpEstatus.val(false);

                    txtIdProducto.val(rowData.id);
                    txtAccion.val('C');
                    $('.btn-close').css('background-color', '#FFF');
                    $('#mdlEliminar').modal('show');

                });
                tblProductos.on('click', '.editarProducto', function (e) {
                    const rowData = dtProducto.row($(this).closest('tr')).data();
                    txtAccion.val('B');
                    txtIdProducto.val(rowData.id);
                    txtDescripcion.val(rowData.descripcion);
                    txtPrecio.val(rowData.precioVenta);
                    cbUnidad.val(rowData.unidad);
                    txtCodigo.val(rowData.codigoP);
                    txtCodigoNombreDelProducto.val(rowData.codigo);
                    $('.btn-close').css('background-color', '#FFF');
                    titleProducto.text('Editar Producto');
                    $('#mdlAgregarEditar').modal('show');

                });



            }
        });

        $('#tblProductos_length').css('margin-bottom', '5px');

        //$('#tblProductos_length').css('margin-left', '30px');
        $('#tblProductos_length').find('select').css('border', '1px solid #e5eaed');
        $('#tblProductos_length').find('select').css('border-radius', '.5rem');
        $('#tblProductos_length').find('select').css('padding', '.5rem .75rem');
        $('#tblProductos_length').find('select').css('color', '#5e6278');
        $('#tblProductos_length').find('select').css('margin-left', '10px');
        $('#tblProductos_length').find('select').css('margin-right', '10px');
        $('#tblProductos_length').find('select').css('outline', 'none');
        //$('#tblProductos_length').find('label').css('color', '#05a692');
        //$('#tblProductos_length').find('label').css('font-weight', '600');
        //$('#tblProductos_length').find('label').css('font-size', '15px');
        $('#tblProductos_paginate').css('text-align', 'right');
        $('#tblProductos_paginate').css('min-height', '30px');

        //$('#tblProductos_filter').find('label').css('color', '#05a692');
        //$('#tblProductos_filter').find('label').css('font-weight', '900');
        //$('#tblProductos_filter').find('label').css('font-size', '20px');
        $('#tblProductos_filter').find('label').css('order', '2');
        $('#tblProductos_filter').find('input').css('margin-left', '5px');
        $('#tblProductos_filter').find('input').css('border', '1px solid #e5eaed');
        $('#tblProductos_filter').find('input').css('border-radius', '.5rem');
        $('#tblProductos_filter').find('input').css('padding', '.5rem .75rem');
        $('#tblProductos_filter').find('input').css('outline', 'none');
        $('#tblProductos_filter').find('input').css('color', '#5e6278');
        $('#tblProductos_filter').find('input').css('font-size', '15px');
        $('#tblProductos_filter').css('text-align', 'left');


    }
    var validarEspacios = function (str) {
        if (str == null) {
            str = '';
        }
        return str;
    }
    const postSpdObtenerListadoCatProductos = function () {
        console.log(txtBuscador.val())
        console.log(txtBuscador.val())
        console.log(txtBuscador.val())
        let parametros = {
            numPag:10,
            Pag: btnNum.text(),
            descripcion: txtBuscador.val() == null ? "" : txtBuscador.val()

        }
        console.log(txtBuscador.val())
        const options = urlReportes + 'postSpdObtenerListadoCatProductos';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            console.log(result.ITEMS)
            dtProducto.clear();
            dtProducto.rows.add(result.ITEMS);
            dtProducto.draw();
        }).catch(function (error) {
            console.error(error);
        });
    }

    const funesperar = function (timer) {
        let timerInterval
        Swal.fire({
            title: 'El contenido se está cargando',
            text: 'Esto puede demorar unos segundos.',
            icon: 'info',
            timer: timer,
            timerProgressBar: true,
            allowOutsideClick: false,
            didOpen: () => {
                Swal.showLoading()
                const b = Swal.getHtmlContainer().querySelector('b')
                timerInterval = setInterval(() => {
                    //b.textContent = Swal.getTimerLeft()
                }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
            }
        })
    }
    const obtenerFormdata = function (base64String, conimagen = true) {
        let formData = new FormData();
        var file = document.getElementById("txtimg").files[0];
        formData.append("Archivo", file);
        formData.append("parametros[Archivo]", file);
        formData.append("parametros[Opcion]", txtAccion.val());
        formData.append("parametros[id]", txtIdProducto.val());
        formData.append("parametros[descripcion]", txtDescripcion.val());
        formData.append("parametros[img64]", base64String);
        if (conimagen) {
            formData.append("parametros[tipoFormato]", document.getElementById('txtimg').files[0].type);
        }
        formData.append("parametros[img]", "");
        formData.append("parametros[precioVenta]", txtPrecio.val());
        formData.append("parametros[unidad]", cbUnidad.val());
        formData.append("parametros[codigoP]", txtCodigo.val());
        formData.append("parametros[codigo]", txtCodigoNombreDelProducto.val());
        
        return formData;
    }
    const postSpdAgregarModificarEliminar = function () {
        var Arch = "";
        const file = document.getElementById('txtimg').files[0];

        if (file) {
            const reader = new FileReader();
            reader.onload = function (e) {
                const base64String = e.target.result.split(',')[1]; // Quita el prefijo `data:...`
                console.log('Archivo en Base64:', base64String);
                Archvo = base64String;
                Arch = base64String;
                let parametros = obtenerFormdata(base64String);
                const options = urlReportes + 'postSpdAgregarModificarEliminar';
                $.ajax({
                    type: "POST",
                    url: options,
                    data: parametros,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        console.log('---->', response)

                        $('#mdlAgregarEditar').modal('hide');
                        postSpdObtenerListadoCatProductos();
                    },
                    error: function (error) {
                    console.log('---->', error)
                    }
                });
            };
            reader.onerror = function (error) {
                console.error('Error al leer el archivo:', error);
            };
            reader.readAsDataURL(file); // Lee el archivo como una URL de datos (Base64)
        } else {
            let parametros = obtenerFormdata("", false);
            const options = urlReportes + 'postSpdAgregarModificarEliminar';
            $.ajax({
                type: "POST",
                url: options,
                data: parametros,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (response) {
                        console.log('---->',response)
                    $('#mdlAgregarEditar').modal('hide');
                    postSpdObtenerListadoCatProductos();
                },
                error: function (error) {
                    console.log('---->', error)
                }
            });
        }
        console.log(Arch);

    }
    const obtenerFormdataEliminar = function () {
        let formData = new FormData();
        var file = document.getElementById("txtimg").files[0];
        formData.append("Archivo", file);
        formData.append("parametros[Archivo]", file);
        formData.append("parametros[Opcion]", txtAccion.val());
        formData.append("parametros[id]", txtIdProducto.val());
        formData.append("parametros[descripcion]", txtDescripcion.val());
        formData.append("parametros[img64]", "");
        formData.append("parametros[tipoFormato]", "");
        formData.append("parametros[img]", "");
        formData.append("parametros[precioVenta]", txtPrecio.val());
        formData.append("parametros[codigoP]", txtCodigo.val());
        formData.append("parametros[codigo]", txtCodigoNombreDelProducto.val());
        return formData;
    }
    const postEliminar = function () {
        let parametros = obtenerFormdataEliminar();
        const options = urlReportes + 'postSpdAgregarModificarEliminar';
        $.ajax({
            type: "POST",
            url: options,
            data: parametros,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (response) {
                $('#mdlEliminar').modal('hide');
                postSpdObtenerListadoCatProductos();
            },
            error: function (error) {
            }
        });

    }




    return {
        Inicializar: Inicializar,
    }
};