﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MEBA.Areas.PruebasMariaTemplates.Controllers
{
    public class VistasController : Controller
    {
        // GET: PruebasMariaTemplates/Vistas
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Usuarios()
        {
            return View();
        }
        public ActionResult Grupos()
        {
            return View();
        }
        public ActionResult Usuarios2()
        {
            return View();
        }
        public ActionResult Grupos2()
        {
            return View();
        }
        public ActionResult Permisos()
        {
            return View();
        }
        public ActionResult Estaciones()
        {
            return View();
        }
        public ActionResult Bitacora()
        {
            return View();
        }
        public ActionResult Reporte()
        {
            return View();
        }
       
    }
}