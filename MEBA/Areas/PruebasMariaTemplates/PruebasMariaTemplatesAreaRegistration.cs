﻿using System.Web.Mvc;

namespace MEBA.Areas.PruebasMariaTemplates
{
    public class PruebasMariaTemplatesAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "PruebasMariaTemplates";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "PruebasMariaTemplates_default",
                "PruebasMariaTemplates/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}