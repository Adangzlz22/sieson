﻿using System.Web.Mvc;

namespace MEBA.Areas.VariacionInventario
{
    public class VariacionInventarioAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "VariacionInventario";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "VariacionInventario_default",
                "VariacionInventario/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}