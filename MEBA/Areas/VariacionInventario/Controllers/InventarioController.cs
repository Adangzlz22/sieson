﻿using MEBA.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MEBA.Areas.VariacionInventario.Controllers
{
    public class InventarioController : Controller
    {
        // GET: VariacionInventario/Inventario
        public ActionResult Index()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
               
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
            //ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
            //ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
            //return View();
        }
        public ActionResult VariacionEstaciones()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;

                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult VariacionEstacionesFecha()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;

                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult VariacionEstacionesFechaDesglose()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;

                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult EstacionesNPD()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;

                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult LoginNPD()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;

                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult RegistroNPD()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;

                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult Dashboard()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;

                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }


        // GET: VariacionInventario/Inventario/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: VariacionInventario/Inventario/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VariacionInventario/Inventario/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: VariacionInventario/Inventario/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: VariacionInventario/Inventario/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: VariacionInventario/Inventario/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: VariacionInventario/Inventario/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
