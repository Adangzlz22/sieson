﻿var IndexControllers = function () {
    const urlHome = '/Home/';
    const url = '/Reportes/';
    const btnConsultar = $('#btnConsultar');
    const dtFechaInicio = $('#dtFechaInicio');
    const dtFechaFin = $('#dtFechaFin');
    const tblVariacion = $('#tblVariacion');
    let dtVariacion;

    const tblVariacionEstaciones = $('#tblVariacionEstaciones');
    let dtVariacionEstaciones;

    const contenedorVariacion = $('#contenedorVariacion');
    const contenedorVariacionEstaciones = $('#contenedorVariacionEstaciones');
    const contenedorBack = $('#contenedorBack');
    const inpVariacion = $('#inpVariacion');
    const btnBack = $('#btnBack');

    const contenedorVariacionFechas = $('#contenedorVariacionFechas');
    const tblVariacionFechas = $('#tblVariacionFechas');
    let dtVariacionFechas;

    const contenedorVariacionDesglose = $('#contenedorVariacionDesglose');
    const lblEstacion = $('#lblEstacion');
    const dtFechaSeleccionada = $('#dtFechaSeleccionada');
    const tblVariacionDesglose = $('#tblVariacionDesglose');

    const lblProducto = $('#lblProducto');
    const lblEstacionSelec = $('#lblEstacionSelec');



    var Inicializar = function () {

        initVariacion();
        initVariacionEstaciones();
        initVariacionFechas();

        inpVariacion.val(1);


        ExtraerZonas();
        let date = new Date()
        let day = date.getDate()
        let month = date.getMonth() + 1
        let year = date.getFullYear()
        let fecha1 = "";
        if (month < 10) {
            fecha1 = `${year}-0${month}-${day}`;
        } else {
            fecha1 = `${year}-${month}-${day}`;
        }
        dtFechaInicio.val(fecha1);
        dtFechaFin.val(fecha1);

        btnConsultar.click(function () {
            postLstDatos();
        });
        btnBack.click(function () {
            if (inpVariacion.val() == 2) {
                contenedorBack.css('display', 'none');
                contenedorVariacionEstaciones.css('display', 'none');
                contenedorVariacion.css('display', 'block');
            }
            if (inpVariacion.val() == 3) {
                inpVariacion.val(2);
                contenedorVariacionEstaciones.css('display', 'block');
                contenedorVariacionFechas.css('display', 'none');
            }
            if (inpVariacion.val() == 4) {
                inpVariacion.val(3);
                contenedorVariacionFechas.css('display', 'block');
                contenedorVariacionDesglose.css('display', 'none');

            }
        });
    }
    const ExtraerZonas = function () {
        let parametros = {};
        const options = url + 'postLstDatos';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            let repetido = 0;
            let lstZonas = [];
            result.forEach(element => {

                let zona = lstZonas.find(s => s.IdZona == element.IdZona);
                if (zona == undefined) {
                    let item = {
                        Activo: element.Activo,
                        Descripcion: element.Descripcion,
                        IdEmpresa: element.IdEmpresa,
                        IdEstaciones: element.IdEstaciones,
                        IdRelZonaEstaciones: element.IdRelZonaEstaciones,
                        IdZona: element.IdZona,
                        IdZona1: element.IdZona1,
                        Magna: 1,
                        Premium: 1,
                        Diesel: 1,
                    }
                    lstZonas.push(item);
                }
            });
            let lstDatos = [];
            lstZonas.forEach(element => {
                let item = {
                    lstZonas: element,
                    lstEstacionesXElement: []
                }
                lstDatos.push(item);
            });
            dtVariacion.clear();
            dtVariacion.rows.add(lstDatos);
            dtVariacion.draw();

        }).catch(function (error) {
            console.error(error);
        });
    }
    const postLstDatos = function () {
        let parametros = {};
        const options = url + 'postLstDatos';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            let repetido = 0;
            let lstZonas = [];
            result.forEach(element => {

                let zona = lstZonas.find(s => s.IdZona == element.IdZona);
                if (zona == undefined) {
                    let item = {
                        Activo: element.Activo,
                        Descripcion: element.Descripcion,
                        IdEmpresa: element.IdEmpresa,
                        IdEstaciones: element.IdEstaciones,
                        IdRelZonaEstaciones: element.IdRelZonaEstaciones,
                        IdZona: element.IdZona,
                        IdZona1: element.IdZona1,
                        Magna: 1,
                        Premium: 1,
                        Diesel: 1,
                    }
                    lstZonas.push(item);
                }
            });
            obtener(lstZonas, result);

        }).catch(function (error) {
            console.error(error);
        });
    }

    const obtener = function (lstZonas, lstEstaciones) {

        let parametros = {
        }
        const options = urlHome + 'ObtenerConfig';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (result.SUCCESS == true) {
                if (result.ITEMS.SitioWEB != 'local') {
                    postGenerarMebaRpt(lstZonas, lstEstaciones, result.ITEMS);
                } else {
                    local();
                }
            } else {
            }
        }).catch(function (error) {
            console.error(error);
        });
    }

    const postGenerarMebaRpt = function (lstZonas, lstEstaciones, ITEMS) {
        let parametros = {
        }
        const options = url + 'postGenerarMebaRpt';
        axios.post(options, parametros).then(function (response) {
            const result = response.data;
            if (ITEMS.SitioWEB != 'local') {
                GenerarUrlMedianteApi(lstZonas, lstEstaciones, ITEMS);
            } else {
                local();
            }
        }).catch(function (error) {
            console.error(error);
        });
    }
    const GenerarUrlMedianteApi = function (lstZonas, lstEstaciones, objUrl) {
        let parametros = {
            Usuario: objUrl.usuarioApi,
            Password: objUrl.passwordApi,
            Url: objUrl.urlApi,
            EndPoint: 'GetToken'
        }
        const options = urlHome + 'LoginApiSistema';
        axios.post(options, parametros).then(function (response) {
            const result = JSON.parse(response.data);
            GetStationData(objUrl, result.Data, lstZonas, lstEstaciones)

            //window.open('/Reportes/WebRptVentasVsInventario.aspx', '_blank');

        }).catch(function (error) {
            console.error(error);
        });
    }

    const GetStationData = function (objUrl, Token, lstZonas, lstEstaciones) {
        let item = {
            token: Token,
            stationId: -1,
            Identifier: -1
        }
        let parametros = {
            Url: objUrl.urlApi,
            EndPoint: 'GetStationData',
            objParams: JSON.stringify(item)
        }
        const options = '/Home/' + 'EndPointNexus';
        axios.post(options, parametros).then(function (response) {
            const result = JSON.parse(response.data);
            postGenerarReporte(objUrl, Token, lstZonas, lstEstaciones, result.Data)

        }).catch(function (error) {
            console.error(error);
        });
    }

    const postGenerarReporte = function (objUrl, Token, lstZonas, lstEstaciones, lstStationData) {
        funesperar(0);
        let objParams = {
            "token": Token,
            "stationId": -1,
            //"stationId": objEstacion.stationId,
            "fromDate": dtFechaInicio.val(),
            "toDate": dtFechaFin.val(),
        };
        let parametros = {
            Url: objUrl.urlApi,
            objParams: JSON.stringify(objParams),
            Token: Token,
            //stationId: cboEstaciones.val(),
            //stationId: objEstacion.stationId,
            //commercialName: objEstacion,
            //commercialName: objEstacion.commercialName,
            fechaInicio: dtFechaInicio.val(),
            fechaFin: dtFechaFin.val(),
            lstEstaciones: objParams
        }
        const options = url + 'postGenerarReporteVariacion';
        axios.post(options, parametros).then(function (response) {
            const lstZonasLimit = response.data;

            let lstDatos = [];
            lstZonas.forEach(element => {
                let item = {
                    lstZonas: element,
                    lstEstacionesXElement: $.grep(lstEstaciones, function (el, index) {
                        return (el.IdZona == element.IdZona);
                    }),
                    lstEstacionesXElement2: obtenerElement2(element.IdZona, $.grep(lstEstaciones, function (el, index) { return (el.IdZona == element.IdZona); }), lstStationData, lstZonasLimit),
                    lstZonasLimit: obtenerlstZonasLimit(element.IdZona, $.grep(lstEstaciones, function (el, index) { return (el.IdZona == element.IdZona); }), lstZonasLimit),
                }
                lstDatos.push(item);
            });



            //let zona = lstStationData.find(s => s.StationId == el.IdEstaciones);

            funesperar(1);
            dtVariacion.clear();
            dtVariacion.rows.add(lstDatos);
            dtVariacion.draw();

        }).catch(function (error) {
            console.error(error);
        });
    }
    const obtenerElement2 = function (IdZona, lstEstaciones, lstStationData, lstZonasLimit) {
        let lstReturn = [];
        let lstDatos = $.grep(lstEstaciones, function (el, index) {
            return (el.IdZona == IdZona);
        });
        lstDatos.forEach(element => {
            let zona = lstStationData.find(s => s.StationId == element.IdEstaciones);
            let item = {
                CurrentDate: zona.CurrentDate,
                CurrentShift: zona.CurrentShift,
                Identifier: zona.Identifier,
                StampBalance: zona.StampBalance,
                StationId: zona.StationId,
                StationName: zona.StationName,
                StationNumber: zona.StationNumber,
                StationShortName: zona.StationShortName,
                lstEstaciones: lstDatos,
                lstStationData: lstStationData,
                lstZonasLimit: obtenerlstZonasLimit(element.IdZona, $.grep(lstEstaciones, function (el, index) { return (el.IdZona == element.IdZona); }), lstZonasLimit),
                lstZonasLimitSinFormat: lstZonasLimit
            }
            lstReturn.push(item);
        });

        return lstReturn;
    }

    const obtenerlstZonasLimit = function (IdZona, lstEstaciones, lstZonasLimit) {
        let lstReturn = [];
        let lstDatos = $.grep(lstEstaciones, function (el, index) {
            return (el.IdZona == IdZona);
        });
        lstDatos.forEach(element => {
            let zona = $.grep(lstZonasLimit, function (el, index) { return (el.IdEstacion == element.IdEstaciones); })

            lstReturn.push(zona);
        });
        return lstReturn;
    }


    var initVariacion = function () {
        dtVariacion = tblVariacion.DataTable({
            language: 'dtDicEsp',
            ordering: true,
            paging: true,
            searching: true,
            bFilter: false,
            info: false,
            pageLength: 25,
            columns: [
                {
                    title: 'Zonas : ', visible: false, render: (data, type, row) => {
                        let html = ` ${row.lstZonas.IdZona}`;
                        return html;
                    }
                },
                {
                    title: 'Zonas', render: (data, type, row) => {
                        let html = ` <h4 Id="btnZona${row.lstZonas.IdZona}"><span class="spnZona" style="color:#05a692">${row.lstZonas.Descripcion}</span></h4>`;

                        return html;
                    }
                },
                {
                    title: '87 Octanos', render: (data, type, row) => {
                        let magna = "0787"
                        let html = ` ${ObtenerRango(row.lstZonasLimit, magna)}`;
                        return html;
                    }
                },
                {
                    title: '91 Octanos', render: (data, type, row) => {
                        let premium = "0792"
                        let html = ` ${ObtenerRango(row.lstZonasLimit, premium )}`;
                        return html;
                    }
                },
                {
                    title: 'Diesel', render: (data, type, row) => {
                        let diesel = "03";
                        let html = ` ${ObtenerRango(row.lstZonasLimit, diesel)}`;
                        return html;
                    }
                },

            ],
            columnDefs: [
                { className: 'dt-center', 'targets': '_all' },
                { 'width': '25%', 'targets': [1, 2, 3, 4] }
            ],
            initComplete: function (settings, json) {

                $('#tblVariacion').on('click', 'tr', function () {
                    const rowData = dtVariacion.row($(this).closest('tr')).data();



                    contenedorVariacion.css('display', 'none');
                    inpVariacion.val(2);
                    contenedorBack.css('display', 'block');
                    ObtenerLstVariacionEstaciones(rowData);
                    tblVariacionEstaciones.css('width', '100%');
                    contenedorVariacionEstaciones.css('display', 'block');
                });

            }
        });




        $('#tblVariacion_length').css('margin-bottom', '5px');
        //$('#tblreportes_length').css('margin-left', '30px');
        $('#tblVariacion_length').find('select').css('border', '1px solid #e5eaed');
        $('#tblVariacion_length').find('select').css('border-radius', '.5rem');
        $('#tblVariacion_length').find('select').css('padding', '.5rem .75rem');
        $('#tblVariacion_length').find('select').css('color', '#5e6278');
        $('#tblVariacion_length').find('select').css('margin-left', '10px');
        $('#tblVariacion_length').find('select').css('margin-right', '10px');
        $('#tblVariacion_length').find('select').css('outline', 'none');
        $('#tblVariacion_length').find('label').css('color', '#05a692');
        $('#tblVariacion_length').find('label').css('font-weight', '600');
        $('#tblVariacion_length').find('label').css('font-size', '15px');
        $('#tblVariacion_paginate').css('text-align', 'right');
        $('#tblVariacion_paginate').css('min-height', '30px');

        $('#tblVariacion_filter').find('label').css('color', '#05a692');
        $('#tblVariacion_filter').find('label').css('font-weight', '900');
        $('#tblVariacion_filter').find('label').css('font-size', '20px');
        $('#tblVariacion_filter').find('label').css('order', '2');
        $('#tblVariacion_filter').find('input').css('margin-left', '5px');
        $('#tblVariacion_filter').find('input').css('border', '1px solid #e5eaed');
        $('#tblVariacion_filter').find('input').css('border-radius', '.5rem');
        $('#tblVariacion_filter').find('input').css('padding', '.5rem .75rem');
        $('#tblVariacion_filter').find('input').css('outline', 'none');
        $('#tblVariacion_filter').find('input').css('color', '#5e6278');
        $('#tblVariacion_filter').find('input').css('font-size', '15px');
        $('#tblVariacion_filter').css('text-align', 'right');
    }



    const ObtenerRango = function (lstZonasLimit, producto) {
        let variableRetorno = "";

        switch (producto) {
            case "0787":
                if (lstZonasLimit != null) {
                    for (let i = 0; i < lstZonasLimit.length; i++) {
                        if (lstZonasLimit[i].length > 0) {
                            for (let b = 0; b < lstZonasLimit[i].length; b++) {
                                if (lstZonasLimit[i][b].Producto.split('-')[0].trim() == "0787") {
                                    if (lstZonasLimit[i][b].LimiteNivel == 7 || lstZonasLimit[i][b].LimiteNivel == 6) {
                                        variableRetorno = `
                                                                <button type="button" class="btn estado4" style="width:100%">
                                                                    <img src="/Content/img/estado4.png" class="img-estado" />
                                                                </button>`;
                                        return variableRetorno;
                                    } else if (lstZonasLimit[i][b].LimiteNivel == 5 || lstZonasLimit[i][b].LimiteNivel == 4) {
                                        variableRetorno = `
                                                                <button type="button" class="btn estado3" style="width:100%">
                                                                    <img src="/Content/img/estado3.png" class="img-estado" />
                                                                </button>`;
                                        return variableRetorno;
                                    } else if (lstZonasLimit[i][b].LimiteNivel == 3 || lstZonasLimit[i][b].LimiteNivel == 2) {
                                        variableRetorno = `
                                                                <button type="button" class="btn estado2" style="width:100%">
                                                                    <img src="/Content/img/estado2.png" class="img-estado" />
                                                                </button>`;
                                        return variableRetorno;
                                    } else if (lstZonasLimit[i][b].LimiteNivel == 1) {
                                        variableRetorno = `
                                                                <button type="button" class="btn estado1" style="width:100%">
                                                                    <img src="/Content/img/estado1.png" class="img-estado" />
                                                                </button>`;
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            case "0792":
            case "0791":

                if (lstZonasLimit != null) {
                    for (let i = 0; i < lstZonasLimit.length; i++) {
                        if (lstZonasLimit[i].length > 0) {
                            for (let b = 0; b < lstZonasLimit[i].length; b++) {
                                if (lstZonasLimit[i][b].Producto.split('-')[0].trim() == "0792" || lstZonasLimit[i][b].Producto.split('-')[0].trim() == "0791") {
                                    if (lstZonasLimit[i][b].LimiteNivel == 7 || lstZonasLimit[i][b].LimiteNivel == 6) {
                                        variableRetorno = `
                                                                <button type="button" class="btn estado4" style="width:100%">
                                                                    <img src="/Content/img/estado4.png" class="img-estado" />
                                                                </button>`;
                                        return variableRetorno;
                                    } else if (lstZonasLimit[i][b].LimiteNivel == 5 || lstZonasLimit[i][b].LimiteNivel == 4) {
                                        variableRetorno = `
                                                                <button type="button" class="btn estado3" style="width:100%">
                                                                    <img src="/Content/img/estado3.png" class="img-estado" />
                                                                </button>`;
                                        return variableRetorno;
                                    } else if (lstZonasLimit[i][b].LimiteNivel == 3 || lstZonasLimit[i][b].LimiteNivel == 2) {
                                        variableRetorno = `
                                                                <button type="button" class="btn estado2" style="width:100%">
                                                                    <img src="/Content/img/estado2.png" class="img-estado" />
                                                                </button>`;
                                        return variableRetorno;
                                    } else if (lstZonasLimit[i][b].LimiteNivel == 1) {
                                        variableRetorno = `
                                                                <button type="button" class="btn estado1" style="width:100%">
                                                                    <img src="/Content/img/estado1.png" class="img-estado" />
                                                                </button>`;
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            case "03":
                if (lstZonasLimit != null) {
                    for (let i = 0; i < lstZonasLimit.length; i++) {
                        if (lstZonasLimit[i].length > 0) {
                            for (let b = 0; b < lstZonasLimit[i].length; b++) {
                                if (lstZonasLimit[i][b].Producto.split('-')[0].trim() == "03") {
                                    if (lstZonasLimit[i][b].LimiteNivel == 7 || lstZonasLimit[i][b].LimiteNivel == 6) {
                                        variableRetorno = `
                                                                <button type="button" class="btn estado4" style="width:100%">
                                                                    <img src="/Content/img/estado4.png" class="img-estado" />
                                                                </button>`;
                                        return variableRetorno;
                                    } else if (lstZonasLimit[i][b].LimiteNivel == 5 || lstZonasLimit[i][b].LimiteNivel == 4) {
                                        variableRetorno = `
                                                                <button type="button" class="btn estado3" style="width:100%">
                                                                    <img src="/Content/img/estado3.png" class="img-estado" />
                                                                </button>`;
                                        return variableRetorno;
                                    } else if (lstZonasLimit[i][b].LimiteNivel == 3 || lstZonasLimit[i][b].LimiteNivel == 2) {
                                        variableRetorno = `
                                                                <button type="button" class="btn estado2" style="width:100%">
                                                                    <img src="/Content/img/estado2.png" class="img-estado" />
                                                                </button>`;
                                        return variableRetorno;
                                    } else if (lstZonasLimit[i][b].LimiteNivel == 1) {
                                        variableRetorno = `
                                                                <button type="button" class="btn estado1" style="width:100%">
                                                                    <img src="/Content/img/estado1.png" class="img-estado" />
                                                                </button>`;
                                    }
                                }
                            }
                        }
                    }
                }
                break;

        }



        return variableRetorno;
    }

    const ObtenerRango2Estaciones = function (lstZonasLimit, producto, stationId) {
        let variableRetorno = "";
        switch (producto) {
            case "0787":
                if (lstZonasLimit != null) {
                    for (let i = 0; i < lstZonasLimit.length; i++) {
                        if (lstZonasLimit[i].length > 0) {
                            for (let b = 0; b < lstZonasLimit[i].length; b++) {
                                if (lstZonasLimit[i][b].IdEstacion == stationId) {
                                    if (lstZonasLimit[i][b].Producto.split('-')[0].trim() == "0787") {
                                        if (lstZonasLimit[i][b].LimiteNivel == 7 || lstZonasLimit[i][b].LimiteNivel == 6) {
                                            variableRetorno = `
                                                                <button type="button" class="btn estado4" style="width:100%">
                                                                    <img src="/Content/img/estado4.png" class="img-estado" />
                                                                </button>`;
                                            return variableRetorno;
                                        } else if (lstZonasLimit[i][b].LimiteNivel == 5 || lstZonasLimit[i][b].LimiteNivel == 4) {
                                            variableRetorno = `
                                                                <button type="button" class="btn estado3" style="width:100%">
                                                                    <img src="/Content/img/estado3.png" class="img-estado" />
                                                                </button>`;
                                            return variableRetorno;
                                        } else if (lstZonasLimit[i][b].LimiteNivel == 3 || lstZonasLimit[i][b].LimiteNivel == 2) {
                                            variableRetorno = `
                                                                <button type="button" class="btn estado2" style="width:100%">
                                                                    <img src="/Content/img/estado2.png" class="img-estado" />
                                                                </button>`;
                                            return variableRetorno;
                                        } else if (lstZonasLimit[i][b].LimiteNivel == 1) {
                                            variableRetorno = `
                                                                <button type="button" class="btn estado1" style="width:100%">
                                                                    <img src="/Content/img/estado1.png" class="img-estado" />
                                                                </button>`;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            case "0792":
            case "0791":

                if (lstZonasLimit != null) {
                    for (let i = 0; i < lstZonasLimit.length; i++) {
                        if (lstZonasLimit[i].length > 0) {
                            for (let b = 0; b < lstZonasLimit[i].length; b++) {
                                if (lstZonasLimit[i][b].IdEstacion == stationId) {
                                    if (lstZonasLimit[i][b].Producto.split('-')[0].trim() == "0792" || lstZonasLimit[i][b].Producto.split('-')[0].trim() == "0791") {
                                        if (lstZonasLimit[i][b].LimiteNivel == 7 || lstZonasLimit[i][b].LimiteNivel == 6) {
                                            variableRetorno = `
                                                                <button type="button" class="btn estado4" style="width:100%">
                                                                    <img src="/Content/img/estado4.png" class="img-estado" />
                                                                </button>`;
                                            return variableRetorno;
                                        } else if (lstZonasLimit[i][b].LimiteNivel == 5 || lstZonasLimit[i][b].LimiteNivel == 4) {
                                            variableRetorno = `
                                                                <button type="button" class="btn estado3" style="width:100%">
                                                                    <img src="/Content/img/estado3.png" class="img-estado" />
                                                                </button>`;
                                            return variableRetorno;
                                        } else if (lstZonasLimit[i][b].LimiteNivel == 3 || lstZonasLimit[i][b].LimiteNivel == 2) {
                                            variableRetorno = `
                                                                <button type="button" class="btn estado2" style="width:100%">
                                                                    <img src="/Content/img/estado2.png" class="img-estado" />
                                                                </button>`;
                                            return variableRetorno;
                                        } else if (lstZonasLimit[i][b].LimiteNivel == 1) {
                                            variableRetorno = `
                                                                <button type="button" class="btn estado1" style="width:100%">
                                                                    <img src="/Content/img/estado1.png" class="img-estado" />
                                                                </button>`;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            case "03":
                if (lstZonasLimit != null) {
                    for (let i = 0; i < lstZonasLimit.length; i++) {
                        if (lstZonasLimit[i].length > 0) {
                            for (let b = 0; b < lstZonasLimit[i].length; b++) {
                                if (lstZonasLimit[i][b].IdEstacion == stationId) {
                                    if (lstZonasLimit[i][b].Producto.split('-')[0].trim() == "03") {
                                        if (lstZonasLimit[i][b].LimiteNivel == 7 || lstZonasLimit[i][b].LimiteNivel == 6) {
                                            variableRetorno = `
                                                                <button type="button" class="btn estado4" style="width:100%">
                                                                    <img src="/Content/img/estado4.png" class="img-estado" />
                                                                </button>`;
                                            return variableRetorno;
                                        } else if (lstZonasLimit[i][b].LimiteNivel == 5 || lstZonasLimit[i][b].LimiteNivel == 4) {
                                            variableRetorno = `
                                                                <button type="button" class="btn estado3" style="width:100%">
                                                                    <img src="/Content/img/estado3.png" class="img-estado" />
                                                                </button>`;
                                            return variableRetorno;
                                        } else if (lstZonasLimit[i][b].LimiteNivel == 3 || lstZonasLimit[i][b].LimiteNivel == 2) {
                                            variableRetorno = `
                                                                <button type="button" class="btn estado2" style="width:100%">
                                                                    <img src="/Content/img/estado2.png" class="img-estado" />
                                                                </button>`;
                                            return variableRetorno;
                                        } else if (lstZonasLimit[i][b].LimiteNivel == 1) {
                                            variableRetorno = `
                                                                <button type="button" class="btn estado1" style="width:100%">
                                                                    <img src="/Content/img/estado1.png" class="img-estado" />
                                                                </button>`;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break;

        }



        return variableRetorno;
    }


    var ObtenerLstVariacionEstaciones = function (lstDatos) {
        dtVariacionEstaciones.clear();
        dtVariacionEstaciones.rows.add(lstDatos.lstEstacionesXElement2);
        dtVariacionEstaciones.draw();
    }

    var initVariacionEstaciones = function () {
        dtVariacionEstaciones = tblVariacionEstaciones.DataTable({
            language: 'dtDicEsp',
            ordering: true,
            paging: true,
            searching: true,
            bFilter: false,
            info: false,
            pageLength: 25,
            columns: [
                {
                    title: 'Estaciones : ', visible: false, render: (data, type, row) => {
                        let html = ` ${row.StationId}`;
                        return html;
                    }
                },
                {
                    title: 'Estaciones', render: (data, type, row) => {
                        let html = ` <h4 Id="btnZona${row.StationId}"> <span class="spnZona" style="color:#05a692">${row.StationName}</span></h4>`;

                        return html;
                    }
                },
                {
                    title: '87 Octanos', render: (data, type, row) => {
                        let magna = "0787"

                        let html = ` ${ObtenerRango2Estaciones(row.lstZonasLimit, magna, row.StationId)}`;
                        return html;
                    }
                },
                {
                    title: '91 Octanos', render: (data, type, row) => {
                        let premium = "0792"
                        let html = ` ${ObtenerRango2Estaciones(row.lstZonasLimit, premium, row.StationId)}`;
                        return html;
                    }
                },
                {
                    title: 'Diesel', render: (data, type, row) => {
                        let diesel = "03";
                        let html = ` ${ObtenerRango2Estaciones(row.lstZonasLimit, diesel, row.StationId)}`;
                        return html;
                    }
                },

            ],
            columnDefs: [
                { className: 'dt-center', 'targets': '_all' },
                { 'width': '25%', 'targets': [1, 2, 3, 4] }
            ],
            initComplete: function (settings, json) {

                $('#tblVariacionEstaciones').on('click', 'tr', function () {
                    const rowData = dtVariacionEstaciones.row($(this).closest('tr')).data();

                    filtrarListaFechas(rowData, rowData.StationId);
                    contenedorVariacionFechas.css('display', 'block');
                    contenedorVariacionEstaciones.css('display', 'none');
                    lblEstacionSelec.text(rowData.StationNumber + ' - ' + rowData.StationName)
                    inpVariacion.val(3);
                    tblVariacionFechas.css('width', '100%');
                });

            }
        });




        $('#tblVariacionEstaciones_length').css('margin-bottom', '5px');
        //$('#tblreportes_length').css('margin-left', '30px');
        $('#tblVariacionEstaciones_length').find('select').css('border', '1px solid #e5eaed');
        $('#tblVariacionEstaciones_length').find('select').css('border-radius', '.5rem');
        $('#tblVariacionEstaciones_length').find('select').css('padding', '.5rem .75rem');
        $('#tblVariacionEstaciones_length').find('select').css('color', '#5e6278');
        $('#tblVariacionEstaciones_length').find('select').css('margin-left', '10px');
        $('#tblVariacionEstaciones_length').find('select').css('margin-right', '10px');
        $('#tblVariacionEstaciones_length').find('select').css('outline', 'none');
        $('#tblVariacionEstaciones_length').find('label').css('color', '#05a692');
        $('#tblVariacionEstaciones_length').find('label').css('font-weight', '600');
        $('#tblVariacionEstaciones_length').find('label').css('font-size', '15px');
        $('#tblVariacionEstaciones_paginate').css('text-align', 'right');
        $('#tblVariacionEstaciones_paginate').css('min-height', '30px');

        $('#tblVariacionEstaciones_filter').find('label').css('color', '#05a692');
        $('#tblVariacionEstaciones_filter').find('label').css('font-weight', '900');
        $('#tblVariacionEstaciones_filter').find('label').css('font-size', '20px');
        $('#tblVariacionEstaciones_filter').find('label').css('order', '2');
        $('#tblVariacionEstaciones_filter').find('input').css('margin-left', '5px');
        $('#tblVariacionEstaciones_filter').find('input').css('border', '1px solid #e5eaed');
        $('#tblVariacionEstaciones_filter').find('input').css('border-radius', '.5rem');
        $('#tblVariacionEstaciones_filter').find('input').css('padding', '.5rem .75rem');
        $('#tblVariacionEstaciones_filter').find('input').css('outline', 'none');
        $('#tblVariacionEstaciones_filter').find('input').css('color', '#5e6278');
        $('#tblVariacionEstaciones_filter').find('input').css('font-size', '15px');
        $('#tblVariacionEstaciones_filter').css('text-align', 'right');
    }


    const funesperar = function (timer) {
        let timerInterval
        Swal.fire({
            title: 'El contenido se está cargando',
            text: 'Esto puede demorar unos segundos.',
            icon: 'info',
            timer: timer,
            timerProgressBar: true,
            allowOutsideClick: false,
            didOpen: () => {
                Swal.showLoading()
                const b = Swal.getHtmlContainer().querySelector('b')
                timerInterval = setInterval(() => {
                    //b.textContent = Swal.getTimerLeft()
                }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
            }
        })
    }

    var filtrarListaFechas = function (lstDatosEntero, IdEstacion) {
        let lstZonas = [];
        for (let a = 0; a < lstDatosEntero.lstZonasLimit.length; a++) {
            for (let b = 0; b < lstDatosEntero.lstZonasLimit[a].length; b++) {
                if (lstDatosEntero.lstZonasLimit[a][b].IdEstacion == IdEstacion) {
                    let fecha = lstZonas.find(s => s.Fecha == lstDatosEntero.lstZonasLimit[a][b].Fecha);
                    if (fecha == undefined) {
                        let NombreEstacion = lstDatosEntero.lstStationData.find(s => s.StationId == lstDatosEntero.lstZonasLimit[a][b].IdEstacion);
                        let item = {
                            Fecha: lstDatosEntero.lstZonasLimit[a][b].Fecha,
                            lstZonasLimit: lstDatosEntero.lstZonasLimit,
                            StationId: lstDatosEntero.lstZonasLimit[a][b].IdEstacion,
                            NombreEstacion: NombreEstacion.StationNumber + ' - ' + NombreEstacion.StationName
                        }
                        lstZonas.push(item);
                    }
                }
            }
        }

        dtVariacionFechas.clear();
        dtVariacionFechas.rows.add(lstZonas);
        dtVariacionFechas.draw();

    }
    var initVariacionFechas = function () {
        dtVariacionFechas = tblVariacionFechas.DataTable({
            language: 'dtDicEsp',
            ordering: true,
            paging: true,
            searching: true,
            bFilter: false,
            info: false,
            pageLength: 25,
            order: [[1, 'asc']],
            columns: [
                {
                    title: 'Fecha : ', visible: false, render: (data, type, row) => {
                        let html = ` ${row.StationId}`;
                        return html;
                    }
                },
                {
                    title: 'Fecha', render: (data, type, row) => {
                        let html = ` <h4 Id="btnZona${row.StationId}"> <span class="spnZona" style="color:#05a692">${row.Fecha}</span></h4>`;

                        return html;
                    }
                },
                {
                    title: '87 Octanos', render: (data, type, row) => {
                        let magna = "0787"
                        let html = ` ${ObtenerRango2(row.lstZonasLimit, magna, row.StationId, row.Fecha)}`;
                        return html;
                    }
                },
                {
                    title: '91 Octanos', render: (data, type, row) => {
                        let premium = "0792"
                        let html = ` ${ObtenerRango2(row.lstZonasLimit, premium, row.StationId, row.Fecha)}`;
                        return html;
                    }
                },
                {
                    title: 'Diesel', render: (data, type, row) => {
                        let diesel = "03";
                        let html = ` ${ObtenerRango2(row.lstZonasLimit, diesel, row.StationId, row.Fecha)}`;
                        return html;
                    }
                },

            ],
            columnDefs: [
                { className: 'dt-center', 'targets': '_all' },
                { 'width': '25%', 'targets': [1, 2, 3, 4] }
            ],
            initComplete: function (settings, json) {

                tblVariacionFechas.on('click', '.magna', function () {
                    const rowData = dtVariacionFechas.row($(this).closest('tr')).data();
                    lblEstacion.text(rowData.NombreEstacion);
                    dtFechaSeleccionada.val(rowData.Fecha);
                    contenedorVariacionDesglose.css('display', 'block');
                    contenedorVariacionFechas.css('display', 'none');
                    inpVariacion.val(4);
                    tblVariacionDesglose.css('width', '100%');
                    lblProducto.text('87 Octanos')
                    let objDatos = {};

                    for (let a = 0; a < rowData.lstZonasLimit.length; a++) {
                        for (let b = 0; b < rowData.lstZonasLimit[a].length; b++) {
                            if (rowData.lstZonasLimit[a][b].Producto.split('-')[0].trim() == "0787") {
                                if (rowData.lstZonasLimit[a][b].IdEstacion == rowData.StationId) {
                                    if (rowData.lstZonasLimit[a][b].Fecha == rowData.Fecha) {
                                        objDatos = rowData.lstZonasLimit[a][b];
                                        break;
                                    }
                                }
                            }

                        }
                    }
                    initVariacionDesglose(objDatos);
                });
                tblVariacionFechas.on('click', '.premium', function () {
                    const rowData = dtVariacionFechas.row($(this).closest('tr')).data();
                    lblEstacion.text(rowData.NombreEstacion);
                    dtFechaSeleccionada.val(rowData.Fecha);
                    contenedorVariacionDesglose.css('display', 'block');
                    contenedorVariacionFechas.css('display', 'none');
                    inpVariacion.val(4);
                    tblVariacionDesglose.css('width', '100%');
                    lblProducto.text('91 Octanos')
                    let objDatos = {};

                    for (let a = 0; a < rowData.lstZonasLimit.length; a++) {
                        for (let b = 0; b < rowData.lstZonasLimit[a].length; b++) {
                            if (rowData.lstZonasLimit[a][b].Producto.split('-')[0].trim() == "0792" || rowData.lstZonasLimit[a][b].Producto.split('-')[0].trim() == "0791") {
                                if (rowData.lstZonasLimit[a][b].IdEstacion == rowData.StationId) {
                                    if (rowData.lstZonasLimit[a][b].Fecha == rowData.Fecha) {
                                        objDatos = rowData.lstZonasLimit[a][b];
                                        break;
                                    }
                                }
                            }

                        }
                    }
                    initVariacionDesglose(objDatos);
                });
                tblVariacionFechas.on('click', '.diesel', function () {
                    const rowData = dtVariacionFechas.row($(this).closest('tr')).data();
                    lblEstacion.text(rowData.NombreEstacion);
                    dtFechaSeleccionada.val(rowData.Fecha);
                    contenedorVariacionDesglose.css('display', 'block');
                    contenedorVariacionFechas.css('display', 'none');
                    inpVariacion.val(4);
                    tblVariacionDesglose.css('width', '100%');
                    lblProducto.text('Diesel')
                    let objDatos = {};

                    for (let a = 0; a < rowData.lstZonasLimit.length; a++) {
                        for (let b = 0; b < rowData.lstZonasLimit[a].length; b++) {
                            if (rowData.lstZonasLimit[a][b].Producto.split('-')[0].trim() == "03") {
                                if (rowData.lstZonasLimit[a][b].IdEstacion == rowData.StationId) {
                                    if (rowData.lstZonasLimit[a][b].Fecha == rowData.Fecha) {
                                        objDatos = rowData.lstZonasLimit[a][b];
                                        break;
                                    }
                                }
                            }

                        }
                    }
                    initVariacionDesglose(objDatos);
                });
            }
        });




        $('#tblVariacionFechas_length').css('margin-bottom', '5px');
        //$('#tblreportes_length').css('margin-left', '30px');
        $('#tblVariacionFechas_length').find('select').css('border', '1px solid #e5eaed');
        $('#tblVariacionFechas_length').find('select').css('border-radius', '.5rem');
        $('#tblVariacionFechas_length').find('select').css('padding', '.5rem .75rem');
        $('#tblVariacionFechas_length').find('select').css('color', '#5e6278');
        $('#tblVariacionFechas_length').find('select').css('margin-left', '10px');
        $('#tblVariacionFechas_length').find('select').css('margin-right', '10px');
        $('#tblVariacionFechas_length').find('select').css('outline', 'none');
        $('#tblVariacionFechas_length').find('label').css('color', '#05a692');
        $('#tblVariacionFechas_length').find('label').css('font-weight', '600');
        $('#tblVariacionFechas_length').find('label').css('font-size', '15px');
        $('#tblVariacionFechas_paginate').css('text-align', 'right');
        $('#tblVariacionFechas_paginate').css('min-height', '30px');

        $('#tblVariacionFechas_filter').find('label').css('color', '#05a692');
        $('#tblVariacionFechas_filter').find('label').css('font-weight', '900');
        $('#tblVariacionFechas_filter').find('label').css('font-size', '20px');
        $('#tblVariacionFechas_filter').find('label').css('order', '2');
        $('#tblVariacionFechas_filter').find('input').css('margin-left', '5px');
        $('#tblVariacionFechas_filter').find('input').css('border', '1px solid #e5eaed');
        $('#tblVariacionFechas_filter').find('input').css('border-radius', '.5rem');
        $('#tblVariacionFechas_filter').find('input').css('padding', '.5rem .75rem');
        $('#tblVariacionFechas_filter').find('input').css('outline', 'none');
        $('#tblVariacionFechas_filter').find('input').css('color', '#5e6278');
        $('#tblVariacionFechas_filter').find('input').css('font-size', '15px');
        $('#tblVariacionFechas_filter').css('text-align', 'right');
    }


    const ObtenerRango2 = function (lstZonasLimit, producto, StationId, Fecha) {
        let variableRetorno = "";

        switch (producto) {
            case "0787":
                if (lstZonasLimit != null) {
                    for (let i = 0; i < lstZonasLimit.length; i++) {
                        if (lstZonasLimit[i].length > 0) {
                            for (let b = 0; b < lstZonasLimit[i].length; b++) {
                                if (lstZonasLimit[i][b].Fecha == Fecha) {
                                    if (lstZonasLimit[i][b].IdEstacion == StationId) {
                                        if (lstZonasLimit[i][b].Producto.split('-')[0].trim() == "0787") {
                                            if (lstZonasLimit[i][b].LimiteNivel == 7 || lstZonasLimit[i][b].LimiteNivel == 6) {
                                                variableRetorno = `
                                                                    <button type="button" class="btn estado4 magna" style="width:100%">
                                                                        ${lstZonasLimit[i][b].PorcDifAjustado + '%'}
                                                                    </button>`;
                                                return variableRetorno;
                                            } else if (lstZonasLimit[i][b].LimiteNivel == 5 || lstZonasLimit[i][b].LimiteNivel == 4) {
                                                variableRetorno = `
                                                                    <button type="button" class="btn estado3 magna" style="width:100%">
                                                                        ${lstZonasLimit[i][b].PorcDifAjustado + '%'}
                                                                    </button>`;
                                                return variableRetorno;
                                            } else if (lstZonasLimit[i][b].LimiteNivel == 3 || lstZonasLimit[i][b].LimiteNivel == 2) {
                                                variableRetorno = `
                                                                    <button type="button" class="btn estado2 magna" style="width:100%">
                                                                        ${lstZonasLimit[i][b].PorcDifAjustado + '%'}
                                                                    </button>`;
                                                return variableRetorno;
                                            } else if (lstZonasLimit[i][b].LimiteNivel == 1) {
                                                variableRetorno = `
                                                                    <button type="button" class="btn estado1 magna" style="width:100%">
                                                                        ${lstZonasLimit[i][b].PorcDifAjustado + '%'}
                                                                    </button>`;

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            case "0792":
            case "0791":

                if (lstZonasLimit != null) {
                    for (let i = 0; i < lstZonasLimit.length; i++) {
                        if (lstZonasLimit[i].length > 0) {
                            for (let b = 0; b < lstZonasLimit[i].length; b++) {
                                if (lstZonasLimit[i][b].Fecha == Fecha) {
                                    if (lstZonasLimit[i][b].IdEstacion == StationId) {
                                        if (lstZonasLimit[i][b].Producto.split('-')[0].trim() == "0792" || lstZonasLimit[i][b].Producto.split('-')[0].trim() == "0791") {
                                            if (lstZonasLimit[i][b].LimiteNivel == 7 || lstZonasLimit[i][b].LimiteNivel == 6) {
                                                variableRetorno = `
                                                                    <button type="button" class="btn estado4 premium" style="width:100%">
                                                                        ${lstZonasLimit[i][b].PorcDifAjustado + '%'}
                                                                    </button>`;
                                                return variableRetorno;
                                            } else if (lstZonasLimit[i][b].LimiteNivel == 5 || lstZonasLimit[i][b].LimiteNivel == 4) {
                                                variableRetorno = `
                                                                    <button type="button" class="btn estado3 premium" style="width:100%">
                                                                        ${lstZonasLimit[i][b].PorcDifAjustado + '%'}
                                                                    </button>`;
                                                return variableRetorno;
                                            } else if (lstZonasLimit[i][b].LimiteNivel == 3 || lstZonasLimit[i][b].LimiteNivel == 2) {
                                                variableRetorno = `
                                                                    <button type="button" class="btn estado2 premium" style="width:100%">
                                                                        ${lstZonasLimit[i][b].PorcDifAjustado + '%'}
                                                                    </button>`;
                                                return variableRetorno;
                                            } else if (lstZonasLimit[i][b].LimiteNivel == 1) {
                                                variableRetorno = `
                                                                    <button type="button" class="btn estado1 premium" style="width:100%">
                                                                        ${lstZonasLimit[i][b].PorcDifAjustado + '%'}
                                                                    </button>`;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            case "03":
                if (lstZonasLimit != null) {
                    for (let i = 0; i < lstZonasLimit.length; i++) {
                        if (lstZonasLimit[i].length > 0) {
                            for (let b = 0; b < lstZonasLimit[i].length; b++) {
                                if (lstZonasLimit[i][b].Fecha == Fecha) {
                                    if (lstZonasLimit[i][b].IdEstacion == StationId) {
                                        if (lstZonasLimit[i][b].Producto.split('-')[0].trim() == "03") {
                                            if (lstZonasLimit[i][b].LimiteNivel == 7 || lstZonasLimit[i][b].LimiteNivel == 6) {
                                                variableRetorno = `
                                                                    <button type="button" class="btn estado4 diesel" style="width:100%">
                                                                        ${lstZonasLimit[i][b].PorcDifAjustado + '%'}
                                                                    </button>`;
                                                return variableRetorno;
                                            } else if (lstZonasLimit[i][b].LimiteNivel == 5 || lstZonasLimit[i][b].LimiteNivel == 4) {
                                                variableRetorno = `
                                                                    <button type="button" class="btn estado3 diesel" style="width:100%">
                                                                        ${lstZonasLimit[i][b].PorcDifAjustado + '%'}
                                                                    </button>`;
                                                return variableRetorno;
                                            } else if (lstZonasLimit[i][b].LimiteNivel == 3 || lstZonasLimit[i][b].LimiteNivel == 2) {
                                                variableRetorno = `
                                                                    <button type="button" class="btn estado2 diesel" style="width:100%">
                                                                        ${lstZonasLimit[i][b].PorcDifAjustado + '%'}
                                                                    </button>`;
                                                return variableRetorno;
                                            } else if (lstZonasLimit[i][b].LimiteNivel == 1) {
                                                variableRetorno = `
                                                                    <button type="button" class="btn estado1 diesel" style="width:100%">
                                                                        ${lstZonasLimit[i][b].PorcDifAjustado + '%'}
                                                                    </button>`;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break;

        }



        return variableRetorno;
    }


    var initVariacionDesglose = function (objDatos) {
        tblVariacionDesglose.find('tbody').remove();
        let html = `
        <tbody>
        <tr>
            <td style="padding: 15px !important;">Inventario Inicial</td>
            <td style="text-align: center">${objDatos.InvInicial}</td>

        </tr>
        <tr>
            <td style="padding: 15px !important;">(+) Recepciones del día</td>
            <td style="text-align: center">${objDatos.Compras}</td>

        </tr>
        <tr>
            <td style="padding: 15px !important;">(-) Ventas Contables</td>
            <td style="text-align: center">${objDatos.VentasConAjuste}</td>

        </tr>
        <tr>
            <td style="padding: 15px !important;">(=) Inventario Final</td>
            <td style="text-align: center">${objDatos.InvFInalAjustado}</td>

        </tr>
        <tr>
            <td style="padding: 15px !important;">(vs) Lectura de Inv. Físico</td>
            <td style="text-align: center">${objDatos.LecturaFinal}</td>

        </tr>
        <tr>
            <td style="padding: 15px !important;">(=) Variación en Litros</td>
            <td style="text-align: center">${objDatos.Pendiente}</td>

        </tr>
        <tr>
            <td style="padding: 15px !important;">% Variación</td>
            <td style="text-align: center">
                    ${funcOption(objDatos)}
            </td>

        </tr>
    </tbody>
        `;
        tblVariacionDesglose.append(html);
    }
    //const funcOption = function (objDatos) {
    //    let html = "";
    //    if (objDatos.LimiteNivel == 7 || objDatos.LimiteNivel == 6) {
    //        html = `
    //        <button type="button" class="btn estado4">
    //            ${(objDatos.DifTeoricoVsLectura / objDatos.Ventas).toFixed(3) + '%'}
    //        </button>`;
    //    } else if (objDatos.LimiteNivel == 5 || objDatos.LimiteNivel == 4) {
    //        html = `
    //        <button type="button" class="btn estado3">
    //            ${(objDatos.DifTeoricoVsLectura / objDatos.Ventas).toFixed(3) + '%'}
    //        </button>`;
    //    } else if (objDatos.LimiteNivel == 3 || objDatos.LimiteNivel == 2) {
    //        html = `
    //        <button type="button" class="btn estado2">
    //            ${(objDatos.DifTeoricoVsLectura / objDatos.Ventas).toFixed(3) + '%'}
    //        </button>`;
    //    } else if (objDatos.LimiteNivel == 1) {
    //        html = `
    //        <button type="button" class="btn estado1">
    //            ${(objDatos.DifTeoricoVsLectura / objDatos.Ventas).toFixed(3) + '%'}
    //        </button>`;
    //    }
    //    return html;
    //}
    const funcOption = function (objDatos) {
        let html = "";
        if (objDatos.LimiteNivel == 7 || objDatos.LimiteNivel == 6) {
            html = `
            <button type="button" class="btn estado4">
                ${objDatos.PorcDifAjustado.toFixed(3) + '%'}
            </button>`;
        } else if (objDatos.LimiteNivel == 5 || objDatos.LimiteNivel == 4) {
            html = `
            <button type="button" class="btn estado3">
                ${objDatos.PorcDifAjustado.toFixed(3) + '%'}
            </button>`;
        } else if (objDatos.LimiteNivel == 3 || objDatos.LimiteNivel == 2) {
            html = `
            <button type="button" class="btn estado2">
                ${objDatos.PorcDifAjustado.toFixed(3) + '%'}
            </button>`;
        } else if (objDatos.LimiteNivel == 1) {
            html = `
            <button type="button" class="btn estado1">
                ${objDatos.PorcDifAjustado.toFixed(3) + '%'}
            </button>`;
        }
        return html;
    }



    return {
        Inicializar: Inicializar,
    }
};