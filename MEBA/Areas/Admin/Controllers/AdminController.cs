﻿using MEBA.Clases;
using MEBA.Models;
using MEBA.Models.ModAjustePerfecto;
using MEBA.Models.ModEmpresas;
using MEBA.Models.ModGenerales;
using MEBA.Models.ModUsuarios;
using MEBA.Models.ModZonas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MEBA.Areas.Admin.Controllers
{
    public class AdminController : Controller
    {
        MEBAEntities db = new Models.MEBAEntities();
        // GET: Admin/Admin
        public ActionResult Index()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult Permisos()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                if (vSessiones.sesionUsuarioDTO.idRol == 1 || vSessiones.sesionUsuarioDTO.idRol == 2)
                {
                    return View();
                }
                else
                {
                    return Redirect("/Login/Login");
                }
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }


        public ActionResult AsignarMenu()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                if (vSessiones.sesionUsuarioDTO.idRol == 1 || vSessiones.sesionUsuarioDTO.idRol == 2)
                {
                    return View();
                }
                else
                {
                    return Redirect("/Login/Login");
                }
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult Usuarios()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                if (vSessiones.sesionUsuarioDTO.idRol == 0 || vSessiones.sesionUsuarioDTO.idRol == 1 || vSessiones.sesionUsuarioDTO.idRol == 2)
                {
                    using (MEBAEntities oDB2 = new MEBAEntities())
                    {
                        MEBAProEmpresas objEmpresas = oDB2.MEBAProEmpresas.Where(r => r.GuidEmpresa == vSessiones.sesionUsuarioDTO.GuidEmpresa).FirstOrDefault();
                        MEBABitacoraAcc objBitacora = new MEBABitacoraAcc();

                        objBitacora.Fecha = DateTime.Now;
                        objBitacora.Tipo = "Operativo";
                        objBitacora.Origen = vSessiones.sessionUsuarioCONFIGDTO.SitioWEB;
                        objBitacora.Usuario = vSessiones.sesionUsuarioDTO.UserName;
                        objBitacora.Grupo = objEmpresas.NombreComercial;
                        objBitacora.Zona = "";
                        objBitacora.Estacion = "";
                        objBitacora.Descripcion = "Acceso al  catalogo de Usuarios";
                        objBitacora.IdAsociado = vSessiones.sessionUsuarioCONFIGDTO.IDAsociado;
                        var FechaAnterior = oDB2.spdObtenerFechaAnterior("0", vSessiones.sessionUsuarioEmpresa.NombreComercial).FirstOrDefault();
                        if (FechaAnterior != null)
                        {
                            objBitacora.FechaAnterior = FechaAnterior;
                        }

                        oDB2.MEBABitacoraAcc.Add(objBitacora);
                        oDB2.SaveChanges();
                    }
                    return View();
                }
                else
                {
                    return Redirect("/Login/Login");
                }
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult Grupos()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                if (vSessiones.sesionUsuarioDTO.idRol == 0 || vSessiones.sesionUsuarioDTO.idRol == 1 || vSessiones.sesionUsuarioDTO.idRol == 2)
                {
                    using (MEBAEntities oDB2 = new MEBAEntities())
                    {
                        MEBAProEmpresas objEmpresas = oDB2.MEBAProEmpresas.Where(r => r.GuidEmpresa == vSessiones.sesionUsuarioDTO.GuidEmpresa).FirstOrDefault();
                        MEBABitacoraAcc objBitacora = new MEBABitacoraAcc();

                        objBitacora.Fecha = DateTime.Now;
                        objBitacora.Tipo = "Operativo";
                        objBitacora.Origen = vSessiones.sessionUsuarioCONFIGDTO.SitioWEB;
                        objBitacora.Usuario = vSessiones.sesionUsuarioDTO.UserName;
                        objBitacora.Grupo = objEmpresas.NombreComercial;
                        objBitacora.Zona = "";
                        objBitacora.Estacion = "";
                        objBitacora.Descripcion = "Acceso al  catalogo de grupos";
                        objBitacora.IdAsociado = vSessiones.sessionUsuarioCONFIGDTO.IDAsociado;
                        var FechaAnterior = oDB2.spdObtenerFechaAnterior("0", vSessiones.sessionUsuarioEmpresa.NombreComercial).FirstOrDefault();
                        if (FechaAnterior != null)
                        {
                            objBitacora.FechaAnterior = FechaAnterior;
                        }


                        oDB2.SaveChanges();
                    }
                    return View();
                }
                else
                {
                    return Redirect("/Login/Login");
                }
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult Bitacora()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                if (vSessiones.sesionUsuarioDTO.idRol == 0 || vSessiones.sesionUsuarioDTO.idRol == 1 || vSessiones.sesionUsuarioDTO.idRol == 2 || vSessiones.sesionUsuarioDTO.idRol == 3)
                {
                    return View();
                }
                else
                {
                    return Redirect("/Login/Login");
                }
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult Zonas()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                if (vSessiones.sesionUsuarioDTO.idRol == 1 || vSessiones.sesionUsuarioDTO.idRol == 2)
                {
                    return View();
                }
                else
                {
                    return Redirect("/Login/Login");
                }
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult AsignarEstaciones()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                if (vSessiones.sesionUsuarioDTO.idRol == 1 || vSessiones.sesionUsuarioDTO.idRol == 2)
                {
                    return View();
                }
                else
                {
                    return Redirect("/Login/Login");
                }
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }
        public ActionResult AsignarCapturadoAEstaciones()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                if (vSessiones.sesionUsuarioDTO.idRol == 1 || vSessiones.sesionUsuarioDTO.idRol == 2)
                {
                    return View();
                }
                else
                {
                    return Redirect("/Login/Login");
                }
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }


        public ActionResult ConfiguracionMermasMangueras()
        {
            if (vSessiones.sesionUsuarioDTO != null)
            {
                ViewBag.Nombre = vSessiones.sesionUsuarioDTO.Nombre + " " + vSessiones.sesionUsuarioDTO.ApellidoPaterno + " " + vSessiones.sesionUsuarioDTO.ApellidoMaterno;
                ViewBag.Puesto = vSessiones.sessionUsuarioEmpresa.NombreComercial;
                if (vSessiones.sesionUsuarioDTO.idRol == 1 || vSessiones.sesionUsuarioDTO.idRol == 2)
                {
                    return View();
                }
                else
                {
                    return Redirect("/Login/Login");
                }
            }
            else
            {
                return Redirect("/Login/Login");
            }
        }





        #region USUARIOS

        public JsonResult GetListadoUsuarios()
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {
                List<ClsModUsuario> lstDatosUsuarios = new List<ClsModUsuario>();
                if (vSessiones.sesionUsuarioDTO.idRol == 0)
                {
                    lstDatosUsuarios = db.MEBAProEmpresasUsuarios.ToList().Select(y => new ClsModUsuario
                    {
                        IdUsuario = y.IdUsuario,
                        GuidUsuario = y.GuidUsuario,
                        GuidEmpresa = y.GuidEmpresa,
                        GuidSucursal = y.GuidSucursal,
                        UserName = y.UserName,
                        Password = y.Password,
                        Nombre = y.Nombre,
                        ApellidoPaterno = y.ApellidoPaterno,
                        ApellidoMaterno = y.ApellidoMaterno,
                        Correo = y.Correo,
                        Telefono = y.Telefono,
                        FechaNacimiento = y.FechaNacimiento,
                        RFC = y.RFC,
                        CURP = y.CURP,
                        CodigoPostal = y.CodigoPostal,
                        Nacionalidad = y.Nacionalidad,
                        Estatus = y.Estatus,
                        idRol = y.idRol,
                        FechaCreacion = y.FechaCreacion,
                        FechaUltimaModificacion = y.FechaUltimaModificacion,
                        Foto = y.Foto,
                        Titulo = y.Titulo,

                        objEmpresa = db.MEBAProEmpresas.Where(r => r.GuidEmpresa == y.GuidEmpresa).FirstOrDefault(),
                        objRol = db.MEBARolUsuario.Where(r => r.idRol == y.idRol).FirstOrDefault(),
                        lstIdZonas = db.MEBARelUsuarioZonas.Where(r => r.IdUsuario == y.IdUsuario).Select(n => n.IdZona).ToList()
                    }).ToList();
                }
                else if (vSessiones.sesionUsuarioDTO.idRol == 1)
                {
                    lstDatosUsuarios = db.MEBAProEmpresasUsuarios.ToList().Select(y => new ClsModUsuario
                    {
                        IdUsuario = y.IdUsuario,
                        GuidUsuario = y.GuidUsuario,
                        GuidEmpresa = y.GuidEmpresa,
                        GuidSucursal = y.GuidSucursal,
                        UserName = y.UserName,
                        Password = y.Password,
                        Nombre = y.Nombre,
                        ApellidoPaterno = y.ApellidoPaterno,
                        ApellidoMaterno = y.ApellidoMaterno,
                        Correo = y.Correo,
                        Telefono = y.Telefono,
                        FechaNacimiento = y.FechaNacimiento,
                        RFC = y.RFC,
                        CURP = y.CURP,
                        CodigoPostal = y.CodigoPostal,
                        Nacionalidad = y.Nacionalidad,
                        Estatus = y.Estatus,
                        idRol = y.idRol,
                        FechaCreacion = y.FechaCreacion,
                        FechaUltimaModificacion = y.FechaUltimaModificacion,
                        Foto = y.Foto,
                        Titulo = y.Titulo,

                        objEmpresa = db.MEBAProEmpresas.Where(r => r.GuidEmpresa == y.GuidEmpresa).FirstOrDefault(),
                        objRol = db.MEBARolUsuario.Where(r => r.idRol == y.idRol).FirstOrDefault(),
                        lstIdZonas = db.MEBARelUsuarioZonas.Where(r => r.IdUsuario == y.IdUsuario).Select(n => n.IdZona).ToList()
                    }).ToList().Where(r => r.idRol >= vSessiones.sesionUsuarioDTO.idRol && r.GuidEmpresa == vSessiones.sesionUsuarioDTO.GuidEmpresa).ToList();
                }
                else if (vSessiones.sesionUsuarioDTO.idRol == 2)
                {
                    lstDatosUsuarios = db.MEBAProEmpresasUsuarios.Where(r => r.GuidEmpresa == vSessiones.sesionUsuarioDTO.GuidEmpresa && r.Estatus == true && r.idRol != 1).ToList().Select(y => new ClsModUsuario
                    {
                        IdUsuario = y.IdUsuario,
                        GuidUsuario = y.GuidUsuario,
                        GuidEmpresa = y.GuidEmpresa,
                        GuidSucursal = y.GuidSucursal,
                        UserName = y.UserName,
                        Password = y.Password,
                        Nombre = y.Nombre,
                        ApellidoPaterno = y.ApellidoPaterno,
                        ApellidoMaterno = y.ApellidoMaterno,
                        Correo = y.Correo,
                        Telefono = y.Telefono,
                        FechaNacimiento = y.FechaNacimiento,
                        RFC = y.RFC,
                        CURP = y.CURP,
                        CodigoPostal = y.CodigoPostal,
                        Nacionalidad = y.Nacionalidad,
                        Estatus = y.Estatus,
                        idRol = y.idRol,
                        FechaCreacion = y.FechaCreacion,
                        FechaUltimaModificacion = y.FechaUltimaModificacion,
                        Foto = y.Foto,
                        Titulo = y.Titulo,
                        objEmpresa = db.MEBAProEmpresas.Where(r => r.GuidEmpresa == y.GuidEmpresa).FirstOrDefault(),
                        objRol = db.MEBARolUsuario.Where(r => r.idRol == y.idRol).FirstOrDefault(),
                        lstIdZonas = db.MEBARelUsuarioZonas.Where(r => r.IdUsuario == y.IdUsuario).Select(n => n.IdZona).ToList()
                    }).ToList();
                }
                else
                {
                    lstDatosUsuarios = db.MEBAProEmpresasUsuarios.Where(r => r.GuidEmpresa == vSessiones.sesionUsuarioDTO.GuidEmpresa && r.Estatus == true && r.idRol != 2 && r.idRol != 1).ToList().Select(y => new ClsModUsuario
                    {
                        IdUsuario = y.IdUsuario,
                        GuidUsuario = y.GuidUsuario,
                        GuidEmpresa = y.GuidEmpresa,
                        GuidSucursal = y.GuidSucursal,
                        UserName = y.UserName,
                        Password = y.Password,
                        Nombre = y.Nombre,
                        ApellidoPaterno = y.ApellidoPaterno,
                        ApellidoMaterno = y.ApellidoMaterno,
                        Correo = y.Correo,
                        Telefono = y.Telefono,
                        FechaNacimiento = y.FechaNacimiento,
                        RFC = y.RFC,
                        CURP = y.CURP,
                        CodigoPostal = y.CodigoPostal,
                        Nacionalidad = y.Nacionalidad,
                        Estatus = y.Estatus,
                        idRol = y.idRol,
                        FechaCreacion = y.FechaCreacion,
                        FechaUltimaModificacion = y.FechaUltimaModificacion,
                        Foto = y.Foto,
                        Titulo = y.Titulo,
                        objEmpresa = db.MEBAProEmpresas.Where(r => r.GuidEmpresa == y.GuidEmpresa).FirstOrDefault(),
                        objRol = db.MEBARolUsuario.Where(r => r.idRol == y.idRol).FirstOrDefault(),
                        lstIdZonas = db.MEBARelUsuarioZonas.Where(r => r.IdUsuario == y.IdUsuario).Select(n => n.IdZona).ToList()
                    }).ToList();
                }


                objResultado.ITEMS = lstDatosUsuarios;
                objResultado.SUCCESS = true;
                objResultado.MENSAJE = "";
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListadoRol()
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {
                List<ClsModCombos> lstDatosRol = new List<ClsModCombos>();
                if (vSessiones.sesionUsuarioDTO.idRol == 0)
                {
                    lstDatosRol = db.MEBARolUsuario.ToList().Select(y => new ClsModCombos
                    {
                        value = y.idRol.ToString(),
                        text = y.Descripcion,
                    }).ToList();
                }else if (vSessiones.sesionUsuarioDTO.idRol == 1)
                {
                    lstDatosRol = db.MEBARolUsuario.ToList().Select(y => new ClsModCombos
                    {
                        value = y.idRol.ToString(),
                        text = y.Descripcion,
                    }).ToList();
                }
                else if (vSessiones.sesionUsuarioDTO.idRol == 2)
                {

                    lstDatosRol = db.MEBARolUsuario.Where(r => (r.idRol != 1)).ToList().Select(y => new ClsModCombos
                    {
                        value = y.idRol.ToString(),
                        text = y.Descripcion,
                    }).ToList();
                }
                else if (vSessiones.sesionUsuarioDTO.idRol == 3)
                {

                    lstDatosRol = db.MEBARolUsuario.Where(r => (r.idRol != 1 && r.idRol != 2 && r.idRol != 3)).ToList().Select(y => new ClsModCombos
                    {
                        value = y.idRol.ToString(),
                        text = y.Descripcion,
                    }).ToList();
                }

                objResultado.ITEMS = lstDatosRol;
                objResultado.SUCCESS = true;
                objResultado.MENSAJE = "";
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListadoRolContatos()
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {
                List<ClsModCombos> lstDatosRol = new List<ClsModCombos>();

                lstDatosRol = db.MEBARolUsuario.Where(r => r.idRol == 2 || r.idRol == 3).ToList().Select(y => new ClsModCombos
                {
                    value = y.idRol.ToString(),
                    text = y.Descripcion,
                }).ToList();


                objResultado.ITEMS = lstDatosRol;
                objResultado.SUCCESS = true;
                objResultado.MENSAJE = "";
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetListadoEmpresas()
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {
                List<ClsModCombos> lstDatosEmpresas = new List<ClsModCombos>();
                if (vSessiones.sesionUsuarioDTO.idRol == 0)
                {
                    lstDatosEmpresas = db.MEBAProEmpresas.ToList().Select(y => new ClsModCombos
                    {
                        value = y.GuidEmpresa,
                        text = y.NombreComercial
                    }).ToList();
                }
                else
                {
                    lstDatosEmpresas = db.MEBAProEmpresas.Where(r => r.GuidEmpresa == vSessiones.sesionUsuarioDTO.GuidEmpresa).ToList().Select(y => new ClsModCombos
                    {
                        value = y.GuidEmpresa,
                        text = y.NombreComercial
                    }).ToList();
                }


                objResultado.ITEMS = lstDatosEmpresas;
                objResultado.SUCCESS = true;
                objResultado.MENSAJE = "";
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult postAgregarModificarEliminar(ParametrosUsuario parametros)
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {
                MEBAProEmpresasUsuarios objUsuario = new MEBAProEmpresasUsuarios();
                string resultado = "";
                switch (parametros.Accion)
                {
                    case "A":
                        objUsuario = db.MEBAProEmpresasUsuarios.Where(r => r.UserName == parametros.Usuario).FirstOrDefault();
                        if (objUsuario == null)
                        {
                            objUsuario = new MEBAProEmpresasUsuarios();
                            string _Guid = Guid.NewGuid().ToString("N");
                            objUsuario.Nombre = parametros.Nombre;
                            objUsuario.ApellidoPaterno = parametros.ApeidoP;
                            objUsuario.ApellidoMaterno = parametros.ApeidoM;
                            objUsuario.Correo = parametros.Correo;
                            objUsuario.Telefono = parametros.Telefono;
                            objUsuario.UserName = parametros.Usuario;
                            objUsuario.Password = Funciones.EncriptaClave(parametros.Contrasena);
                            objUsuario.GuidUsuario = _Guid;
                            objUsuario.GuidEmpresa = parametros.Grupo;
                            objUsuario.Estatus = true;
                            objUsuario.idRol = parametros.Rol;
                            db.MEBAProEmpresasUsuarios.Add(objUsuario);
                            db.SaveChanges();

                            //foreach (var item in parametros.lstIdZonas)
                            //{
                            //    MEBARelUsuarioZonas objMEBARelUsuarioZonas = new MEBARelUsuarioZonas();
                            //    objMEBARelUsuarioZonas.IdUsuario = objUsuario.IdUsuario;
                            //    objMEBARelUsuarioZonas.IdZona = item;

                            //    db.MEBARelUsuarioZonas.Add(objMEBARelUsuarioZonas);
                            //    db.SaveChanges();
                            //}


                            resultado = "usuario agregado con exito.";
                            objResultado.ITEMS = resultado;
                            objResultado.SUCCESS = true;
                            objResultado.MENSAJE = "";
                        }
                        else
                        {
                            resultado = "este usuario ya se encuentra registrado en la base de datos.";
                            objResultado.ITEMS = resultado;
                            objResultado.SUCCESS = false;
                            objResultado.MENSAJE = "";
                            return Json(objResultado, JsonRequestBehavior.AllowGet);
                        }

                        break;
                    case "M":

                        objUsuario = db.MEBAProEmpresasUsuarios.Where(r => r.IdUsuario == parametros.IdUsuario).FirstOrDefault();
                        if (objUsuario != null)
                        {
                            var objExiste = db.MEBAProEmpresasUsuarios.Where(r => r.IdUsuario != parametros.IdUsuario && r.UserName == parametros.Usuario).FirstOrDefault();
                            if (objExiste == null)
                            {
                                string _Guid = Guid.NewGuid().ToString("N");
                                objUsuario.Nombre = parametros.Nombre;
                                objUsuario.ApellidoPaterno = parametros.ApeidoP;
                                objUsuario.ApellidoMaterno = parametros.ApeidoM;
                                objUsuario.Correo = parametros.Correo;
                                objUsuario.Telefono = parametros.Telefono;
                                objUsuario.UserName = parametros.Usuario;
                                objUsuario.Estatus = true;
                                if (objUsuario.Password != parametros.Contrasena)
                                {
                                    objUsuario.Password = Funciones.EncriptaClave(parametros.Contrasena);
                                }
                                objUsuario.GuidUsuario = _Guid;
                                objUsuario.GuidEmpresa = parametros.Grupo;
                                objUsuario.idRol = parametros.Rol;
                                db.SaveChanges();


                                var obtenerLstZonas = db.MEBARelUsuarioZonas.Where(r => r.IdUsuario == parametros.IdUsuario).ToList();
                                db.MEBARelUsuarioZonas.RemoveRange(obtenerLstZonas);
                                db.SaveChanges();

                                //if (parametros.lstIdZonas != null)
                                //{
                                //    foreach (var item in parametros.lstIdZonas)
                                //    {
                                //        MEBARelUsuarioZonas objMEBARelUsuarioZonas = new MEBARelUsuarioZonas();
                                //        objMEBARelUsuarioZonas.IdUsuario = parametros.IdUsuario;
                                //        objMEBARelUsuarioZonas.IdZona = item;

                                //        db.MEBARelUsuarioZonas.Add(objMEBARelUsuarioZonas);
                                //        db.SaveChanges();
                                //    }
                                //}



                                resultado = "usuario editado con exito.";
                                objResultado.ITEMS = resultado;
                                objResultado.SUCCESS = true;
                                objResultado.MENSAJE = "";
                            }
                            else
                            {
                                resultado = "este usuario ya se encuentra registrado en la base de datos.";
                            }
                        }
                        else
                        {
                            resultado = "este usuario ya se encuentra registrado en la base de datos.";
                            objResultado.ITEMS = resultado;
                            objResultado.SUCCESS = false;
                            objResultado.MENSAJE = "";
                            return Json(objResultado, JsonRequestBehavior.AllowGet);

                        }

                        break;
                    case "B":
                        objUsuario = db.MEBAProEmpresasUsuarios.Where(r => r.IdUsuario == parametros.IdUsuario).FirstOrDefault();
                        if (objUsuario != null)
                        {
                            objUsuario.Estatus = parametros.Estatus;
                            db.SaveChanges();
                            resultado = "usuario eliminado con exito.";
                            objResultado.ITEMS = resultado;
                            objResultado.SUCCESS = true;
                            objResultado.MENSAJE = "";
                        }
                        break;
                }
                objResultado.ITEMS = resultado;
                objResultado.SUCCESS = true;
                objResultado.MENSAJE = "";
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult cboZonas()
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {
                var lstDatosUsuarios = db.MEBAZonas.Where(r => r.IdEmpresa == vSessiones.sessionUsuarioEmpresa.IDEmpresa).ToList().Select(y => new
                {
                    value = y.IdZona,
                    text = y.Descripcion
                }).ToList();


                objResultado.ITEMS = lstDatosUsuarios;
                objResultado.SUCCESS = true;
                objResultado.MENSAJE = "";
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region GRUPOS

        public JsonResult GetListadoGrupo()
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {
                List<ClsModEmpresas> lstDatosUsuarios = new List<ClsModEmpresas>();
                if (vSessiones.sesionUsuarioDTO.idRol == 1 || vSessiones.sesionUsuarioDTO.idRol == 0)
                {
                    lstDatosUsuarios = db.MEBAProEmpresas.ToList().Select(y => new ClsModEmpresas
                    {
                        objEmpresa = db.MEBAProEmpresas.Where(r => r.GuidEmpresa == y.GuidEmpresa).FirstOrDefault(),
                        objAsociados = db.MEBAAsociados.Where(r => r.IDAsociado == y.IDAsociado).FirstOrDefault(),
                        objAsociadosConfig = db.MEBAAsociadosConfig.Where(r => r.IDAsociado == y.IDAsociado).FirstOrDefault(),
                        objEstados = db.MEBAGenEstados.Where(r => r.IdEstado == y.Estado).FirstOrDefault(),
                        objMunicipios = db.MEBAGenMunicipios.Where(r => r.IdMunicipio == y.Municipio).FirstOrDefault(),
                    }).ToList();

                    objResultado.ITEMS = lstDatosUsuarios;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }
                else
                {

                    objResultado.ITEMS = null;
                    objResultado.SUCCESS = false;
                    objResultado.MENSAJE = "No tienes permisos para acceder a esta vista.";
                }

            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult postAgregarModificarEliminarGrupos(ParametrosEmpresas parametros)
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {

                if (vSessiones.sesionUsuarioDTO.idRol == 1)
                {

                    MEBAProEmpresas objUsuario = new MEBAProEmpresas();
                    string resultado = "";
                    switch (parametros.Accion)
                    {
                        case "A":
                            objUsuario = db.MEBAProEmpresas.Where(r => r.NombreComercial == parametros.NombreComercial).FirstOrDefault();
                            if (objUsuario == null)
                            {
                                objUsuario = new MEBAProEmpresas();
                                string _GuidEmpresa = Guid.NewGuid().ToString("N");
                                string _GuidAsociado = Guid.NewGuid().ToString("N");

                                MEBAAsociados objAociado = new MEBAAsociados();
                                MEBAAsociadosConfig objAociadoConfig = new MEBAAsociadosConfig();
                                MEBAProEmpresas objEmpresas = new MEBAProEmpresas();

                                objAociado.GUIDAsocuado = _GuidAsociado;
                                objAociado.NombreComercial = parametros.NombreComercial;
                                objAociado.Telefono = parametros.Telefono;
                                objAociado.Calle = parametros.Calle;
                                objAociado.CP = parametros.CP;
                                objAociado.Pais = parametros.Pais;
                                objAociado.Estado = parametros.Estado;
                                objAociado.Municipio = parametros.Municipio;
                                objAociado.ReprocesarEstados = parametros.ReprocesarEstados == null ? false : true;
                                //objAociado.MostrarTiempo = parametros.MostrarTiempo;
                                //objAociado.MostrarTurno = parametros.MostrarTurno;
                                //objAociado.MostrarXml = parametros.MostrarXml;
                                //objAociado.MostrarInventarios = parametros.MostrarInventarios;

                                db.MEBAAsociados.Add(objAociado);
                                db.SaveChanges();

                                var objAsociado = db.MEBAAsociados.OrderByDescending(r => r.IDAsociado).FirstOrDefault();

                                if (parametros.TipoConexion.ToUpper() == "1")
                                {
                                    objAociadoConfig.IDAsociado = objAociado.IDAsociado;
                                    objAociadoConfig.SitioWEB = "local";
                                    objAociadoConfig.UsuarioSQLSeguridad = parametros.UsuarioSQLSeguridad;
                                    objAociadoConfig.PasswordSQLSeguridad = Funciones.EncriptaClaveSQL(parametros.PasswordSQLSeguridad);
                                    objAociadoConfig.BaseProduccion = parametros.BaseProduccion;
                                    objAociadoConfig.ServerSeguridad = parametros.ServerSeguridad;
                                    db.MEBAAsociadosConfig.Add(objAociadoConfig);
                                    db.SaveChanges();
                                }
                                else
                                {
                                    objAociadoConfig.SitioWEB = parametros.SitioWEB;
                                    objAociadoConfig.IDAsociado = objAociado.IDAsociado;
                                    objAociadoConfig.urlApi = parametros.urlApi;
                                    objAociadoConfig.usuarioApi = parametros.usuarioApi;
                                    objAociadoConfig.passwordApi = parametros.passwordApi;
                                    db.MEBAAsociadosConfig.Add(objAociadoConfig);
                                    db.SaveChanges();
                                }


                                objEmpresas.GuidEmpresa = _GuidEmpresa;
                                objEmpresas.IDAsociado = objAsociado.IDAsociado;
                                objEmpresas.NombreComercial = parametros.NombreComercial;
                                objEmpresas.Telefono = parametros.Telefono;
                                objEmpresas.Calle = parametros.Calle;
                                objEmpresas.CP = parametros.CP;
                                objEmpresas.Municipio = Convert.ToInt32(parametros.Municipio);
                                objEmpresas.Estado = Convert.ToInt32(parametros.Estado);
                                objEmpresas.Pais = parametros.Pais;
                                objEmpresas.Estatus = true;
                                objEmpresas.FechaAlta = DateTime.Now;
                                objEmpresas.FechaRegistro = DateTime.Now;
                                db.MEBAProEmpresas.Add(objEmpresas);
                                db.SaveChanges();

                                using (MEBAEntities oDB2 = new MEBAEntities())
                                {
                                    MEBAProEmpresas objEmpresas2 = oDB2.MEBAProEmpresas.Where(r => r.GuidEmpresa == vSessiones.sesionUsuarioDTO.GuidEmpresa).FirstOrDefault();
                                    MEBABitacoraAcc objBitacora = new MEBABitacoraAcc();

                                    objBitacora.Fecha = DateTime.Now;
                                    objBitacora.Tipo = "Operativo";
                                    objBitacora.Origen = vSessiones.sessionUsuarioCONFIGDTO.SitioWEB;
                                    objBitacora.Usuario = vSessiones.sesionUsuarioDTO.UserName;
                                    objBitacora.Grupo = objEmpresas2.NombreComercial;
                                    objBitacora.Zona = "";
                                    objBitacora.Estacion = "";
                                    objBitacora.Descripcion = "Se agrego un nuevo Usuario";
                                    objBitacora.IdAsociado = vSessiones.sessionUsuarioCONFIGDTO.IDAsociado;
                                    var FechaAnterior = oDB2.spdObtenerFechaAnterior("0", vSessiones.sessionUsuarioEmpresa.NombreComercial).FirstOrDefault();
                                    if (FechaAnterior != null)
                                    {
                                        objBitacora.FechaAnterior = FechaAnterior;
                                    }

                                    oDB2.SaveChanges();
                                }

                                resultado = "grupo agregado con exito.";
                            }
                            else
                            {
                                resultado = "este usuario ya se encuentra registrado en la base de datos.";
                                objResultado.ITEMS = resultado;
                                objResultado.SUCCESS = false;
                                objResultado.MENSAJE = "";
                                return Json(objResultado, JsonRequestBehavior.AllowGet);
                            }

                            break;
                        case "M":

                            objUsuario = db.MEBAProEmpresas.Where(r => r.IDEmpresa == parametros.IDEmpresa).FirstOrDefault();
                            if (objUsuario != null)
                            {
                                var objExiste = db.MEBAProEmpresas.Where(r => r.IDEmpresa != parametros.IDEmpresa && r.NombreComercial == parametros.NombreComercial).FirstOrDefault();
                                if (objExiste == null)
                                {
                                    MEBAAsociados objAociado = db.MEBAAsociados.Where(r => r.IDAsociado == objUsuario.IDAsociado).FirstOrDefault();
                                    MEBAAsociadosConfig objAociadoConfig = db.MEBAAsociadosConfig.Where(r => r.IDAsociado == objUsuario.IDAsociado).FirstOrDefault();

                                    objAociado.NombreComercial = parametros.NombreComercial;
                                    objAociado.Telefono = parametros.Telefono;
                                    objAociado.Calle = parametros.Calle;
                                    objAociado.CP = parametros.CP;
                                    objAociado.Pais = parametros.Pais;
                                    objAociado.Estado = parametros.Estado;
                                    objAociado.Municipio = parametros.Municipio;
                                    objAociado.ReprocesarEstados = parametros.ReprocesarEstados == null ? false : true;
                                    //objAociado.MostrarTiempo = parametros.MostrarTiempo;
                                    //objAociado.MostrarTurno = parametros.MostrarTurno;
                                    //objAociado.MostrarXml = parametros.MostrarXml;
                                    //objAociado.MostrarInventarios = parametros.MostrarInventarios;

                                    db.SaveChanges();

                                    var objAsociado = db.MEBAAsociados.OrderByDescending(r => r.IDAsociado).FirstOrDefault();

                                    if (parametros.TipoConexion.ToUpper() == "1")
                                    {
                                        objAociadoConfig.IDAsociado = objAociado.IDAsociado;
                                        objAociadoConfig.SitioWEB = "local";
                                        objAociadoConfig.UsuarioSQLSeguridad = parametros.UsuarioSQLSeguridad;
                                        objAociadoConfig.PasswordSQLSeguridad = Funciones.EncriptaClaveSQL(parametros.PasswordSQLSeguridad);
                                        objAociadoConfig.BaseProduccion = parametros.BaseProduccion;
                                        objAociadoConfig.ServerSeguridad = parametros.ServerSeguridad;
                                        objAociadoConfig.urlApi = null;
                                        objAociadoConfig.usuarioApi = null;
                                        objAociadoConfig.passwordApi = null;
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        objAociadoConfig.SitioWEB = "sbs.sicmacontroles.mx";
                                        objAociadoConfig.IDAsociado = objAociado.IDAsociado;
                                        objAociadoConfig.UsuarioSQLSeguridad = null;
                                        objAociadoConfig.PasswordSQLSeguridad = null;
                                        objAociadoConfig.BaseProduccion = null;
                                        objAociadoConfig.ServerSeguridad = null;
                                        objAociadoConfig.urlApi = parametros.urlApi;
                                        objAociadoConfig.usuarioApi = parametros.usuarioApi;
                                        objAociadoConfig.passwordApi = parametros.passwordApi;
                                        db.SaveChanges();
                                    }

                                    objUsuario.NombreComercial = parametros.NombreComercial;
                                    objUsuario.Telefono = parametros.Telefono;
                                    objUsuario.Calle = parametros.Calle;
                                    objUsuario.CP = parametros.CP;
                                    objUsuario.Municipio = Convert.ToInt32(parametros.Municipio);
                                    objUsuario.Estado = Convert.ToInt32(parametros.Estado);
                                    objUsuario.Pais = parametros.Pais;
                                    db.SaveChanges();
                                    using (MEBAEntities oDB2 = new MEBAEntities())
                                    {
                                        MEBAProEmpresas objEmpresas = oDB2.MEBAProEmpresas.Where(r => r.GuidEmpresa == vSessiones.sesionUsuarioDTO.GuidEmpresa).FirstOrDefault();
                                        MEBABitacoraAcc objBitacora = new MEBABitacoraAcc();

                                        objBitacora.Fecha = DateTime.Now;
                                        objBitacora.Tipo = "Operativo";
                                        objBitacora.Origen = vSessiones.sessionUsuarioCONFIGDTO.SitioWEB;
                                        objBitacora.Usuario = vSessiones.sesionUsuarioDTO.UserName;
                                        objBitacora.Grupo = objEmpresas.NombreComercial;
                                        objBitacora.Zona = "";
                                        objBitacora.Estacion = "";
                                        objBitacora.Descripcion = "Se modifico un nuevo Usuario";
                                        objBitacora.IdAsociado = vSessiones.sessionUsuarioCONFIGDTO.IDAsociado;
                                        var FechaAnterior = oDB2.spdObtenerFechaAnterior("0", vSessiones.sessionUsuarioEmpresa.NombreComercial).FirstOrDefault();
                                        if (FechaAnterior != null)
                                        {
                                            objBitacora.FechaAnterior = FechaAnterior;
                                        }
                                        oDB2.MEBABitacoraAcc.Add(objBitacora);
                                        oDB2.SaveChanges();
                                    }
                                    resultado = "grupo editado con exito.";
                                }
                                else
                                {
                                    resultado = "este usuario ya se encuentra registrado en la base de datos.";
                                    objResultado.ITEMS = resultado;
                                    objResultado.SUCCESS = false;
                                    objResultado.MENSAJE = "";
                                    return Json(objResultado, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                resultado = "este usuario ya se encuentra registrado en la base de datos.";
                            }

                            break;
                        case "B":
                            objUsuario = db.MEBAProEmpresas.Where(r => r.IDEmpresa == parametros.IDEmpresa).FirstOrDefault();
                            if (objUsuario != null)
                            {
                                objUsuario.Estatus = parametros.Estatus;
                                db.SaveChanges();
                                using (MEBAEntities oDB2 = new MEBAEntities())
                                {
                                    MEBAProEmpresas objEmpresas = oDB2.MEBAProEmpresas.Where(r => r.GuidEmpresa == vSessiones.sesionUsuarioDTO.GuidEmpresa).FirstOrDefault();
                                    MEBABitacoraAcc objBitacora = new MEBABitacoraAcc();

                                    objBitacora.Fecha = DateTime.Now;
                                    objBitacora.Tipo = "Operativo";
                                    objBitacora.Origen = vSessiones.sessionUsuarioCONFIGDTO.SitioWEB;
                                    objBitacora.Usuario = vSessiones.sesionUsuarioDTO.UserName;
                                    objBitacora.Grupo = objEmpresas.NombreComercial;
                                    objBitacora.Zona = "";
                                    objBitacora.Estacion = "";
                                    objBitacora.Descripcion = "Se Elimino un nuevo Usuario";
                                    objBitacora.IdAsociado = vSessiones.sessionUsuarioCONFIGDTO.IDAsociado;
                                    var FechaAnterior = oDB2.spdObtenerFechaAnterior("0", vSessiones.sessionUsuarioEmpresa.NombreComercial).FirstOrDefault();
                                    if (FechaAnterior != null)
                                    {
                                        objBitacora.FechaAnterior = FechaAnterior;
                                    }
                                    oDB2.MEBABitacoraAcc.Add(objBitacora);
                                    oDB2.SaveChanges();
                                }
                                resultado = "usuario eliminado con exito.";
                            }
                            break;
                    }
                    objResultado.ITEMS = resultado;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }
                else
                {

                    objResultado.ITEMS = "No tienes permisos para Agregar Grupos.";
                    objResultado.SUCCESS = false;
                    objResultado.MENSAJE = "No tienes permisos para Agregar Grupos.";
                }
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }


        #endregion

        public JsonResult GetListadoBitacora()
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {
                List<MEBABitacoraAcc> lstDatosUsuarios = new List<MEBABitacoraAcc>();
                if (vSessiones.sesionUsuarioDTO.idRol == 0 || vSessiones.sesionUsuarioDTO.idRol == 1 && vSessiones.sessionUsuarioEmpresa.GuidEmpresa == "b64027dd93594c4dadbe1fb74afd0553")
                {

                    lstDatosUsuarios = db.MEBABitacoraAcc.ToList().OrderByDescending(r => r.Fecha).ToList();

                    objResultado.ITEMS = lstDatosUsuarios;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }else if (vSessiones.sesionUsuarioDTO.idRol == 1 && vSessiones.sessionUsuarioEmpresa.GuidEmpresa != "b64027dd93594c4dadbe1fb74afd0553")
                {

                    lstDatosUsuarios = db.MEBABitacoraAcc.Where(r => r.Grupo == vSessiones.sessionUsuarioEmpresa.NombreComercial).ToList().OrderByDescending(r => r.Fecha).ToList();

                    objResultado.ITEMS = lstDatosUsuarios;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }
                else if (vSessiones.sesionUsuarioDTO.idRol == 2)
                {
                    lstDatosUsuarios = db.MEBABitacoraAcc.Where(r => r.Grupo == vSessiones.sessionUsuarioEmpresa.NombreComercial).ToList().OrderByDescending(r => r.Fecha).ToList();

                    objResultado.ITEMS = lstDatosUsuarios;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";

                }
                else if (vSessiones.sesionUsuarioDTO.idRol == 3)
                {
                    lstDatosUsuarios = db.MEBABitacoraAcc.Where(r => r.Grupo == vSessiones.sessionUsuarioEmpresa.NombreComercial).ToList().OrderByDescending(r => r.Fecha).ToList();

                    objResultado.ITEMS = lstDatosUsuarios;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";

                }
                else
                {
                    objResultado.ITEMS = null;
                    objResultado.SUCCESS = false;
                    objResultado.MENSAJE = "No tienes permisos para acceder a esta vista.";
                }

            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            var jsonResult = Json(objResultado, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
            //return Json(objResultado, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetListadoEstados()
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {

                var lstDatosUsuarios = db.MEBAGenEstados.ToList().Select(y => new
                {
                    value = y.IdEstado,
                    text = y.Estado,
                }).ToList();

                objResultado.ITEMS = lstDatosUsuarios;
                objResultado.SUCCESS = true;
                objResultado.MENSAJE = "";


            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListadoMunicipios(int IdEstado)
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {

                var lstDatosUsuarios = db.MEBAGenMunicipios.Where(r => r.IdEstado == IdEstado).ToList().Select(y => new
                {
                    value = y.IdMunicipio,
                    text = y.Municipio,
                }).ToList();

                objResultado.ITEMS = lstDatosUsuarios;
                objResultado.SUCCESS = true;
                objResultado.MENSAJE = "";

            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }


        public JsonResult postObtenerAsociados(ParametrosAsociados parametros)
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {
                List<ClsModAsociadosContactos> lstDatosUsuarios = new List<ClsModAsociadosContactos>();
                if (vSessiones.sesionUsuarioDTO.idRol == 1)
                {

                    lstDatosUsuarios = db.MEBAAsociadosContactos.Where(r => r.IDAsociado == parametros.IDAsociado).ToList().Select(y => new ClsModAsociadosContactos
                    {
                        IDContacto = y.IDContacto,
                        IDAsociado = y.IDAsociado,
                        IDTipoContacto = y.IDTipoContacto,
                        Nombre = y.Nombre,
                        Telefono = y.Telefono,
                        Extension = y.Extension,
                        Puesto = y.Puesto,
                        Email = y.Email,
                        Cometario = y.Cometario,
                        Activo = y.Activo,
                        FechaAlta = y.FechaAlta,
                        IdRol = y.IdRol,
                        Rol = db.MEBARolUsuario.Where(r => r.idRol == y.IdRol).FirstOrDefault() == null ? "" : db.MEBARolUsuario.Where(r => r.idRol == y.IdRol).FirstOrDefault().Descripcion,
                    }).ToList();

                    objResultado.ITEMS = lstDatosUsuarios;
                    objResultado.SUCCESS = true;
                    objResultado.MENSAJE = "";
                }
                else
                {

                    objResultado.ITEMS = null;
                    objResultado.SUCCESS = false;
                    objResultado.MENSAJE = "No tienes permisos para acceder a esta vista.";
                }

            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult postAgregarModificarEliminarAsociados(ParametrosAsociados parametros)
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {
                MEBAAsociadosContactos objUsuario = new MEBAAsociadosContactos();
                string resultado = "";
                switch (parametros.Accion)
                {
                    case "A":
                        objUsuario = db.MEBAAsociadosContactos.Where(r => r.IDContacto == parametros.IDContacto).FirstOrDefault();
                        if (objUsuario == null)
                        {
                            objUsuario = new MEBAAsociadosContactos();
                            objUsuario.IDAsociado = parametros.IDAsociado;
                            objUsuario.Nombre = parametros.Nombre;
                            objUsuario.Telefono = parametros.Telefono;
                            objUsuario.IdRol = parametros.IdRol;
                            db.MEBAAsociadosContactos.Add(objUsuario);
                            db.SaveChanges();


                            using (MEBAEntities oDB2 = new MEBAEntities())
                            {


                                MEBAProEmpresas objEmpresas = oDB2.MEBAProEmpresas.Where(r => r.GuidEmpresa == vSessiones.sesionUsuarioDTO.GuidEmpresa).FirstOrDefault();
                                MEBABitacoraAcc objBitacora = new MEBABitacoraAcc();

                                objBitacora.Fecha = DateTime.Now;
                                objBitacora.Tipo = "Operativo";
                                objBitacora.Origen = vSessiones.sessionUsuarioCONFIGDTO.SitioWEB;
                                objBitacora.Usuario = vSessiones.sesionUsuarioDTO.UserName;
                                objBitacora.Grupo = objEmpresas.NombreComercial;
                                objBitacora.Zona = "";
                                objBitacora.Estacion = "";
                                objBitacora.Descripcion = "Se Agrego un nuevo Asociado";
                                objBitacora.IdAsociado = vSessiones.sessionUsuarioCONFIGDTO.IDAsociado;
                                var FechaAnterior = oDB2.spdObtenerFechaAnterior("0", vSessiones.sessionUsuarioEmpresa.NombreComercial).FirstOrDefault();
                                if (FechaAnterior != null)
                                {
                                    objBitacora.FechaAnterior = FechaAnterior;
                                }
                                oDB2.MEBABitacoraAcc.Add(objBitacora);
                                oDB2.SaveChanges();
                            }
                        }


                        break;
                    case "M":
                        objUsuario = db.MEBAAsociadosContactos.Where(r => r.IDContacto == parametros.IDContacto).FirstOrDefault();
                        if (objUsuario != null)
                        {
                            objUsuario.Nombre = parametros.Nombre;
                            objUsuario.Telefono = parametros.Telefono;
                            objUsuario.IdRol = parametros.IdRol;
                            db.SaveChanges();
                            using (MEBAEntities oDB2 = new MEBAEntities())
                            {
                                MEBAProEmpresas objEmpresas = oDB2.MEBAProEmpresas.Where(r => r.GuidEmpresa == vSessiones.sesionUsuarioDTO.GuidEmpresa).FirstOrDefault();
                                MEBABitacoraAcc objBitacora = new MEBABitacoraAcc();

                                objBitacora.Fecha = DateTime.Now;
                                objBitacora.Tipo = "Operativo";
                                objBitacora.Origen = vSessiones.sessionUsuarioCONFIGDTO.SitioWEB;
                                objBitacora.Usuario = vSessiones.sesionUsuarioDTO.UserName;
                                objBitacora.Grupo = objEmpresas.NombreComercial;
                                objBitacora.Zona = "";
                                objBitacora.Estacion = "";
                                objBitacora.Descripcion = "Se Modifico un nuevo Asociado";
                                objBitacora.IdAsociado = vSessiones.sessionUsuarioCONFIGDTO.IDAsociado;
                                var FechaAnterior = oDB2.spdObtenerFechaAnterior("0", vSessiones.sessionUsuarioEmpresa.NombreComercial).FirstOrDefault();
                                if (FechaAnterior != null)
                                {
                                    objBitacora.FechaAnterior = FechaAnterior;
                                }
                                oDB2.MEBABitacoraAcc.Add(objBitacora);
                                oDB2.SaveChanges();
                            }
                        }

                        break;
                    case "B":
                        objUsuario = db.MEBAAsociadosContactos.Where(r => r.IDContacto == parametros.IDContacto).FirstOrDefault();
                        if (objUsuario != null)
                        {
                            db.MEBAAsociadosContactos.Remove(objUsuario);
                            db.SaveChanges();
                            using (MEBAEntities oDB2 = new MEBAEntities())
                            {
                                MEBAProEmpresas objEmpresas = oDB2.MEBAProEmpresas.Where(r => r.GuidEmpresa == vSessiones.sesionUsuarioDTO.GuidEmpresa).FirstOrDefault();
                                MEBABitacoraAcc objBitacora = new MEBABitacoraAcc();

                                objBitacora.Fecha = DateTime.Now;
                                objBitacora.Tipo = "Operativo";
                                objBitacora.Origen = vSessiones.sessionUsuarioCONFIGDTO.SitioWEB;
                                objBitacora.Usuario = vSessiones.sesionUsuarioDTO.UserName;
                                objBitacora.Grupo = objEmpresas.NombreComercial;
                                objBitacora.Zona = "";
                                objBitacora.Estacion = "";
                                objBitacora.Descripcion = "Se Elimino un nuevo Asociado";
                                objBitacora.IdAsociado = vSessiones.sessionUsuarioCONFIGDTO.IDAsociado;
                                var FechaAnterior = oDB2.spdObtenerFechaAnterior("0", vSessiones.sessionUsuarioEmpresa.NombreComercial).FirstOrDefault();
                                if (FechaAnterior != null)
                                {
                                    objBitacora.FechaAnterior = FechaAnterior;
                                }
                                oDB2.MEBABitacoraAcc.Add(objBitacora);
                                oDB2.SaveChanges();
                            }
                            resultado = "usuario eliminado con exito.";
                        }
                        break;
                }
                objResultado.ITEMS = resultado;
                objResultado.SUCCESS = true;
                objResultado.MENSAJE = "";
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        #region ZONAS

        public JsonResult GetListadoZonas()
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {

                var lstDatosUsuarios = db.MEBAZonas.Where(r => r.IdEmpresa == vSessiones.sessionUsuarioEmpresa.IDEmpresa).ToList().Select(y => new resultadosZonas
                {
                    IdZona = y.IdZona,
                    Zona = y.Descripcion,
                    lstEstacionesAsignadas = db.MEBARelZonasEstaciones.Where(r => r.IdZona == y.IdZona).ToList().Select(n => n.IdEstaciones).ToList(),
                }).ToList();

                objResultado.ITEMS = lstDatosUsuarios;
                objResultado.SUCCESS = true;
                objResultado.MENSAJE = "";


            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult postCboUsuarios()
        {
            try
            {
                var objCombo = db.MEBAProEmpresasUsuarios.Where(r => r.GuidEmpresa == vSessiones.sessionUsuarioEmpresa.GuidEmpresa).ToList().Select(y => new
                {
                    value = y.IdUsuario,
                    text = y.Nombre + " " + y.ApellidoPaterno + " " + y.ApellidoMaterno
                }).ToList();
                return Json(objCombo, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public JsonResult postListadoZonas(parametrosZona parametros)
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {
                MEBAZonas objZona = new MEBAZonas();
                MEBARelZonasEstaciones objRelZonas = new MEBARelZonasEstaciones();
                List<MEBARelZonasEstaciones> lstRelZonas = new List<MEBARelZonasEstaciones>();
                string resultado = "";
                switch (parametros.Accion)
                {
                    case "A":
                        objZona = new MEBAZonas();
                        objZona.IdEmpresa = vSessiones.sessionUsuarioEmpresa.IDEmpresa;
                        objZona.Descripcion = parametros.Zona;
                        objZona.Activo = true;
                        db.MEBAZonas.Add(objZona);
                        db.SaveChanges();

                        foreach (var item in parametros.lstEstacionesAsignadas)
                        {
                            objRelZonas = new MEBARelZonasEstaciones();
                            objRelZonas.IdEstaciones = item.Value;
                            objRelZonas.IdZona = objZona.IdZona;
                            lstRelZonas.Add(objRelZonas);
                        }
                        db.MEBARelZonasEstaciones.AddRange(lstRelZonas);
                        db.SaveChanges();


                        break;
                    case "M":

                        lstRelZonas = db.MEBARelZonasEstaciones.Where(r => r.IdZona == parametros.IdZona).ToList();
                        if (lstRelZonas.Count != 0)
                        {
                            db.MEBARelZonasEstaciones.RemoveRange(lstRelZonas);
                            db.SaveChanges();
                        }

                        lstRelZonas = new List<MEBARelZonasEstaciones>();
                        foreach (var item in parametros.lstEstacionesAsignadas)
                        {

                            objRelZonas = new MEBARelZonasEstaciones();
                            objRelZonas.IdEstaciones = item.Value;
                            objRelZonas.IdZona = parametros.IdZona;
                            lstRelZonas.Add(objRelZonas);
                        }
                        db.MEBARelZonasEstaciones.AddRange(lstRelZonas);
                        db.SaveChanges();

                        break;
                    case "B":

                        objZona = db.MEBAZonas.Where(r => r.IdZona == parametros.IdZona).FirstOrDefault();
                        lstRelZonas = db.MEBARelZonasEstaciones.Where(r => r.IdZona == parametros.IdZona).ToList();
                        if (lstRelZonas.Count != 0)
                        {
                            db.MEBARelZonasEstaciones.RemoveRange(lstRelZonas);
                            db.SaveChanges();
                        }
                        db.MEBAZonas.Remove(objZona);
                        db.SaveChanges();
                        break;
                }
                objResultado.ITEMS = resultado;
                objResultado.SUCCESS = true;
                objResultado.MENSAJE = "";
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }




        #endregion

        public JsonResult GetListadoPermisos()
        {
            ClsModResponse objResultado = new ClsModResponse();
            {
                try
                {
                    using (var ctx = new MEBAEntities())
                    {
                        var lstUsuarios = ctx.MEBAProEmpresasUsuarios.ToList();
                        objResultado.ITEMS = lstUsuarios;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult postCargarModalMenu(int idUsuario)
        {
            ClsModResponse objResultado = new ClsModResponse();
            try
            {
                var lstDatosUsuarios = db.spdObtenerMenuParaVistaUsuarios().ToList().Select(r => new parametrosMenu
                {
                    contador = obtenerContador(idUsuario, r.OIDMenuPadre),
                    OIDMenuPadre = r.OIDMenuPadre,
                    OIDMenuHijo = r.OIDMenuHijo,
                    MenuDescripcionPadre = r.MenuDescripcionPadre,
                    MenuDescripcionHijo = r.MenuDescripcionHijo,
                    OIDUsuario = idUsuario,
                    check = db.spdObtenerMenuSeleccionado(idUsuario, r.OIDMenuPadre, r.OIDMenuHijo).FirstOrDefault() == null ? false : true

                }).ToList();

                objResultado.ITEMS = lstDatosUsuarios;
                objResultado.SUCCESS = true;
                objResultado.MENSAJE = "";
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }
        public int obtenerContador(int Idusuario, int OIDMenuPadre)
        {
            int variable = 0;
            var obtenerUsuario = db.spdObtenerContadorDeHijoEnPadres(Idusuario, OIDMenuPadre).FirstOrDefault();
            if (obtenerUsuario == null)
            {
                variable = 0;
            }
            else
            {
                variable = obtenerUsuario.Value;
            }
            return variable;
        }
        public JsonResult postInsertarMenu(List<parametrosMenu> parametros)
        {
            ClsModResponse objResultado = new ClsModResponse();
            GenPermisosVistas objPermisosVistas = new GenPermisosVistas();

            try
            {
                var OIDUsuario = parametros.Select(r => r.OIDUsuario).FirstOrDefault();
                var lstResultado = db.GenPermisosVistas.Where(r => r.OIDUsuario == OIDUsuario).ToList();
                if (lstResultado.Count() != 0)
                {
                    db.GenPermisosVistas.RemoveRange(lstResultado);
                    db.SaveChanges();
                }

                foreach (var item in parametros)
                {
                    if (item.check == true)
                    {
                        objPermisosVistas = new GenPermisosVistas();
                        objPermisosVistas.OIDUsuario = item.OIDUsuario;
                        objPermisosVistas.OIDMenuPadre = item.OIDMenuPadre;
                        objPermisosVistas.OIDMenuHijo = item.OIDMenuHijo;

                        db.GenPermisosVistas.Add(objPermisosVistas);
                        db.SaveChanges();
                    }
                }

                objResultado.ITEMS = null;
                objResultado.SUCCESS = true;
                objResultado.MENSAJE = "Guardado con exito.";
            }
            catch (Exception ex)
            {
                objResultado.ITEMS = null;
                objResultado.SUCCESS = false;
                objResultado.MENSAJE = ex.Message.ToString();
            }
            return Json(objResultado, JsonRequestBehavior.AllowGet);
        }
    }
}
